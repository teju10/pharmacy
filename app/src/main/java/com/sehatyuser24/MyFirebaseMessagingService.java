/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sehatyuser24;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.sehatyuser24.activity.other.ActivityProductDetail;
import com.sehatyuser24.activity.other.Activity_MyOrderDetail;
import com.sehatyuser24.activity.registration.MainActivity;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Utility;

import java.util.Map;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ

        Log.e(TAG, "getFrom ====> " + remoteMessage.getFrom());


        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Message data payload: " + remoteMessage.getData());
            Map<String, String> notification = remoteMessage.getData();
            Bundle bundle = new Bundle();
            for (Map.Entry<String, String> entry : notification.entrySet()) {
                bundle.putString(entry.getKey(), entry.getValue());
            }
            try {
                if (ActivityProductDetail.isopenHome != null) {
                    if (notification.get("message").contains("Added new product")) {
                        Utility.setIntegerSharedPreference(getApplicationContext(), Constants.COUNT, Integer.parseInt(notification.get("count")));
                        Intent broadcastIntent = new Intent();
                        broadcastIntent.setAction(ActivityProductDetail.mBroadcastString);
                        sendBroadcast(broadcastIntent);
                        System.out.println("mBroadcastString--->>>" + Utility.getIngerSharedPreferences(getApplicationContext(), Constants.COUNT));

                    }
                } else if (notification.get("message").contains("Your order ready to delivered")) {
                    sendNotification(notification.get("message"), bundle);
                } else if (notification.get("type").contains("OverallOrderRating")) {
                    sendNotification(notification.get("message"), bundle);
                }  else {
                    sendNotification(notification.get("message"), bundle);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
    }

    private void sendNotification(String messageBody, Bundle b) {
        Intent i = new Intent();
        if (b.getString("message").contains("Added new product")) {
            i = new Intent(this, ActivityProductDetail.class);
        } else if (b.getString("message").contains("Your order ready to delivered")) {
            i = new Intent(this, Activity_MyOrderDetail.class);
        }  else if (b.getString("type").contains("OverallOrderRating")) {
            b.putString(Constants.ORDERID, b.getString("orderid"));
            b.putString(Constants.NOTITYPE, b.getString("type"));
            i = new Intent(this, MainActivity.class);
        } else {
            i = new Intent(this, MainActivity.class);
        }
        i.putExtras(b);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, i,
                    PendingIntent.FLAG_ONE_SHOT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setContentTitle(getResources().getString(R.string.app_name))
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
        }
    }

