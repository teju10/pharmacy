package com.sehatyuser24.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.sehatyuser24.R;
import com.sehatyuser24.activity.other.Activity_MyOrderDetail;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.drawerfragment.MyOrderFragment;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by sandeep on 19/5/17.
 */

public class MyOrderAdapter extends BaseAdapter {

    Context mContext;
    List<JSONObject> datalist;
    LayoutInflater li;
    MyOrderFragment mainFragment;

    public MyOrderAdapter(MyOrderFragment fragment, Context apContext, List<JSONObject> list) {
        this.mainFragment = fragment;
        this.mContext = apContext;
        this.datalist = list;
        li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return datalist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final MyOrderVH viewHolder;
        if (convertView == null) {
            convertView = li.inflate(R.layout.myorder_list_single_item, null);
            viewHolder = new MyOrderVH(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (MyOrderVH) convertView.getTag();
        }
        try {

            JSONObject object = datalist.get(position);
            String pri = object.getString("price") + " " + mContext.getResources().getString(R.string.currncy_symbl);
            viewHolder.my_order_prodctid.setText(object.getString("orderid"));

            String date[] = object.getString("datetime").split(" ");
            String Formate_date = Utility.ChangeDateFormat("yyyy-MM-dd", "dd MMM", date[0]);

            viewHolder.ohis_date_time_txt.setText(Formate_date);
            viewHolder.my_order_price.setText(pri);
            if (object.getString("order_status").equals("1")) {
                viewHolder.my_order_status.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
                viewHolder.my_order_status.setText(object.getString("status"));

            } else {
                viewHolder.my_order_status.setTextColor(mContext.getResources().getColor(R.color.orange_papya));
                viewHolder.my_order_status.setText(object.getString("status"));
            }
           // viewHolder.my_order_quant.setText(object.getString("quantity"));

            Glide.with(mContext).
                    load(object.getString("image")).
                    listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            viewHolder.mor_pb.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            viewHolder.mor_pb.setVisibility(View.GONE);
                            return false;
                        }
                    }).
                    into(viewHolder.my_order_img);

            convertView.setOnClickListener(new ItemClick(position));

        } catch (JSONException j) {
            j.printStackTrace();
        }

        return convertView;
    }

    public class ItemClick implements View.OnClickListener {
        int pos;

        public ItemClick(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            try {
                System.out.println("DATALIST " + datalist.get(pos).getString("orderid"));
                Bundle b = new Bundle();
                Intent i = new Intent(mContext, Activity_MyOrderDetail.class);
                b.putString(Constants.ORDERNO, datalist.get(pos).getString("orderid"));
                i.putExtras(b);
                mContext.startActivity(i);
                Utility.setSharedPreference(mContext, Constants.lay_click, "1");
            } catch (JSONException j) {
                j.printStackTrace();
            }
        }
    }

    public class MyOrderVH {
        CustomTextView my_order_prodctid, ohis_date_time_txt, my_order_price,
                my_order_status, my_order_quant, quant_list_txt, order_list_txt, tprice_list_txt;
        ProgressBar mor_pb;
        ImageView my_order_img;

        public MyOrderVH(View v) {
            my_order_img = (ImageView) v.findViewById(R.id.my_order_img);
            my_order_prodctid = (CustomTextView) v.findViewById(R.id.my_order_prodctid);
            ohis_date_time_txt = (CustomTextView) v.findViewById(R.id.ohis_date_time_txt);
            my_order_price = (CustomTextView) v.findViewById(R.id.my_order_price);
            my_order_status = (CustomTextView) v.findViewById(R.id.my_order_status);
            //my_order_quant = (CustomTextView) v.findViewById(R.id.my_order_quant);
            //quant_list_txt = (CustomTextView) v.findViewById(R.id.quant_list_txt);
            order_list_txt = (CustomTextView) v.findViewById(R.id.order_list_txt);
            tprice_list_txt = (CustomTextView) v.findViewById(R.id.tprice_list_txt);
            mor_pb = (ProgressBar) v.findViewById(R.id.mor_pb);
        }

    }

}
