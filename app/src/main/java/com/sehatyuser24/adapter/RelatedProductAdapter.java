package com.sehatyuser24.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.sehatyuser24.R;
import com.sehatyuser24.customwidget.CustomButton;
import com.sehatyuser24.customwidget.CustomTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by and-04 on 26/7/17.
 */

public class RelatedProductAdapter extends BaseAdapter {

    Context mContext;
    ArrayList<JSONObject> jobj;
    LayoutInflater li;


    public static final String TAG = RelatedProductAdapter.class.getSimpleName();

    public RelatedProductAdapter(Context context, ArrayList<JSONObject> jobj) {
        this.mContext = context;
        this.jobj = jobj;
        li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return jobj.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final MyOrderVH viewHolder;
        if (convertView == null) {
            convertView = li.inflate(R.layout.adap_home_sessional_item, null);
            viewHolder = new MyOrderVH(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (MyOrderVH) convertView.getTag();
        }
        try {
            JSONObject object = jobj.get(position);
            viewHolder.sesion_product_desc.setText(object.getString("title"));

            if (object.getString("discount").equals("0")) {
                viewHolder.sesion_price.setVisibility(View.GONE);
                viewHolder.sesion_per_off.setVisibility(View.GONE);
                viewHolder.sesion_actual_pri.setText("$" + object.getString("discount_price"));
            } else {
                viewHolder.sesion_price.setText("$" + object.getString("price"));
                viewHolder.sesion_actual_pri.setText("$" + object.getString("discount_price"));
            }

            Glide.with(mContext).
                    load(object.getString("product_image")).into(viewHolder.sesion_product_img);
//            viewHolder.f_mem_edit_account_btn.setOnClickListener(new ItemEditClick(position));

        } catch (JSONException j) {
            j.printStackTrace();
        }

        return convertView;
    }

    public class MyOrderVH {
        ImageView sesion_product_img;
        CustomTextView sesion_product_desc, sesion_price, sesion_per_off, sesion_actual_pri;
        ProgressBar sesion_home_pb;
        CustomButton sesion_view_detail_btn;

        public MyOrderVH(View v) {
            sesion_product_img = (ImageView) v.findViewById(R.id.sesion_product_img);
            sesion_product_desc = (CustomTextView) v.findViewById(R.id.sesion_product_desc);
            sesion_price = (CustomTextView) v.findViewById(R.id.sesion_price);
            sesion_per_off = (CustomTextView) v.findViewById(R.id.sesion_per_off);
            sesion_actual_pri = (CustomTextView) v.findViewById(R.id.sesion_actual_pri);
            sesion_view_detail_btn = (CustomButton) v.findViewById(R.id.sesion_view_detail_btn);
            sesion_home_pb = (ProgressBar) v.findViewById(R.id.sesion_home_pb);

        }

    }
}
