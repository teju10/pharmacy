package com.sehatyuser24.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.sehatyuser24.R;
import com.sehatyuser24.activity.other.ActivityProductDetail;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.utility.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Tejas on 9/5/17.
 */

public class RelatedHorizontalListAdapter extends RecyclerView.Adapter<RelatedHorizontalListAdapter.PopViewHolder> {

    Context mContext;
    ArrayList<JSONObject> j_pop_list;
    int res = 0;
    public static final String TAG = RelatedHorizontalListAdapter.class.getSimpleName();

    public RelatedHorizontalListAdapter(Context context, int res, ArrayList<JSONObject> jobj) {
        this.mContext = context;
        this.j_pop_list = jobj;
        this.res = res;
    }

    @Override
    public RelatedHorizontalListAdapter.PopViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = li.inflate(res, parent, false);
        return new RelatedHorizontalListAdapter.PopViewHolder(convertView);
    }

    @Override
    public void onBindViewHolder(final RelatedHorizontalListAdapter.PopViewHolder holder, int position) {
        try {

            JSONObject j = j_pop_list.get(position);
            holder.releted_titlename.setText(j.getString("title"));
            if (j.getString(Constants.PRESCRIPTION_STATUS).equals("1")) {
                holder.related_pro_rximg.setVisibility(View.VISIBLE);
            }

            Glide.with(mContext).load(j.getString("product_image"))
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            holder.pop_home_pb.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            holder.pop_home_pb.setVisibility(View.GONE);
                            return false;
                        }
                    }).into(holder.pop_product_img);

            holder.view_rl.setOnClickListener(new MyClick(position));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return j_pop_list == null ? 0 : j_pop_list.size();
    }

    class PopViewHolder extends RecyclerView.ViewHolder {

        CustomTextView releted_titlename, pop_price, pop_per_off, pop_view_detail_btn;
        ImageView pop_product_img, related_pro_rximg;
        ProgressBar pop_home_pb;
        RelativeLayout view_rl;

        public PopViewHolder(View v) {
            super(v);
            releted_titlename = (CustomTextView) v.findViewById(R.id.releted_titlename);
            pop_price = (CustomTextView) v.findViewById(R.id.pop_price);
            pop_per_off = (CustomTextView) v.findViewById(R.id.pop_per_off);
            pop_view_detail_btn = (CustomTextView) v.findViewById(R.id.pop_view_detail_btn);
            pop_product_img = (ImageView) v.findViewById(R.id.pop_product_img);
            pop_home_pb = (ProgressBar) v.findViewById(R.id.pop_home_pb);
            pop_product_img = (ImageView) v.findViewById(R.id.pop_product_img);
            related_pro_rximg = (ImageView) v.findViewById(R.id.related_pro_rximg);
            view_rl = (RelativeLayout) v.findViewById(R.id.view_rl);

        }
    }

    public class MyClick implements View.OnClickListener {
        int pos;

        public MyClick(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            try {
                Intent i = new Intent(mContext, ActivityProductDetail.class);
                Bundle b = new Bundle();
                b.putString(Constants.PRODUCTID, j_pop_list.get(pos).getString("product_id"));
                i.putExtras(b);
                mContext.startActivity(i);

            } catch (JSONException j) {
                j.printStackTrace();
            }

        }
    }

}

