package com.sehatyuser24.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sehatyuser24.R;

import com.sehatyuser24.activity.other.Activity_Medication_Medicine;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.fastscroll_list.RecyclerViewFastScroller;
import com.sehatyuser24.fastscroll_list.adapters.AlphabetAdapter;
import com.sehatyuser24.utility.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by and-04 on 8/6/17.
 */

public class MedicationSingleViewAdapter extends RecyclerView.Adapter<MedicationSingleViewAdapter.ViewHolder>
        implements RecyclerViewFastScroller.BubbleTextGetter {

    List<String> mDataArray;
    Context mContext;

    public MedicationSingleViewAdapter(Context context, List<String> dataset) {
        mDataArray = dataset;
        mContext = context;

    }

    @Override
    public MedicationSingleViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_view_layout, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MedicationSingleViewAdapter.ViewHolder holder, int position) {

        try {
            JSONObject j = new JSONObject(mDataArray.get(position));
            String cat = j.getString("category");
            String count = j.getString("count");
            holder.tv_alphabet.setText(cat);
            holder.tv_count.setText("(" + count + ")");

            holder.itemView.setOnClickListener(new MyClick(position));

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        if (mDataArray == null)
            return 0;
        return mDataArray.size();
    }

    @Override
    public String getTextToShowInBubble(int pos) {

        if (pos < 0 || pos >= mDataArray.size())
            return null;

        String name = mDataArray.get(pos);

        if (name == null || name.length() < 1)
            return null;

        return mDataArray.get(pos).substring(0, 1);
    }

    public class MyClick implements View.OnClickListener {
        int position;

        public MyClick(int pos) {
            this.position = pos;
        }

        @Override
        public void onClick(View v) {
            try {
                JSONObject j = new JSONObject(mDataArray.get(position));
                String cat_id = j.getString("cat_id");
                mContext.startActivity(new Intent(mContext, Activity_Medication_Medicine.class).
                        putExtra(Constants.CATEGORYID,cat_id));
                System.out.println("CATID===========>"+cat_id);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CustomTextView tv_alphabet, tv_count;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_alphabet = (CustomTextView) itemView.findViewById(R.id.tv_alphabet);
            tv_count = (CustomTextView) itemView.findViewById(R.id.tv_count);
        }
    }
}

