package com.sehatyuser24.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.sehatyuser24.R;
import com.sehatyuser24.activity.other.ActivityProductDetail;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.utility.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by and-04 on 21/9/17.
 */

public class AdapSubcat extends BaseAdapter {

    Context mContext;
    List<JSONObject> mlist;
    LayoutInflater li;

    public AdapSubcat(Context mcontext, List<JSONObject> list) {

        this.mContext = mcontext;
        this.mlist = list;
        li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mlist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder v;

        if (convertView == null) {
            convertView = li.inflate(R.layout.adap_medicinesub_item, null);
            v = new ViewHolder(convertView);
            convertView.setTag(v);
        } else {
            v = (ViewHolder) convertView.getTag();
        }
        try {

            JSONObject object = mlist.get(position);
            String pri = object.getString("price")+" "+mContext.getResources().getString(R.string.currncy_symbl);
            String dp = object.getString("discount_price")+" "+mContext.getResources().getString(R.string.currncy_symbl);

            v.subcat_price.setPaintFlags(v.subcat_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            v.subcat_product_desc.setText(object.getString("title"));
            v.subcat_price.setText(pri);
            v.subcat_actual_pri.setText(dp);
            v.subcat_per_off.setText(object.getString("discount"));

            convertView.setOnClickListener(new ItemClick(position));

        } catch (JSONException j) {
            j.printStackTrace();
        }
        return convertView;
    }

    public class ItemClick implements View.OnClickListener{
        int pos = 0;

        public ItemClick(int pos){
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            try {
                mContext.startActivity(new Intent(mContext, ActivityProductDetail.class).
                        putExtra(Constants.PRODUCTID,mlist.get(pos).getString("product_id")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public class ViewHolder {
        ImageView subcat_product_img;
        CustomTextView subcat_product_desc, subcat_price, subcat_actual_pri, subcat_per_off;

        public ViewHolder(View v) {
            subcat_product_img = (ImageView) v.findViewById(R.id.subcat_product_img);
            subcat_product_desc = (CustomTextView) v.findViewById(R.id.subcat_product_desc);
            subcat_price = (CustomTextView) v.findViewById(R.id.subcat_price);
            subcat_actual_pri = (CustomTextView) v.findViewById(R.id.subcat_actual_pri);
            subcat_per_off = (CustomTextView) v.findViewById(R.id.subcat_per_off);
        }

    }

}
