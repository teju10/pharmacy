package com.sehatyuser24.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.sehatyuser24.R;
import com.sehatyuser24.activity.other.Activity_MyPrescriptionOrderDetail;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by and-04 on 14/9/17.
 */

public class MyPrescriptionAdap extends BaseAdapter {

    Context mContext;
    List<JSONObject> datalist;
    LayoutInflater li;
    JSONArray jo;

    public MyPrescriptionAdap(Context apContext, List<JSONObject> list) {
        this.mContext = apContext;
        this.datalist = list;
        li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return datalist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final MyOrderVH viewHolder;
        if (convertView == null) {
            convertView = li.inflate(R.layout.adap_mypresc_list, null);
            viewHolder = new MyOrderVH(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (MyOrderVH) convertView.getTag();
        }
        try {
            JSONObject object = datalist.get(position);
            viewHolder.presc_id.setText(object.getString("prescrition_id"));

            String date[] = object.getString("datetime").split(" ");
            String Formate_date = Utility.ChangeDateFormat("yyyy-MM-dd", "dd MMM yyyy", date[0]);

//            String time = Utility.Time(date[1]);
            viewHolder.my_presc_date.setText(Formate_date + " | " + date[1]);

            if (object.getString("prescription_status").equals("0") || object.getString("prescription_status").equals("1") ||
                    object.getString("prescription_status").equals("2")) {
                viewHolder.my_presc_status.setText(mContext.getResources().getString(R.string.pending));
                viewHolder.my_presc_status.setTextColor(mContext.getResources().getColor(R.color.orange_papya));
            } else if (object.getString("prescription_status").equals("3")) {
                viewHolder.my_presc_status.setText(mContext.getResources().getString(R.string.presc_rejected));
                viewHolder.my_presc_status.setTextColor(mContext.getResources().getColor(R.color.orange_papya));
            } else if (object.getString("prescription_status").equals("4")) {
                viewHolder.my_presc_status.setText(mContext.getResources().getString(R.string.cencelled));
                viewHolder.my_presc_status.setTextColor(mContext.getResources().getColor(R.color.orange_papya));
            } else if (object.getString("prescription_status").equals("5")) {
                viewHolder.my_presc_status.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
                viewHolder.my_presc_status.setText(mContext.getResources().getString(R.string.approved));
            } else if (object.getString("prescription_status").equals("6")) {
                viewHolder.my_presc_status.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
                viewHolder.my_presc_status.setText(mContext.getResources().getString(R.string.order_place));
            }

            String img = "";
            jo = new JSONArray();
            jo = object.getJSONArray("prescription_image");

            img = jo.getString((jo.length() - 1));
           /* if (!String.valueOf(jo.length() - 1).equals("0")) {
                viewHolder.my_presc_img_cnt.setText("+ " + String.valueOf(jo.length() - 1));
            } else {
                viewHolder.img_count_ll.setVisibility(View.GONE);
            }*/

            viewHolder.my_presc_img_cnt.setText("+" + String.valueOf(jo.length() - 1));

            Glide.with(mContext).
                    load(img).
                    listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            viewHolder.myprsc_pb.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            viewHolder.myprsc_pb.setVisibility(View.GONE);
                            return false;
                        }
                    }).
                           into(viewHolder.my_presc_img);
            convertView.setOnClickListener(new ItemClick(position));

        } catch (JSONException j) {
            j.printStackTrace();
        }

        return convertView;
    }

    public class ItemClick implements View.OnClickListener {
        int pos = 0;

        public ItemClick(int _pos) {
            this.pos = _pos;
        }

        @Override
        public void onClick(View v) {

            try {
                mContext.startActivity(new Intent(mContext, Activity_MyPrescriptionOrderDetail.class).
                        putExtra(Constants.PRECRIPTIONID, datalist.get(pos).getString("prescrition_id")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public class MyOrderVH {
        CustomTextView presc_id, my_presc_img_cnt, my_presc_status, my_presc_date;
        ProgressBar myprsc_pb;
        ImageView my_presc_img;
        LinearLayout img_count_ll;

        public MyOrderVH(View v) {
            my_presc_img = (ImageView) v.findViewById(R.id.my_presc_img);
            presc_id = (CustomTextView) v.findViewById(R.id.presc_id);
            my_presc_img_cnt = (CustomTextView) v.findViewById(R.id.my_presc_img_cnt);
            my_presc_status = (CustomTextView) v.findViewById(R.id.my_presc_status);
            my_presc_date = (CustomTextView) v.findViewById(R.id.my_presc_date);
            img_count_ll = (LinearLayout) v.findViewById(R.id.img_count_ll);

            myprsc_pb = (ProgressBar) v.findViewById(R.id.myprsc_pb);
        }

    }
}
