package com.sehatyuser24.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.sehatyuser24.R;

import com.sehatyuser24.customwidget.CustomTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by and-04 on 18/7/17.
 */

public class ReviewRateAdapter extends BaseAdapter {

    Context mContext;
    ArrayList<JSONObject> jobj;
    LayoutInflater li;

    public ReviewRateAdapter(Context context, ArrayList<JSONObject> jlist) {
        this.mContext = context;
        this.jobj = jlist;
        li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return jobj.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final MyOrderVH viewHolder;
        if (convertView == null) {
            convertView = li.inflate(R.layout.adap_people_review, null);
            viewHolder = new MyOrderVH(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (MyOrderVH) convertView.getTag();
        }
        try {
            JSONObject object = jobj.get(position);
            viewHolder.tv_rate.setText(object.getString("product_rate"));
            viewHolder.review_name.setText(object.getString("fname"));
            if(!object.getString("comment").equals("")){
                viewHolder.review_txt.setText(object.getString("comment"));
            }else{
                viewHolder.review_txt.setTextColor(mContext.getResources().getColor(R.color.grey_400));
                viewHolder.review_txt.setText(mContext.getResources().getString(R.string.no_comment));
            }

            float rate = Float.parseFloat(object.getString("product_rate"));

            if (rate >= 0 && rate < 0.5) {

                viewHolder.star_red.setVisibility(View.VISIBLE);
                viewHolder.star_orange.setVisibility(View.GONE);
                viewHolder.star_green.setVisibility(View.GONE);
                viewHolder.rate_txt_boarder.setBackground(mContext.getResources().getDrawable(R.drawable.oval_boarder_red));

            } else if (rate >= 0.5 && rate < 1) {
                viewHolder.star_red.setVisibility(View.VISIBLE);
                viewHolder.star_orange.setVisibility(View.GONE);
                viewHolder.star_green.setVisibility(View.GONE);
                viewHolder.rate_txt_boarder.setBackground(mContext.getResources().getDrawable(R.drawable.oval_boarder_red));

            } else if (rate >= 1 && rate < 1.5) {
                viewHolder.star_red.setVisibility(View.VISIBLE);
                viewHolder.star_orange.setVisibility(View.GONE);
                viewHolder.star_green.setVisibility(View.GONE);
                viewHolder.rate_txt_boarder.setBackground(mContext.getResources().getDrawable(R.drawable.oval_boarder_red));

            } else if (rate >= 1.5 && rate < 2) {
                viewHolder.star_red.setVisibility(View.VISIBLE);
                viewHolder.star_orange.setVisibility(View.GONE);
                viewHolder.star_green.setVisibility(View.GONE);
                viewHolder.rate_txt_boarder.setBackground(mContext.getResources().getDrawable(R.drawable.oval_boarder_red));

            } else if (rate >= 2 && rate < 2.5) {
                viewHolder.star_red.setVisibility(View.VISIBLE);
                viewHolder.star_orange.setVisibility(View.GONE);
                viewHolder.star_green.setVisibility(View.GONE);
                viewHolder.rate_txt_boarder.setBackground(mContext.getResources().getDrawable(R.drawable.oval_boarder_red));

            } else if (rate >= 2.5 && rate < 3) {
                viewHolder.star_red.setVisibility(View.VISIBLE);
                viewHolder.star_orange.setVisibility(View.GONE);
                viewHolder.star_green.setVisibility(View.GONE);
                viewHolder.rate_txt_boarder.setBackground(mContext.getResources().getDrawable(R.drawable.oval_boarder_red));

            } else if (rate >= 3 && rate < 3.5) {
                viewHolder.star_orange.setVisibility(View.VISIBLE);
                viewHolder.star_red.setVisibility(View.GONE);
                viewHolder.star_green.setVisibility(View.GONE);
                viewHolder.rate_txt_boarder.setBackground(mContext.getResources().getDrawable(R.drawable.oval_boarder_orange));

            } else if (rate >= 3.5 && rate < 4) {
                viewHolder.star_orange.setVisibility(View.VISIBLE);
                viewHolder.star_red.setVisibility(View.GONE);
                viewHolder.star_green.setVisibility(View.GONE);
                viewHolder.rate_txt_boarder.setBackground(mContext.getResources().getDrawable(R.drawable.oval_boarder_orange));

            } else if (rate >= 4 && rate < 4.5) {
                viewHolder.star_green.setVisibility(View.VISIBLE);
                viewHolder.star_red.setVisibility(View.GONE);
                viewHolder.star_orange.setVisibility(View.GONE);
                viewHolder.rate_txt_boarder.setBackground(mContext.getResources().getDrawable(R.drawable.oval_boarder_green));

            } else if (rate >= 4.5 && rate < 5) {
                viewHolder.star_green.setVisibility(View.VISIBLE);
                viewHolder.star_red.setVisibility(View.GONE);
                viewHolder.star_orange.setVisibility(View.GONE);
                viewHolder.rate_txt_boarder.setBackground(mContext.getResources().getDrawable(R.drawable.oval_boarder_green));

            } else if (rate == 5) {
                viewHolder.star_green.setVisibility(View.VISIBLE);
                viewHolder.star_red.setVisibility(View.GONE);
                viewHolder.star_orange.setVisibility(View.GONE);
                viewHolder.rate_txt_boarder.setBackground(mContext.getResources().getDrawable(R.drawable.oval_boarder_green));
            }

        } catch (JSONException j) {
            j.printStackTrace();
        }
        return convertView;
    }


    public class MyOrderVH {

        CustomTextView tv_rate, review_name, review_txt;
        ImageView star_orange, star_green, star_red;
        LinearLayout rate_txt_boarder;

        public MyOrderVH(View v) {

            tv_rate = (CustomTextView) v.findViewById(R.id.tv_rate);
            review_name = (CustomTextView) v.findViewById(R.id.review_name);
            review_txt = (CustomTextView) v.findViewById(R.id.review_txt);

            star_red = (ImageView) v.findViewById(R.id.star_red);
            star_orange = (ImageView) v.findViewById(R.id.star_orange);
            star_green = (ImageView) v.findViewById(R.id.star_green);
            rate_txt_boarder = (LinearLayout) v.findViewById(R.id.rate_txt_boarder);
        }

    }
}
