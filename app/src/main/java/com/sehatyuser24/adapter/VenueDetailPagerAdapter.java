package com.sehatyuser24.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.sehatyuser24.R;

import org.json.JSONArray;
import org.json.JSONException;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by and-04 on 3/8/17.
 */

public class VenueDetailPagerAdapter extends PagerAdapter {
    Context mContext;
    JSONArray js;

    public VenueDetailPagerAdapter(Context context, JSONArray j) {
        this.mContext = context;
        this.js = j;
    }

    @Override
    public int getCount() {
        return js.length();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);

        final View layout = inflater.inflate(R.layout.venue_detail_image, container, false);

        ImageView imageView = (ImageView) layout.findViewById(R.id.detailimage);
        try {
            System.out.println("IMAGELIST -----" + js.getString(position));
            Glide.with(mContext).load(js.getJSONObject(position).getString("image")).
                    listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            layout.findViewById(R.id.pb).setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            layout.findViewById(R.id.pb).setVisibility(View.GONE);
                            return false;
                        }
                    }).
                    into(imageView);
//                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        } catch (JSONException j) {
            j.printStackTrace();
        }
           /* if (js.equals(null) || js.length() == 0) {

            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    try {
                        Glide.with(mContext).load(js.getString(position)).into(imageView);
                    } catch (JSONException j) {
                        j.printStackTrace();
                    }
                }
            }*/
        container.addView(layout);
        return layout;
    }
}
