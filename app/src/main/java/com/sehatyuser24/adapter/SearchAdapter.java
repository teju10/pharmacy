package com.sehatyuser24.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.sehatyuser24.R;
import com.sehatyuser24.activity.other.ActivityProductDetail;
import com.sehatyuser24.activity.other.Activity_SearchingMedicine;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.utility.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Tejas on 3/6/17.
 */

public class SearchAdapter extends BaseAdapter {

    Context mContext;
    List<JSONObject> jlist;
    LayoutInflater li;
    Activity_SearchingMedicine activity;

    public SearchAdapter(Activity_SearchingMedicine activity, Context context, List<JSONObject> list) {

        this.activity = activity;
        this.mContext = context;
        this.jlist = list;
        li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return jlist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = li.inflate(R.layout.search_single_text, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        try {
            JSONObject object = jlist.get(position);
            viewHolder.search_single_txt.setText(object.getString("title"));
            convertView.setOnClickListener(new SingleClick(position));
        } catch (JSONException j) {
            j.printStackTrace();
        }

        return convertView;
    }

    public class SingleClick implements View.OnClickListener {
        int pos;

        public SingleClick(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            try {
                mContext.startActivity(new Intent(mContext, ActivityProductDetail.class).putExtra
                        (Constants.PRODUCTID, jlist.get(pos).getString("product_id").toString()));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public class ViewHolder {
        CustomTextView search_single_txt;

        public ViewHolder(View v) {
            search_single_txt = (CustomTextView) v.findViewById(R.id.search_single_txt);
        }
    }
}
