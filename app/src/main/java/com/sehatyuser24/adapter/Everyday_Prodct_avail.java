package com.sehatyuser24.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.sehatyuser24.R;
import com.sehatyuser24.activity.other.Activity_Medicine_Subcategory;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.utility.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by and-04 on 20/9/17.
 */

public class Everyday_Prodct_avail extends BaseAdapter {

    Context mContext;
    List<JSONObject> datalist;
    LayoutInflater li;

    public Everyday_Prodct_avail(Context mContext, List<JSONObject> list) {
        this.mContext = mContext;
        this.datalist = list;
        li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return datalist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder v;

        if (convertView == null) {
            convertView = li.inflate(R.layout.adap_everyday_prdct_item, null);
            v = new ViewHolder(convertView);
            convertView.setTag(v);
        } else {
            v = (ViewHolder) convertView.getTag();
        }
        try {

            JSONObject object = datalist.get(position);
            v.everyday_id.setText(object.getString("category"));

            convertView.setOnClickListener(new ItemClick(position));

        } catch (JSONException j) {
            j.printStackTrace();
        }

        return convertView;
    }

    public class ItemClick implements View.OnClickListener {
        int pos = 0;

        public ItemClick(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            try {
                Intent i = new Intent(mContext, Activity_Medicine_Subcategory.class);
                i.putExtra(Constants.CATEGORYID, datalist.get(pos).getString("cat_id"));
                i.putExtra(Constants.CATNAME, datalist.get(pos).getString("category"));

                mContext.startActivity(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public class ViewHolder {
        CustomTextView everyday_id;

        public ViewHolder(View b) {
            everyday_id = (CustomTextView) b.findViewById(R.id.everyday_id);
        }
    }
}
