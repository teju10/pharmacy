package com.sehatyuser24.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.sehatyuser24.R;
import com.sehatyuser24.customwidget.CustomTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by and-04 on 17/10/17.
 */

public class Dosage_Adap extends BaseAdapter {

    Context mContext;
    ArrayList<JSONObject> datalist;
    LayoutInflater li;

    public Dosage_Adap(Context mcon, ArrayList<JSONObject> mlist) {
        this.mContext = mcon;
        this.datalist = mlist;
        li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        System.out.println("LST SIZE===>" + datalist.size());
        System.out.println("LST SIZE===>" + datalist);
        return datalist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder v;

        if (convertView == null) {
            convertView = li.inflate(R.layout.adap_dosage_item, null);
            v = new ViewHolder(convertView);
            convertView.setTag(v);
        } else {
            v = (ViewHolder) convertView.getTag();
        }
        try {
            JSONObject object = datalist.get(position);
            v.dos_medname.setText(object.getString("title"));
            v.dosage.setText(object.getString("dose"));

//            convertView.setOnClickListener(new ItemClick(position));

        } catch (JSONException j) {
            j.printStackTrace();
        }

        return convertView;
    }

    public class ViewHolder {
        CustomTextView dos_medname, dosage;

        public ViewHolder(View b) {
            dos_medname = (CustomTextView) b.findViewById(R.id.dos_item_medname);
            dosage = (CustomTextView) b.findViewById(R.id.dos_item_disc);
        }
    }
}
