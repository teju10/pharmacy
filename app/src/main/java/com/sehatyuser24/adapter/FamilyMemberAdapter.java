package com.sehatyuser24.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.sehatyuser24.R;

import com.sehatyuser24.activity.other.Activity_AddFamily_Mem_Account;
import com.sehatyuser24.customwidget.CustomButton;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.drawerfragment.FamillyFragment;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Tejas on 1/6/17.
 */

public class FamilyMemberAdapter extends BaseAdapter {
    Context mContext;
    List<JSONObject> datalist;
    LayoutInflater li;
    FamillyFragment myfrag;


    public FamilyMemberAdapter(FamillyFragment fragment, Context apContext, List<JSONObject> list) {
        this.myfrag = fragment;
        this.mContext = apContext;
        this.datalist = list;
        li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return datalist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final MyOrderVH viewHolder;
        if (convertView == null) {
            convertView = li.inflate(R.layout.familymember_list_single_item, null);
            viewHolder = new MyOrderVH(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (MyOrderVH) convertView.getTag();
        }
        try {
            JSONObject object = datalist.get(position);
            viewHolder.f_mem_name.setText(object.getString("fname"));
            if (object.getString("sex").equals("0")) {
                viewHolder.f_mem_sex.setText(mContext.getResources().getString(R.string.male));
            } else {
                viewHolder.f_mem_sex.setText(mContext.getResources().getString(R.string.female));
            }
            viewHolder.f_mem_age.setText(object.getString("age"));
            viewHolder.f_mem_relation.setText(object.getString("relation"));
            Glide.with(mContext).
                    load(object.getString("image")).
                    listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            viewHolder.fm_progress.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            viewHolder.fm_progress.setVisibility(View.GONE);
                            return false;
                        }
                    }).
                    into(viewHolder.f_mem_img);
            viewHolder.f_mem_view_btn.setOnClickListener(new ItemViewClick(position));
            viewHolder.f_mem_edit_account_btn.setOnClickListener(new ItemEditClick(position));

        } catch (JSONException j) {
            j.printStackTrace();
        }

        return convertView;
    }

    public class ItemEditClick implements View.OnClickListener {
        int pos;

        public ItemEditClick(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            try {
                Utility.setIntegerSharedPreference(mContext, Constants.SP_FAMILYMEM, 2);
                Bundle b = new Bundle();
                Intent i = new Intent(mContext, Activity_AddFamily_Mem_Account.class);
                b.putString(Constants.FAMILYMEMBERSINGLEVIEW, datalist.get(pos).toString());
                i.putExtras(b);
                mContext.startActivity(i);
            } catch (Exception j) {
                j.printStackTrace();
            }
        }
    }

    public class ItemViewClick implements View.OnClickListener {
        int pos;

        public ItemViewClick(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            try {
                Utility.setIntegerSharedPreference(mContext, Constants.SP_FAMILYMEM, 3);
                Bundle b = new Bundle();
                Intent i = new Intent(mContext, Activity_AddFamily_Mem_Account.class);
                b.putString(Constants.FAMILYMEMBERSINGLEVIEW, datalist.get(pos).toString());
                i.putExtras(b);
                mContext.startActivity(i);
            } catch (Exception j) {
                j.printStackTrace();
            }
        }
    }

    public class MyOrderVH {
        ImageView f_mem_img;
        CustomTextView f_mem_name, f_mem_sex, f_mem_age, f_mem_relation;
        ProgressBar fm_progress;
        CustomButton f_mem_view_btn, f_mem_edit_account_btn;

        public MyOrderVH(View v) {
            f_mem_img = (ImageView) v.findViewById(R.id.f_mem_img);
            f_mem_name = (CustomTextView) v.findViewById(R.id.f_mem_name);
            f_mem_sex = (CustomTextView) v.findViewById(R.id.f_mem_sex);
            f_mem_age = (CustomTextView) v.findViewById(R.id.f_mem_age);
            f_mem_relation = (CustomTextView) v.findViewById(R.id.f_mem_relation);
            f_mem_view_btn = (CustomButton) v.findViewById(R.id.f_mem_view_btn);
            f_mem_edit_account_btn = (CustomButton) v.findViewById(R.id.f_mem_edit_account_btn);
            fm_progress = (ProgressBar) v.findViewById(R.id.fm_progress);
        }

    }
}
