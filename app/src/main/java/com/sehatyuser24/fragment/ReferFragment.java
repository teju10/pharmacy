package com.sehatyuser24.fragment;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.AppInviteDialog;
import com.facebook.share.widget.ShareDialog;
import com.sehatyuser24.R;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.Constant_Urls;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by and-04 on 14/6/17.
 */

public class ReferFragment extends Fragment {

    Context mContext;
    View rootview;
    ResponseTask rt;
    String Promocode = "", appLinkUrl;
    ImageView sign_up_msg;
    AppInviteDialog shareDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.frag_referapp, container, false);

        super.onCreate(savedInstanceState);

        mContext = getActivity();
        FacebookSdk.sdkInitialize(getActivity());
        appLinkUrl = "http://www.infograins.in/INFO01/Pharm01/admin/orders_list.php" + Promocode;
        sign_up_msg = (ImageView) rootview.findViewById(R.id.sign_up_msg);

        if (Utility.getSharedPreferences(mContext, Constants.LANG_PREF).equals("en")) {
            sign_up_msg.setImageDrawable(mContext.getResources().getDrawable(R.drawable.icon_sign_icon));
        } else {
            sign_up_msg.setImageDrawable(mContext.getResources().getDrawable(R.drawable.icon_sign_icon_ar));
        }

        rootview.findViewById(R.id.share_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Share();
            }
        });


        if (Utility.isConnectingToInternet(mContext)) {
            GetPromoCode();
        } else {
            Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
        }

        return rootview;
    }

    public void GetPromoCode() {

        // http://www.infograins.in/INFO01/Pharm01/api/api.php?action=SharePromoCode&userid=141

        try {
            JSONObject j = new JSONObject();
            j.put(Constants.ACTION, Constants.SHAREPROMOCODE);
            j.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(mContext, j);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Ttoast.show(mContext, mContext.getResources().getString(R.string.server_fail), false);
                    } else {
                        try {
                            JSONObject j = new JSONObject(result);
                            if (j.getString("success").equals("1")) {
                                Promocode = j.getString("promo_code");
                                ((CustomTextView) rootview.findViewById(R.id.promo_code)).setText(j.getString("promo_code"));
                            } else {

                            }

                        } catch (JSONException h) {
                            h.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException g) {
            g.printStackTrace();
        }

    }

   /* public void Share(String urlshare){
        try {
            Intent intent1 = new Intent();
            intent1.setClassName("com.facebook.katana", "com.facebook.katana.activity.composer.ImplicitShareIntentHandler");
            intent1.setAction("android.intent.action.SEND");
            intent1.setType("text/plain");
            intent1.putExtra("android.intent.extra.TEXT", urlshare);
            startActivity(intent1);
        } catch (Exception e) {
            // If we failed (not native FB app installed), try share through SEND
            String sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=" + urlshare;
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
            startActivity(intent);
        }
        }*/

    public void Share() {

        List<Intent> targetShareIntents = new ArrayList<Intent>();
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        PackageManager pm = getActivity().getPackageManager();
        List<ResolveInfo> resInfos = pm.queryIntentActivities(shareIntent, 0);
        if (!resInfos.isEmpty()) {
            for (ResolveInfo resInfo : resInfos) {
                String packageName = resInfo.activityInfo.packageName;
                Log.w("Package Name", packageName);

                if (packageName.contains("com.facebook.katana")
                        ||
//                        || packageName.contains("com.google.android.talk")
                        (packageName.contains("com.google.android.gm")
                                || packageName.contains("com.facebook.orca")
                                || packageName.contains("com.whatsapp")
                       /* || packageName.contains("com.google.android.apps.plus")
                        || packageName.contains("com.google.android.talk")
                        || packageName.contains("com.slack")
                        || packageName.contains("com.yahoo.mobile")
                        || packageName.contains("com.skype.raider")
                        || packageName.contains("com.android.mms")
                        || packageName.contains("com.linkedin.android")
                        || packageName.contains("com.google.android.apps.messaging")*/)) {
                   /* shareDialog = new AppInviteDialog(this);
                    if (shareDialog.canShow()) {
                        AppInviteContent content = new AppInviteContent.Builder()
                                .setApplinkUrl(appLinkUrl)
                                .build();
                        shareDialog.show(this, content);*/

                    Intent intent = new Intent();
                    intent.setComponent(new ComponentName(packageName, resInfo.activityInfo.name));
                    intent.putExtra("AppName", resInfo.loadLabel(pm).toString());
                    intent.setAction(Intent.ACTION_SEND);
                    intent.setType("text/plain");

                    intent.putExtra(Intent.EXTRA_SUBJECT, "Sehaty24");

                   /* String sAux = "\nAccept my invite to download the Sehaty24 App and get \"flat 10% "+
                            "OFF on purchase of Prescription Medicines worth Rs. 500.\""+
                            "Your promo code is: \n" + Promocode;*/
                    String Aux = "http://www.infograins.in/INFO01/Pharm01/admin/orders_list.php?referral=" + Promocode;

                    StringBuffer ap = new StringBuffer();
                    ap.append(Aux);
                    intent.putExtra(Intent.EXTRA_TEXT, Aux);
                    intent.setPackage(packageName);
                    targetShareIntents.add(intent);
                }
            }

            if (!targetShareIntents.isEmpty()) {
                Collections.sort(targetShareIntents, new Comparator<Intent>() {
                    @Override
                    public int compare(Intent o1, Intent o2) {
                        return o1.getStringExtra("AppName").compareTo(o2.getStringExtra("AppName"));
                    }
                });

                Intent chooserIntent = Intent.createChooser(targetShareIntents.remove(0), mContext.getResources().getString(R.string.select_app_share));
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetShareIntents.toArray(new Parcelable[]{}));
                startActivity(chooserIntent);
            } else {
                Ttoast.show(getActivity(), mContext.getResources().getString(R.string.no_app_share), false);
            }

        }
    }

}
