package com.sehatyuser24.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sehatyuser24.R;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.CircularProgressBar;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by and-04 on 14/6/17.
 */

public class EarningPointFragment extends Fragment implements View.OnClickListener {

    View rootview;
    Context mContext;
    ResponseTask rt;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Utility.ChangeLang(getActivity(),Utility.getIngerSharedPreferences(getActivity(),Constants.LANG));

        rootview = inflater.inflate(R.layout.frag_earnpoint_progressbar, container, false);

        mContext = getActivity();

        if (Utility.isConnectingToInternet(mContext)) {
            GetRewardPoints();
        } else {
            Ttoast.show(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
        }

        rootview.findViewById(R.id.get_points).setOnClickListener(this);
        rootview.findViewById(R.id.ur_reward_points).setOnClickListener(this);

        return rootview;
    }

    public void GetRewardProgress(int points) {
        final CircularProgressBar c2 = (CircularProgressBar) rootview.findViewById(R.id.cprgress);
        c2.animateProgressTo(0, points, new CircularProgressBar.ProgressAnimationListener() {

            @Override
            public void onAnimationStart() {

            }

            @Override
            public void onAnimationProgress(int progress) {
                c2.setTitle(progress + "");
            }

            @Override
            public void onAnimationFinish() {
                c2.setSubTitle(mContext.getResources().getString(R.string.total_earn_point));
            }
        });
    }

    public void GetRewardPoints() {

        try {
            JSONObject j = new JSONObject();
            j.put(Constants.ACTION, Constants.GETWALLETAMOUNT);
            j.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
//            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));

            rt = new ResponseTask(mContext, j);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
//                    Utility.HideDialog();
                    try {
                        if (result == null) {
                            Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.server_fail), false);
                        }
                        JSONObject jo = new JSONObject(result);
                        if (jo.getString("success").equals("1")) {
                            if ((jo.getString("wallet_amount").equals("0"))) {
                                GetRewardProgress(0);
                                Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.did_not_reward), false);
                            } else {
                                GetRewardProgress(Integer.parseInt(jo.getString("wallet_amount")));
                            }

                            int WalletPoint = 100 - Integer.parseInt(jo.getString("wallet_amount"));

                            if (Utility.getSharedPreferences(mContext, Constants.LANG_PREF).equals("en")) {
                                ((CustomTextView) rootview.findViewById(R.id.remaining_points_txt)).
                                        setText("Only " + WalletPoint + " Points to the next reward");
                            } else {
                                ((CustomTextView) rootview.findViewById(R.id.remaining_points_txt)).
                                        setText(" فقط " + WalletPoint + "النقاط إلى المكافأة التالية ");
                            }

                        } else {
                            Ttoast.show(mContext, jo.getString("msg"), false);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            rt.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.ur_reward_points:
                Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.c_soon), false);
                break;

            case R.id.get_points:
                Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.c_soon), false);
                break;
        }

    }

}
