package com.sehatyuser24;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.sql.SQLData;
import java.util.ArrayList;

/**
 * Created by and-04 on 25/7/17.
 */

public class DBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "UploadImage.db";
    private static final String TABLE_NAME = "uploadimage";
    // Contacts table name
    private static final String KEY_ID = "id";
    private static final String Image1 = "image1";
    private static final String Image2 = "image2";
    private static final String Image3 = "image3";
    private static final String Image4 = "image4";
    private static final String Image5 = "image5";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," // and auto increment will be handled with primary key
                + Image1 + " TEXT ,"
                + Image2 + " TEXT ,"
                + Image3 + " TEXT ,"
                + Image4 + " TEXT ,"
                + Image5 + " TEXT" + ");";
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        Log.w("DBHandler.Class", "Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        onCreate(db);

    }

    public void InsertImage(String img1, String img2, String img3, String img4, String img5) {
        SQLiteDatabase db = getWritableDatabase();
        //db.execSQL("insert into mstUserMobAppSurvey");
        ContentValues cv = new ContentValues();
        cv.put(Image1, img1);
        cv.put(Image2, img2);
        cv.put(Image3, img3);
        cv.put(Image4, img4);
        cv.put(Image5, img5);

        db.insert(TABLE_NAME, null, cv);
        db.close(); // Closing database connection
    }

    public ArrayList<String> GetAllImage() {

        ArrayList<String> shopList = new ArrayList<String>();
        try {
            String selectQuery = "SELECT * FROM " + TABLE_NAME;
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        shopList.add(cursor.getString(1));
                        shopList.add(cursor.getString(2));
                        shopList.add(cursor.getString(3));
                        shopList.add(cursor.getString(4));
                        shopList.add(cursor.getString(5));

                    } while (cursor.moveToNext());
                    System.out.println("ARRAYLIST=====>SHOPLIST" + shopList);
                }

            } else {
                System.out.println("DATABASE CURSOR EMPTY=======>");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return shopList;
    }

    public void clearDatabase() {
        SQLiteDatabase db = this.getReadableDatabase();
        String clearDBQuery = "DELETE FROM "+TABLE_NAME;
        db.execSQL(clearDBQuery);
    }
}
