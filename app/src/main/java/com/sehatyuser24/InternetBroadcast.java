package com.sehatyuser24;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;

import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.drawerfragment.UploadFragment;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.MultiPartFileUpload;
import com.sehatyuser24.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.util.ArrayList;

/**
 * Created by and-04 on 25/7/17.
 */

public class InternetBroadcast extends BroadcastReceiver {

    DBHelper db;
    Context mContext;
    ArrayList<String> mainarraylist;
    ArrayList<String> newimg = new ArrayList<String>();
    String path = "";
    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;
        db = new DBHelper(context);

        if (Utility.isConnectingToInternet(context)) {
            upload(db);
        } else {

        }

    }

    /*public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }*/

    public void upload(DBHelper db) {
        mainarraylist = new ArrayList<>();
        mainarraylist = db.GetAllImage();
        System.out.println("IMAGEGETALL=======>" + mainarraylist);
        if (mainarraylist.size() > 0&&!mainarraylist.equals("")) {
            new UploadPrescription().execute();
        }
       /* for (int i = 0; i < mainarraylist.size(); i++) {
            // Blob blob = mainarraylist.get(i).getBlob("image_data"); //This line gets the image's blob data
            //image = blob.getBytes(0, blob.length);
            byte[] b = Base64.decode(mainarraylist.get(i).getBytes(), i);*//*Base64(imageString.getBytes())*//*

            try {
                System.out.println("IMAGESIZE======>" + mainarraylist.get(i));
                Bitmap bmp = BitmapFactory.decodeByteArray(b, 0, b.length);
                // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                Uri tempUri = getImageUri(mContext, bmp);
                // CALL THIS METHOD TO GET THE ACTUAL PATH
              String Pathname =  Utility.SaveImage(bmp,String.valueOf(System.currentTimeMillis()),tempUri.toString());
//                File finalFile = new File(getRealPathFromURI(tempUri));
                newimg.add(Pathname);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("CURSOR ERROR=>"+e);
            }
        }*/

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        OutputStream fOut = null;
        String strDirectory = Environment.getExternalStorageDirectory().toString();


        File f = new File(strDirectory, "Sehaty24");
        try {
            fOut = new FileOutputStream(f);
            /**Compress image**/
            inImage.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
            fOut.flush();
            fOut.close();
            path = f.getAbsolutePath();
            System.out.println("----->>>>>>>>>" + path);
            /**Update image to gallery**/
            MediaStore.Images.Media.insertImage(inContext.getContentResolver(),
                    f.getAbsolutePath(), f.getName(), f.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }


        // String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        int idx = 0;
        Cursor cursor = mContext.getContentResolver().query(uri, null, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
             idx= cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            }
        }
        return cursor.getString(idx);
    }

    public class UploadPrescription extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            Utility.ShowLoading(this, mContext.getResources().getString(R.string.loading_msg));
        }

        @Override
        protected String doInBackground(String... params) {

/*http://infograins.com.208-91-199-7.md-plesk-web2.webhostbox.net/INFO01/pharmacy/api/api.php?action=
UploadPrescription&product_id=1&prescription_image=files_type*/
            try {
                MultiPartFileUpload multipart = new MultiPartFileUpload(mContext, "UTF-8");
                multipart.addFormField(Constants.ACTION, Constants.UPLOADPRESCRIPTION);
                multipart.addFormField(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
                if (mainarraylist.size() > 0) {
                    if (!(mainarraylist.size() == 0 && (mainarraylist != null))) {
                        for (int i = 0; i < (mainarraylist.size()); i++) {
                            if (!(mainarraylist.get(i).equals("") && (mainarraylist.get(i) != null))) {
                                multipart.addFilePart("prescription_image[]", new File(mainarraylist.get(i)));
                                System.out.println("sourceFile[i] " + mainarraylist.get(i));
                            }
                        }
                        // multipart.addFilePart("prescription_image[]",newimg.toString());
                        // System.out.println("sourceFile[i] " + mainarraylist);
                    }

                }
                return multipart.finish();
            } catch (Exception e) {
                e.printStackTrace();

                return mContext.getResources().getString(R.string.server_fail);
            }
        }

        @Override
        protected void onPostExecute(String result) {
//            Utility.HideDialog();
            System.out.println("Server result ====> " + result);
            if (result == null) {
                Ttoast.show(mContext, mContext.getResources().getString(R.string.server_fail), true);

            } else {
                try {
                    JSONObject jobj = new JSONObject(result);
                    if (jobj.get(Constants.RESULT_KEY).equals("1")) {
                        db.clearDatabase();
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.upload_presc_success), false);
                    } else {
//                        db.clearDatabase();
//                        Utility.showCroutonWarn(jobj.getString(Constants.SERVER_MSG), getActivity());
                    }
                } catch (JSONException e) {
//                    db.clearDatabase();
                }
            }
            super.onPostExecute(result);
        }

    }
}
