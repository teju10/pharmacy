package com.sehatyuser24.drawerfragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.sehatyuser24.R;
import com.sehatyuser24.adapter.VenueDetailPagerAdapter;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Utility;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by nilesh on 11/4/17.
 */

public class ViewOfferFragment extends Fragment {

    Context mContext;
    Toolbar toolbar;
    ViewPager offer_pager;
    CirclePageIndicator offer_indicator;
    View rootView;
    ResponseTask rt;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_offer, container, false);

        mContext = getActivity();
        toolbar = (Toolbar) rootView.findViewById(R.id.toolbarprovidernav2);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.offer));

        ((ImageView) getActivity().findViewById(R.id.imag_tb)).setVisibility(View.GONE);

        offer_pager = (ViewPager) rootView.findViewById(R.id.offer_pager);
        offer_indicator = (CirclePageIndicator) rootView.findViewById(R.id.offer_indicator);

        if (Utility.isConnectingToInternet(mContext)) {
            Getoffertask();
        } else {
            Ttoast.show(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
        }
        return rootView;
    }

    public void Getoffertask() {
        try {

            final JSONObject jo = new JSONObject();
            jo.put(Constants.ACTION, Constants.OFFERLIST);
            jo.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));

            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Ttoast.show(mContext, mContext.getResources().getString(R.string.server_fail), false);
                    } else {
                        try {
                            JSONObject j = new JSONObject(result);
                            if (j.getString("success").equals("1")) {
                                offer_pager.setAdapter(new VenueDetailPagerAdapter(mContext, j.getJSONArray(Constants.OBJECT)));
                                offer_indicator.setViewPager(offer_pager);
                                offer_pager.setCurrentItem(0);
                            }
                        } catch (JSONException j) {
                            j.printStackTrace();
                        }
                    }

                }
            });
            rt.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
