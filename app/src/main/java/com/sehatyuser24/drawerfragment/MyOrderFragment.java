package com.sehatyuser24.drawerfragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.sehatyuser24.R;
import com.sehatyuser24.adapter.MyOrderAdapter;
import com.sehatyuser24.customwidget.LoadMoreListView;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Utility;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tejas on 11/4/17.
 */

public class MyOrderFragment extends Fragment {
    Context mContext;
    Toolbar toolbar;
    MyOrderAdapter myOrderAdapter;
    LoadMoreListView my_order_list;
    View rootView;
    int page = 0;
    ResponseTask rt;
    List<JSONObject> list = new ArrayList<>();
    Boolean MORERESULT = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Utility.ChangeLang(getActivity(),Utility.getIngerSharedPreferences(getActivity(),Constants.LANG));

        rootView = inflater.inflate(R.layout.fragment_order, container, false);
        mContext = getActivity();
        toolbar = (Toolbar) rootView.findViewById(R.id.toolbarprovidernav2);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.my_order));
        ((ImageView) getActivity().findViewById(R.id.imag_tb)).setVisibility(View.GONE);

        Find();

        return rootView;
    }

    public void Find() {
        my_order_list = (LoadMoreListView) rootView.findViewById(R.id.my_order_list);
        if (Utility.isConnectingToInternet(mContext)) {
            GetOrderListTask();
        } else {
            Ttoast.show(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
        }
        Init();

    }

    public void Init() {
        Utility.hideKeyboard(mContext);
        myOrderAdapter = new MyOrderAdapter(this, mContext, list);
        my_order_list.setAdapter(myOrderAdapter);

        my_order_list.setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {

                if (Utility.isConnectingToInternet(mContext)) {
                    if (MORERESULT) {
                        page = page + 1;
                        GetOrderListTask();
                    } else {
                        my_order_list.onLoadMoreComplete();
                    }
                } else {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
                }
            }
        });

    }

    public void GetOrderListTask() {
        try {
            JSONObject k = new JSONObject();
            k.put(Constants.ACTION, Constants.GETORDERLIST);
            k.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
            k.put(Constants.PAGE, page);
            if (page == 0) {
                Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
            }
            rt = new ResponseTask(mContext, k);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    if (page == 0) {
                        Utility.HideDialog();
                    }
                    my_order_list.onLoadMoreComplete();
                    if (result == null) {
                        page = page - 1;
                        Ttoast.show(mContext, mContext.getResources().getString(R.string.server_fail), false);
                    } else {
                        try {
                            JSONObject j = new JSONObject(result);
                            if (j.getString("success").equals("1")) {
//                                list.clear();
                                JSONArray jsonArray = j.getJSONArray(Constants.OBJECT);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    list.add(jsonArray.getJSONObject(i));
                                }
                                myOrderAdapter.notifyDataSetChanged();
                            } else {
                                MORERESULT = false;
                                Ttoast.ShowToast(mContext, j.getString(Constants.SERVER_MSG), false);
                            }
                        } catch (JSONException e) {
                            MORERESULT = false;
                            page = page - 1;
                            e.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException h) {
            h.printStackTrace();
        }
    }

}
