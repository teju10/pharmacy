package com.sehatyuser24.drawerfragment;

import android.content.Context;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.sehatyuser24.R;
import com.sehatyuser24.adapter.MedicationSingleViewAdapter;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.fastscroll_list.RecyclerViewFastScroller;
import com.sehatyuser24.fastscroll_list.models.AlphabetItem;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.Constant_Urls;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by nilesh on 11/4/17.
 */

public class MedicationFragment extends Fragment {
    ResponseTask rt;
    Toolbar toolbar;
    RecyclerView mRecyclerView;
    RecyclerViewFastScroller fastScroller;
    View rootView;
    Context mContext;
    private List<String> mDataArray = new ArrayList<>();
    private List<AlphabetItem> mAlphabetItems;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Utility.ChangeLang(getActivity(),Utility.getIngerSharedPreferences(getActivity(),Constants.LANG));

        rootView = inflater.inflate(R.layout.fragment_medication, container, false);
        mContext = getActivity();
        toolbar = (Toolbar) rootView.findViewById(R.id.toolbarprovidernav2);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.prescri));
        ((ImageView) getActivity().findViewById(R.id.imag_tb)).setVisibility(View.GONE);

        Find();

        return rootView;
    }

    public void Find() {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.my_recycler_view);
        fastScroller = (RecyclerViewFastScroller) rootView.findViewById(R.id.fast_scroller);
        if (Utility.isConnectingToInternet(mContext)) {
            GetMedicationListTask();
        } else {
            Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
        }

    }

    public void METHOD() {
        mAlphabetItems = new ArrayList<>();

        String name = "";
        List<String> strAlphabets = new ArrayList<>();
        for (int i = 0; i < mDataArray.size(); i++) {
            String cat = mDataArray.get(i);
            try {
                JSONObject j = new JSONObject(cat);
                name = j.getString("category");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (name == null || name.trim().isEmpty())
                continue;

            String word = name.substring(0, 1);
            if (!strAlphabets.contains(word)) {
                strAlphabets.add(word);
                mAlphabetItems.add(new AlphabetItem(i, word, false));
            }
        }
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(new MedicationSingleViewAdapter(mContext,mDataArray));

        fastScroller.setRecyclerView(mRecyclerView);
        fastScroller.setUpAlphabet(mAlphabetItems);

    }

    public void GetMedicationListTask() {
        try {
            JSONObject j = new JSONObject();
            j.put(Constants.ACTION, Constants.MEDICATIONLIST);
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(mContext, j);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {
                        if (result == null) {
                            Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.server_fail), false);
                        } else {
                            try {
                                JSONObject jo = new JSONObject(result);
                                if (jo.getString("success").equals("1")) {
                                    JSONArray jsonArray = jo.getJSONArray(Constants.OBJECT);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        mDataArray.add(jsonArray.getString(i));
                                    }
                                    METHOD();
                                }
                            } catch (JSONException j) {
                                j.printStackTrace();
                            }
                        }
                    } catch (Exception g) {
                        g.printStackTrace();
                    }
                }
            });
            rt.execute();
        } catch (JSONException j) {
            j.printStackTrace();
        }
    }

}
