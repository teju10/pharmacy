package com.sehatyuser24.drawerfragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.sehatyuser24.R;
import com.sehatyuser24.activity.other.ActivityProductDetail;
import com.sehatyuser24.activity.other.Activity_AddtoCart_List;
import com.sehatyuser24.activity.other.Activity_Everyday_Medical_Product;
import com.sehatyuser24.activity.other.Activity_Medicine_Subcategory;
import com.sehatyuser24.activity.other.Activity_SearchingMedicine;
import com.sehatyuser24.activity.registration.LoginActivity;
import com.sehatyuser24.activity.registration.MainActivity;
import com.sehatyuser24.adapter.ImageAdapter;
import com.sehatyuser24.adapter.VenueDetailPagerAdapter;
import com.sehatyuser24.customwidget.CustomEditText;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Util;
import com.sehatyuser24.utility.Utility;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import permissions.dispatcher.NeedsPermission;

/**
 * Created by Infograins on 12/24/2016.
 */

public class HomeFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = HomeFragment.class.getSimpleName();
    private static int count = 0;
    public int maxcount = 5;
    public ArrayList<String> filePaths = new ArrayList<>();
    Toolbar toolbar;
    ViewPager mPager;
    CirclePageIndicator indicator;
    View rootView;
    Context mContext;
    ResponseTask rt;
    Dialog dialog_refercode;
    ArrayList<JSONObject> category_list = new ArrayList<>();
    ArrayList<JSONObject> popular_list = new ArrayList<>();
    ArrayList<JSONObject> sessional_list = new ArrayList<>();
    TopSellingHorizontalListAdapter adap;
    PopularHorizontalListAdapter adap2;
    SessionalHorizontalListAdapter adap3;
    RecyclerView top_selling_horizontal, popolar_horizontal, sessional_horizontal;
    LinearLayoutManager horizontalLayoutManager;
    LinearLayoutManager horizontalLayoutManager1;
    LinearLayoutManager horizontalLayoutManager2;
    View presc_medication_btn;
    private int MAX_ATTACHMENT_COUNT = 5;
    private ArrayList<String> photoPaths = new ArrayList<>();
    private ArrayList<String> docPaths = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mContext = getActivity();
        count = Utility.getIngerSharedPreferences(mContext, Constants.COUNT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Utility.ChangeLang(getActivity(), Utility.getIngerSharedPreferences(getActivity(), Constants.LANG));
        rootView = inflater.inflate(R.layout.fragment_home, container, false);
        toolbar = (Toolbar) rootView.findViewById(R.id.toolbarprovidernav2);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");

        ((ImageView) getActivity().findViewById(R.id.imag_tb)).setVisibility(View.VISIBLE);

        Utility.hideKeyboard(mContext);
        bind();

        return rootView;
    }

    public void bind() {
        indicator = (CirclePageIndicator) rootView.findViewById(R.id.upcoming_indicator);
        mPager = (ViewPager) rootView.findViewById(R.id.upcoming_pager);

        top_selling_horizontal = (RecyclerView) rootView.findViewById(R.id.top_selling_horizontal);
        popolar_horizontal = (RecyclerView) rootView.findViewById(R.id.popolar_horizontal);
        sessional_horizontal = (RecyclerView) rootView.findViewById(R.id.sessional_horizontal);

        presc_medication_btn = rootView.findViewById(R.id.presc_medication_btn);
        rootView.findViewById(R.id.upload_prescription).setOnClickListener(this);
        rootView.findViewById(R.id.call_us).setOnClickListener(this);
        rootView.findViewById(R.id.presc_medication_btn).setOnClickListener(this);
        rootView.findViewById(R.id.home_search_bar).setOnClickListener(this);
        rootView.findViewById(R.id.every_day_medical_btn).setOnClickListener(this);
        rootView.findViewById(R.id.female_care_btn).setOnClickListener(this);

        if (Utility.isConnectingToInternet(mContext)) {
            GetSellingProduct();
        } else {
            Ttoast.show(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
        }
    }

    public void GetSellingProduct() {
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constants.ACTION, Constants.SELLINGPRODUCTLIST);
            jo.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {
                        JSONObject j = new JSONObject(result);
                        System.out.println("SYSTEM IMAGE ------>>" + j.getJSONArray("banner_list"));
                        mPager.setAdapter(new VenueDetailPagerAdapter(mContext, j.getJSONArray("banner_list")));
                        indicator.setViewPager(mPager);
                        mPager.setCurrentItem(0);
                        if (j.getString("success").equals("1")) {
                            if (result == null) {
                                Ttoast.show(mContext, mContext.getResources().getString(R.string.server_fail), false);
                            } else if (j.getString("login_status").equals("0")) {
                                DialogViewReferCode();
                            } else {
                                JSONArray jj = j.getJSONObject(Constants.OBJECT).getJSONArray("Top Selling Products");
                                for (int i = 0; i < jj.length(); i++) {
                                    category_list.add(jj.getJSONObject(i));
                                }
                                TopSellingHorizontalList();

                                JSONArray jpop = j.getJSONObject(Constants.OBJECT).getJSONArray("Papular Products");

                                for (int i = 0; i < jpop.length(); i++) {
                                    popular_list.add(jpop.getJSONObject(i));
                                }
                                PopularHorizontalList();

                                JSONArray jses = j.getJSONObject(Constants.OBJECT).getJSONArray("Recent Views");

                                for (int i = 0; i < jses.length(); i++) {
                                    sessional_list.add(jses.getJSONObject(i));
                                }
                                SessionalHorizontalList();
                            }
                            adap.notifyDataSetChanged();
                        } else {
                            Ttoast.show(mContext, j.getString(Constants.SERVER_MSG), false);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void TopSellingHorizontalList() {
        horizontalLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        top_selling_horizontal.setLayoutManager(horizontalLayoutManager);

        adap = new TopSellingHorizontalListAdapter(mContext, R.layout.adap_home_sellingitem, category_list);
        top_selling_horizontal.setAdapter(adap);

    }

    public void SessionalHorizontalList() {
        horizontalLayoutManager2 = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        sessional_horizontal.setLayoutManager(horizontalLayoutManager2);

        adap3 = new SessionalHorizontalListAdapter(mContext, R.layout.adap_home_sessional_item, sessional_list);
        sessional_horizontal.setAdapter(adap3);

    }

    public void PopularHorizontalList() {

        horizontalLayoutManager1 = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        popolar_horizontal.setLayoutManager(horizontalLayoutManager1);

        adap2 = new PopularHorizontalListAdapter(mContext, R.layout.adap_home_popular_item, popular_list);
        popolar_horizontal.setAdapter(adap2);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.upload_prescription:
                if (Utility.getSharedPreferences(mContext, Constants.GUEST_USER).equals("1")) {
                    Utility.setIntegerSharedPreference(mContext, Constants.UPLOADPRESCACTION, 0);
                    ((MainActivity) mContext).callSelectedFragment
                            (Constants.UPLOADFRAGMENT, mContext.getResources().getString(R.string.upload_your_pres));
                } else {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.youneedlogin), false);
                    startActivity(new Intent(mContext, LoginActivity.class));
                }
                break;

            case R.id.call_us:
                Call();
                break;

            case R.id.home_search_bar:
                startActivity(new Intent(mContext, Activity_SearchingMedicine.class));
                break;

            case R.id.female_care_btn:
                Utility.setIntegerSharedPreference(mContext, Constants.BTNCLK, 3);
                startActivity(new Intent(mContext, Activity_Medicine_Subcategory.class));
                break;

            case R.id.every_day_medical_btn:
                Utility.setIntegerSharedPreference(mContext, Constants.BTNCLK, 0);
                startActivity(new Intent(mContext, Activity_Everyday_Medical_Product.class));
                break;

            case R.id.presc_medication_btn:
                ((MainActivity) mContext).callSelectedFragment(Constants.UPLOADFRAGMENT, mContext.getResources().getString(R.string.upload_pres));
                break;

            default:
                break;
        }
    }

    public void Call() {

        int permissionCheck = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.CALL_PHONE},
                    Integer.parseInt("123"));
        } else {
            startActivity(new Intent(Intent.ACTION_CALL).setData(Uri.parse("tel:12345678901")));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {

            case 123:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Call();
                } else {
                    Log.d("TAG", "Call Permission Not Granted");
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("requestCode " + requestCode);
        System.out.println("resultCode " + resultCode);
        switch (requestCode) {
            case FilePickerConst.PHOTO_PICKER:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    photoPaths = new ArrayList<>();
                    photoPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_PHOTOS));
                    System.out.println("filePathsimg " + photoPaths);
                    maxcount = 5;
                    filePaths = photoPaths;
//                    maxcount = maxcount - filePaths.size();
                }
                break;
        }
        addThemToView(photoPaths, docPaths);
    }

    private void addThemToView(ArrayList<String> imagePaths, ArrayList<String> docPaths) {
        ArrayList<String> filePaths = new ArrayList<>();
        if (imagePaths != null)
            filePaths.addAll(imagePaths);

        if (docPaths != null)
            filePaths.addAll(docPaths);

        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerview);
        if (recyclerView != null) {
            StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(5, OrientationHelper.VERTICAL);
            layoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS);
            recyclerView.setLayoutManager(layoutManager);

            ImageAdapter imageAdapter = new ImageAdapter(mContext, filePaths);

            recyclerView.setAdapter(imageAdapter);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
        }

//        Toast.makeText(this, "Num of files selected: "+ filePaths.size(), Toast.LENGTH_SHORT).show();
    }

    @NeedsPermission({Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public void onPickPhoto() {
        int maxCount = MAX_ATTACHMENT_COUNT - docPaths.size() - photoPaths.size();
        if ((docPaths.size() + photoPaths.size()) == MAX_ATTACHMENT_COUNT) {

        } else
            FilePickerBuilder.getInstance().setMaxCount(maxCount)
                    .setSelectedFiles(photoPaths)
                    .setActivityTheme(R.style.FilePickerTheme)
                    .pickPhoto(this);
    }

    public void SendPromotionalCode(String promo_code) {

        //http://www.infograins.in/INFO01/Pharm01/api/api.php?action=PromotionalUser&userid=14&promo_code=CP1421542

        try {
            JSONObject j = new JSONObject();
            j.put(Constants.ACTION, Constants.SENDPROMOTIONALCODE);
            j.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
            j.put(Constants.PROMOCODE, promo_code);
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));

            rt = new ResponseTask(mContext, j);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.server_fail), false);
                    } else {
                        try {
                            JSONObject jo = new JSONObject(result);
                            if (jo.getString("success").equals("1")) {

                                if (dialog_refercode.isShowing() && dialog_refercode != null) {
                                    dialog_refercode.dismiss();
                                }
                            } else {
                                Ttoast.ShowToast(mContext, jo.getString("msg"), false);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void DialogViewReferCode() {

        dialog_refercode = new Dialog(mContext);
        dialog_refercode = new Dialog(mContext, R.style.MyDialog);
        dialog_refercode.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_refercode.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        dialog_refercode.setContentView(R.layout.dialog_send_refercode);
        dialog_refercode.setCancelable(false);
        dialog_refercode.show();

        dialog_refercode.findViewById(R.id.submit_refer_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CustomEditText) dialog_refercode.findViewById(R.id.edt_promocode)).getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.enter_refer_code), false);
                } else {
                    SendPromotionalCode(((CustomEditText) dialog_refercode.findViewById(R.id.edt_promocode)).getText().toString());
                }
            }
        });

        dialog_refercode.findViewById(R.id.cancel_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CustomEditText) dialog_refercode.findViewById(R.id.edt_promocode)).setText("");
                SendPromotionalCode(((CustomEditText) dialog_refercode.findViewById(R.id.edt_promocode)).getText().toString());
            }

        });

        dialog_refercode.findViewById(R.id.dialog_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendPromotionalCode(((CustomEditText) dialog_refercode.findViewById(R.id.edt_promocode)).getText().toString());

            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_home, menu);

        MenuItem item = menu.findItem(R.id.menu_addcart);

        LayerDrawable icon = (LayerDrawable) item.getIcon();
        Util.setBadgeCount(getActivity(), icon, count);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_addcart:
                if (Utility.getSharedPreferences(mContext, Constants.GUEST_USER).equals("1")) {
                    startActivity(new Intent(mContext, Activity_AddtoCart_List.class));
                } else {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.youneedlogin), false);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    class TopSellingHorizontalListAdapter extends RecyclerView.Adapter<TopSellingHorizontalListAdapter.SimpleViewHolder> {

        Context appContext;
        ArrayList<JSONObject> jlist;
        int res = 0;

        public TopSellingHorizontalListAdapter(Context context, int res, ArrayList<JSONObject> jobj) {
            this.appContext = context;
            this.jlist = jobj;
            this.res = res;
        }

        @Override
        public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View convertView = li.inflate(res, parent, false);
            return new SimpleViewHolder(convertView);
        }

        @Override
        public void onBindViewHolder(final SimpleViewHolder holder, int position) {
            JSONObject j = jlist.get(position);
            try {
                Log.e(TAG, "jlist.get(position) ======>  " + jlist.get(position));

                String dp = j.getString("discount_price") + " " + mContext.getResources().getString(R.string.currncy_symbl);
                String pri = j.getString("price") + " " + mContext.getResources().getString(R.string.currncy_symbl);

                holder.product_desc.setText(j.getString("title"));
                if (j.getString("discount").equals("0")) {
                    holder.price.setVisibility(View.GONE);
                    holder.actual_pri.setText(pri);
                    holder.per_off.setVisibility(View.GONE);
                } else {
                    holder.price.setText(pri);
                    holder.actual_pri.setText(dp);
                    holder.per_off.setText(j.getString("discount") + " " + "%off");
                }

                if (j.getString("prescription_status").equals("1")) {
                    holder.home_rx_img.setVisibility(View.VISIBLE);
                } else {
                    holder.home_rx_img.setVisibility(View.GONE);
                }
                Glide.with(appContext).load(j.getString("product_image"))
                        /*.listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                                holder.home_pb.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                holder.home_pb.setVisibility(View.GONE);
                                return true;
                            }
                        })*/.into(holder.product_img);

                holder.view_lay.setOnClickListener(new MyClick(position));

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return jlist == null ? 0 : jlist.size();
        }

        class SimpleViewHolder extends RecyclerView.ViewHolder {

            CustomTextView product_desc, price, per_off, view_detail_btn, actual_pri;
            ImageView product_img, home_rx_img;
            ProgressBar home_pb;
            RelativeLayout view_lay;

            public SimpleViewHolder(View v) {
                super(v);
                product_desc = (CustomTextView) v.findViewById(R.id.product_desc);
                price = (CustomTextView) v.findViewById(R.id.price);
                price.setPaintFlags(((CustomTextView) v.findViewById(R.id.price)).getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                actual_pri = (CustomTextView) v.findViewById(R.id.actual_pri);
                per_off = (CustomTextView) v.findViewById(R.id.per_off);
                view_detail_btn = (CustomTextView) v.findViewById(R.id.view_detail_btn);
                product_img = (ImageView) v.findViewById(R.id.product_img);
                home_rx_img = (ImageView) v.findViewById(R.id.home_rx_img);
                home_pb = (ProgressBar) v.findViewById(R.id.home_pb);
                view_lay = (RelativeLayout) v.findViewById(R.id.view_lay);
            }
        }

        public class MyClick implements View.OnClickListener {
            int pos;

            public MyClick(int pos) {
                this.pos = pos;
            }

            @Override
            public void onClick(View v) {
                try {
                    Intent i = new Intent(mContext, ActivityProductDetail.class);
                    Bundle b = new Bundle();
                    b.putString(Constants.PRODUCTID, jlist.get(pos).getString("product_id"));
                    b.putString(Constants.PRODUCTTITLE, jlist.get(pos).getString("title"));

                    i.putExtras(b);
                    mContext.startActivity(i);
                } catch (JSONException j) {
                    j.printStackTrace();
                }

            }
        }
    }

    class PopularHorizontalListAdapter extends RecyclerView.Adapter<PopularHorizontalListAdapter.PopViewHolder> {
        Context appContext;
        ArrayList<JSONObject> j_pop_list;
        int res = 0;

        public PopularHorizontalListAdapter(Context context, int res, ArrayList<JSONObject> jobj) {
            this.appContext = context;
            this.j_pop_list = jobj;
            this.res = res;
        }

        @Override
        public PopViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View convertView = li.inflate(res, parent, false);
            return new PopViewHolder(convertView);
        }

        @Override
        public void onBindViewHolder(final PopViewHolder holder, int position) {
            try {

                Log.e(TAG, "j_pop_list ======>  " + j_pop_list.get(position));
                JSONObject j = j_pop_list.get(position);

                String dp = j.getString("discount_price") + " " + mContext.getResources().getString(R.string.currncy_symbl);
                String pri = j.getString("price") + " " + mContext.getResources().getString(R.string.currncy_symbl);
                String discount = j.getString("discount") + " " + "%off";

                holder.pop_product_desc.setText(j.getString("title"));

                if (j.getString("discount").equals("0")) {
                    holder.pop_price.setVisibility(View.GONE);
                    holder.pop_actual_pri.setText(pri);
                    holder.pop_per_off.setVisibility(View.GONE);
                } else {
                    holder.pop_price.setText(pri);
                    holder.pop_actual_pri.setText(dp);
                    holder.pop_per_off.setText(discount);
                }

                if (j.getString("prescription_status").equals("1")) {
                    holder.home_popular_rx_img.setVisibility(View.VISIBLE);
                } else {
                    holder.home_popular_rx_img.setVisibility(View.GONE);
                }
                Glide.with(appContext).load(j.getString("product_image")).into(holder.pop_product_img);
                   /*.listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                holder.pop_home_pb.setVisibility(View.GONE);
                                return true;
                            }
                        })*/
                holder.pop_view_clk.setOnClickListener(new MyClick(position));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return j_pop_list == null ? 0 : j_pop_list.size();
        }

        class PopViewHolder extends RecyclerView.ViewHolder {

            CustomTextView pop_product_desc, pop_price, pop_per_off, pop_view_detail_btn, pop_actual_pri;
            ImageView pop_product_img, home_popular_rx_img;
            ProgressBar pop_home_pb;
            RelativeLayout pop_view_clk;

            public PopViewHolder(View v) {
                super(v);
                pop_product_desc = (CustomTextView) v.findViewById(R.id.pop_product_desc);
                pop_price = (CustomTextView) v.findViewById(R.id.pop_price);
                pop_price.setPaintFlags(((CustomTextView) v.findViewById(R.id.pop_price)).getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                pop_per_off = (CustomTextView) v.findViewById(R.id.pop_per_off);
                pop_actual_pri = (CustomTextView) v.findViewById(R.id.pop_actual_pri);
                pop_view_detail_btn = (CustomTextView) v.findViewById(R.id.pop_view_detail_btn);
                pop_product_img = (ImageView) v.findViewById(R.id.pop_product_img);
                home_popular_rx_img = (ImageView) v.findViewById(R.id.home_popular_rx_img);
                pop_home_pb = (ProgressBar) v.findViewById(R.id.pop_home_pb);
                pop_view_clk = (RelativeLayout) v.findViewById(R.id.pop_view_clk);
            }
        }

        public class MyClick implements View.OnClickListener {
            int pos;

            public MyClick(int pos) {
                this.pos = pos;
            }

            @Override
            public void onClick(View v) {
                try {
                    Intent i = new Intent(mContext, ActivityProductDetail.class);
                    Bundle b = new Bundle();
                    b.putString(Constants.PRODUCTID, j_pop_list.get(pos).getString("product_id"));
                    i.putExtras(b);
                    mContext.startActivity(i);

                } catch (JSONException j) {
                    j.printStackTrace();
                }

            }
        }
    }

    class SessionalHorizontalListAdapter extends RecyclerView.Adapter<SessionalHorizontalListAdapter.SessionalViewHolder> {

        Context appContext;
        ArrayList<JSONObject> j_sessional_list;
        int res = 0;

        public SessionalHorizontalListAdapter(Context context, int res, ArrayList<JSONObject> jobj) {
            this.appContext = context;
            this.j_sessional_list = jobj;
            this.res = res;
        }

        @Override
        public SessionalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View convertView = li.inflate(res, parent, false);
            return new SessionalViewHolder(convertView);
        }

        @Override
        public void onBindViewHolder(final SessionalViewHolder holder, int position) {
            try {
                Log.e(TAG, "j_sessional_list  " + j_sessional_list.get(position));
                JSONObject j = j_sessional_list.get(position);

                String dp = j.getString("discount_price") + " " + mContext.getResources().getString(R.string.currncy_symbl);
                String pri = j.getString("price") + " " + mContext.getResources().getString(R.string.currncy_symbl);

                holder.sesion_product_desc.setText(j.getString("title"));
                if (j.getString("discount").equals("0")) {
                    holder.sesion_price.setVisibility(View.GONE);
                    holder.sesion_actual_pri.setText(pri);
                    holder.sesion_per_off.setVisibility(View.GONE);
                } else {
                    holder.sesion_price.setText(pri);
                    holder.sesion_actual_pri.setText(dp);
                    holder.sesion_per_off.setText(j.getString("discount") + " " + "%off");
                }

                if (j.getString("prescription_status").equals("1")) {
                    holder.home_recent_rx_img.setVisibility(View.VISIBLE);
                } else {
                    holder.home_recent_rx_img.setVisibility(View.GONE);
                }

                Glide.with(appContext).load(j.getString("product_image"))
                        /*listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                holder.sesion_home_pb.setVisibility(View.GONE);
                                return true;
                            }
                        }).*/.into(holder.sesion_product_img);
                holder.sessional_lay.setOnClickListener(new MyClick(position));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return j_sessional_list == null ? 0 : j_sessional_list.size();
        }

        class SessionalViewHolder extends RecyclerView.ViewHolder {

            CustomTextView sesion_product_desc, sesion_price, sesion_per_off, sesion_view_detail_btn, sesion_actual_pri;
            ImageView sesion_product_img, home_recent_rx_img;
            ProgressBar sesion_home_pb;
            RelativeLayout sessional_lay;

            public SessionalViewHolder(View v) {
                super(v);
                sesion_product_desc = (CustomTextView) v.findViewById(R.id.sesion_product_desc);
                sesion_price = (CustomTextView) v.findViewById(R.id.sesion_price);
                sesion_price.setPaintFlags(((CustomTextView) v.findViewById(R.id.sesion_price)).getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                sesion_actual_pri = (CustomTextView) v.findViewById(R.id.sesion_actual_pri);
                sesion_per_off = (CustomTextView) v.findViewById(R.id.sesion_per_off);
                sesion_view_detail_btn = (CustomTextView) v.findViewById(R.id.sesion_view_detail_btn);
                sesion_product_img = (ImageView) v.findViewById(R.id.sesion_product_img);
                home_recent_rx_img = (ImageView) v.findViewById(R.id.home_recent_rx_img);
                sesion_home_pb = (ProgressBar) v.findViewById(R.id.sesion_home_pb);
                sessional_lay = (RelativeLayout) v.findViewById(R.id.sessional_lay);
            }
        }

        public class MyClick implements View.OnClickListener {
            int pos;

            public MyClick(int pos) {
                this.pos = pos;
            }

            @Override
            public void onClick(View v) {
                try {
                    Intent i = new Intent(mContext, ActivityProductDetail.class);
                    Bundle b = new Bundle();
                    b.putString(Constants.PRODUCTID, j_sessional_list.get(pos).getString("product_id"));
                    i.putExtras(b);
                    mContext.startActivity(i);

                } catch (JSONException j) {
                    j.printStackTrace();
                }

            }
        }
    }


}