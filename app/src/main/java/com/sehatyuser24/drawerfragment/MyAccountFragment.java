package com.sehatyuser24.drawerfragment;


import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SimpleAdapter;
import android.widget.Spinner;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import com.sehatyuser24.ImageSelection.ImageSelection;
import com.sehatyuser24.R;
import com.sehatyuser24.activity.other.ActivitySetting;
import com.sehatyuser24.activity.other.Activity_GetAddress;
import com.sehatyuser24.activity.registration.ActivityChangePW;
import com.sehatyuser24.activity.registration.MainActivity;
import com.sehatyuser24.customwidget.CircleImageView;
import com.sehatyuser24.customwidget.CustomAutoCompleteTextView;
import com.sehatyuser24.customwidget.CustomButton;
import com.sehatyuser24.customwidget.CustomEditText;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.serverinteraction.GetResponseTask;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.Constant_Urls;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.MultipartUtility;
import com.sehatyuser24.utility.PlaceJSONParser;
import com.sehatyuser24.utility.Utility;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by nilesh on 11/4/17.
 */

public class MyAccountFragment extends Fragment implements View.OnClickListener, ImageSelection, DatePickerDialog.OnDateSetListener {

    Context mContext;
    Toolbar toolbar;
    ResponseTask rt;
    View rv;
    CustomEditText edtprof_fname, edtprof_number, edtprof_email, edtprof_lname;
    String profile_image = "", date = "", Updated_ScreetAddress;
    CustomButton btn_submit;

    CustomTextView edtprof_dob, edtprof_addr;
    Spinner edt_pro_gender;
    ArrayList<HashMap<String, String>> gender_spinner;
    String[] genderarray;
    String gen = "", SocialImg = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Utility.ChangeLang(getActivity(),Utility.getIngerSharedPreferences(getActivity(),Constants.LANG));

        rv = inflater.inflate(R.layout.fragment_editaccount, container, false);

        mContext = getActivity();
        toolbar = (Toolbar) rv.findViewById(R.id.toolbarprovidernav2);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.my_account));
        ((ImageView) getActivity().findViewById(R.id.imag_tb)).setVisibility(View.GONE);

        genderarray = new String[]{mContext.getResources().getString(R.string.select_gender),
                mContext.getResources().getString(R.string.male), mContext.getResources().getString(R.string.female)};

        Find();

        Init();

        return rv;
    }

    public void Find() {

        edtprof_fname = (CustomEditText) rv.findViewById(R.id.edtprof_fname);
        edtprof_lname = (CustomEditText) rv.findViewById(R.id.edtprof_lname);
        edtprof_number = (CustomEditText) rv.findViewById(R.id.edtprof_number);
        edtprof_email = (CustomEditText) rv.findViewById(R.id.edtprof_email);
        edtprof_dob = (CustomTextView) rv.findViewById(R.id.edtprof_dob);
        edtprof_addr = (CustomTextView) rv.findViewById(R.id.edtprof_addr);
        btn_submit = (CustomButton) rv.findViewById(R.id.btn_submit);
        edt_pro_gender = (Spinner) rv.findViewById(R.id.edt_pro_gender);

    }

    public void Init() {

        ((LinearLayout) rv.findViewById(R.id.tablelay_one)).setVisibility(View.VISIBLE);
        ((LinearLayout) rv.findViewById(R.id.tablelay_two)).setVisibility(View.GONE);

        btn_submit.setOnClickListener(this);
        rv.findViewById(R.id.edit_btn).setOnClickListener(this);
        rv.findViewById(R.id.select_img).setOnClickListener(this);
        rv.findViewById(R.id.edtprof_dob).setOnClickListener(this);
        rv.findViewById(R.id.family_mem_link).setOnClickListener(this);
        rv.findViewById(R.id.my_order_link).setOnClickListener(this);
        edtprof_addr.setOnClickListener(this);

        if (Utility.isConnectingToInternet(mContext)) {
            GetProfileTask();
        } else {
            Ttoast.show(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
        }

        getcurrency_spinner();

    }

    @Override
    public void selected(String path) {
        profile_image = path;
        Glide.with(mContext).
                load(profile_image)
                .into(((CircleImageView) rv.findViewById(R.id.img_pro)));
    }

    public void GetProfileTask() {

        /*http://infograins.com.208-91-199-7.md-plesk-web2.webhostbox.net/INFO01/pharmacy/api/api.php?
        action=ProfileById&userid=2*/
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, Constants.GETPROFILEID);
            jsonObject.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(mContext, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), getActivity());
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString("success").equals("1")) {
                                JSONObject userdetail = jobj.getJSONObject(Constants.OBJECT);

                                if (userdetail.getString("fullname").equals("")) {
                                    ((CustomTextView) rv.findViewById(R.id.editp_name)).setHint(mContext.getResources().getString(R.string.name));
                                } else {
                                    ((CustomTextView) rv.findViewById(R.id.editp_name)).setText(userdetail.getString("fullname"));
                                }

                                if (userdetail.getString("fname").equals("")) {
                                    ((CustomTextView) rv.findViewById(R.id.edit_fname)).setHint(mContext.getResources().getString(R.string.first_name));
                                } else {
                                    ((CustomTextView) rv.findViewById(R.id.edit_fname)).setText(userdetail.getString("fname"));
                                }

                                if (userdetail.getString("lname").equals("")) {
                                    ((CustomTextView) rv.findViewById(R.id.edit_lname)).setHint(mContext.getResources().getString(R.string.last_name));
                                } else {
                                    ((CustomTextView) rv.findViewById(R.id.edit_lname)).setText(userdetail.getString("lname"));
                                }

                                if (userdetail.getString("mobile").equals("")) {
                                    ((CustomTextView) rv.findViewById(R.id.eprofile_number)).setHint(mContext.getResources().getString(R.string.mob_no));
                                } else {
                                    ((CustomTextView) rv.findViewById(R.id.eprofile_number)).setText(userdetail.getString("mobile"));
                                }

                                if (userdetail.getString("email").equals("")) {
                                    ((CustomTextView) rv.findViewById(R.id.eprofile_email)).setHint(mContext.getResources().getString(R.string.email));
                                } else {
                                    ((CustomTextView) rv.findViewById(R.id.eprofile_email)).setText(userdetail.getString("email"));
                                }
                                if (userdetail.getString("dob").equals("")) {
                                    ((CustomTextView) rv.findViewById(R.id.eprofile_dob)).setHint(mContext.getResources().getString(R.string.dob_format));
                                } else {
                                    ((CustomTextView) rv.findViewById(R.id.eprofile_dob)).setText(userdetail.getString("dob"));
                                }
                                if (userdetail.getString("street_address").equals("")) {
                                    ((CustomTextView) rv.findViewById(R.id.eprofile_addr)).setHint(mContext.getResources().getString(R.string.email));
                                } else {
                                    ((CustomTextView) rv.findViewById(R.id.eprofile_addr)).setText(userdetail.getString("street_address"));
                                }

                                if (userdetail.getString("total_member").equals("")) {
                                    ((CustomTextView) rv.findViewById(R.id.fam_mem_count)).setText("0");
                                } else {
                                    ((CustomTextView) rv.findViewById(R.id.fam_mem_count)).setText(userdetail.getString("total_member"));
                                }

                                if (userdetail.getString("total_order").equals("")) {
                                    ((CustomTextView) rv.findViewById(R.id.my_order_count)).setText("0");
                                } else {
                                    ((CustomTextView) rv.findViewById(R.id.my_order_count)).setText(userdetail.getString("total_order"));
                                }

                                ((CustomTextView) rv.findViewById(R.id.fam_mem_count)).setText(userdetail.getString("total_member"));
                                ((CustomTextView) rv.findViewById(R.id.my_order_count)).setText(userdetail.getString("total_order"));

                                if (userdetail.getString("gender").equals("")) {
                                    ((CustomTextView) rv.findViewById(R.id.eprofile_gender)).setHint(mContext.getResources().getString(R.string.gender));
                                } else {
                                    if (userdetail.getString("gender").equals("0")) {
                                        ((CustomTextView) rv.findViewById(R.id.eprofile_gender)).setText(mContext.getResources().getString(R.string.male));

                                    } else {
                                        ((CustomTextView) rv.findViewById(R.id.eprofile_gender)).setText(mContext.getResources().getString(R.string.female));

                                    }
                                }
                                if (userdetail.getString("profile_image").equals("")) {

                                    Glide.with(mContext).load(ContextCompat.getDrawable(mContext, R.drawable.user_icon)).
                                            into((CircleImageView) rv.findViewById(R.id.img_pro));

                                } else {
                                    Glide.with(mContext).load(userdetail.getString("profile_image")).
                                            listener(new RequestListener<String, GlideDrawable>() {
                                                @Override
                                                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                                    rv.findViewById(R.id.img_pb).setVisibility(View.GONE);
                                                    rv.findViewById(R.id.img_pb).setVisibility(View.GONE);
                                                    return false;
                                                }

                                                @Override
                                                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                                    rv.findViewById(R.id.img_pb).setVisibility(View.GONE);
                                                    return false;
                                                }
                                            }).
                                            into((CircleImageView) rv.findViewById(R.id.img_pro));
                                }
                                SocialImg = userdetail.getString("profile_image");
                                edtprof_fname.setText(userdetail.getString("fname"));
                                edtprof_lname.setText(userdetail.getString("lname"));
                                edtprof_number.setText(userdetail.getString("mobile"));
                                edtprof_email.setText(userdetail.getString("email"));
                                edtprof_dob.setText(userdetail.getString("dob"));
                                edtprof_addr.setText(userdetail.getString("street_address"));

                                Utility.setSharedPreference(mContext, Constants.FNAME, userdetail.getString("fullname"));
                                Utility.setSharedPreference(mContext, Constants.PROFILE_IMAGE, userdetail.getString("profile_image"));
                                ((MainActivity) mContext).SetDrawerData();

                                if (userdetail.getString("gender").equals("0")) {
                                    edt_pro_gender.setSelection(1);
                                    gen = "0";
                                } else if (userdetail.getString("gender").equals("1")) {
                                    edt_pro_gender.setSelection(2);
                                    gen = "1";
                                } else {
                                    edt_pro_gender.setSelection(0);
                                }


                            } else {
                                Ttoast.ShowToast(mContext, jobj.getString(Constants.SERVER_MSG), false);
                            }
                        } catch (JSONException e2) {
                            Ttoast.ShowToast(mContext, getResources().getString(R.string.server_fail), false);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

    public void getcurrency_spinner() {
        gender_spinner = new ArrayList<>();
        for (int i = 0; i < genderarray.length; i++) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("Type", genderarray[i]);
            gender_spinner.add(hm);
        }

        int[] to = new int[]{R.id.card};
        String[] from = new String[]{"Type"};
        // Creating a SimpleAdapter for the Spinner
        SimpleAdapter adapter = new SimpleAdapter(mContext, gender_spinner, R.layout.text_single_add, from, to);
        edt_pro_gender.setAdapter(adapter);

        edt_pro_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.gender_select), false);
                } else if (position == 1) {
                    gen = "0";
                    System.out.println("GENDERRRR  " + gen);
                } else if (position == 2) {
                    gen = "1";
                }
                System.out.println("GENDERRRR  " + gen);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                gen = "";
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.select_img:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(getActivity(), CAMERA) == PackageManager.PERMISSION_GRANTED
                            && ContextCompat.checkSelfPermission(getActivity(), WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        ((MainActivity) mContext).SelectImage(this);
                    } else {
                        requestCameraPermission();
                    }
                } else {
                    ((MainActivity) mContext).SelectImage(this);
                }
                break;
            case R.id.edit_btn:

                ((LinearLayout) rv.findViewById(R.id.tablelay_one)).setVisibility(View.GONE);
                ((LinearLayout) rv.findViewById(R.id.tablelay_two)).setVisibility(View.VISIBLE);

                break;

            case R.id.btn_submit:
                if (edtprof_fname.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_fname), false);
                } else if (edtprof_number.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_mobile), false);
                } else if (((CustomTextView) rv.findViewById(R.id.edtprof_dob)).getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_dob), false);
                } else if (edtprof_email.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_email), false);
                } else if (edtprof_addr.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_street), false);
                } else if (!Utility.isValidEmail(edtprof_email.getText().toString())) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.corect_email), false);
                } else if (gen.equals("")) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.gender_select), false);
                } else if (Utility.isConnectingToInternet(mContext)) {

                    new EditProfileTask().execute(edtprof_fname.getText().toString(), edtprof_lname.getText().toString(),
                            edtprof_email.getText().toString(),
                            ((CustomTextView) rv.findViewById(R.id.edtprof_dob)).getText().toString(),
                            edtprof_addr.getText().toString(), edtprof_number.getText().toString(), gen);
                }
                break;

            case R.id.edtprof_dob:
                DatePickerMet();
                break;

            case R.id.my_order_link:
                ((MainActivity) mContext).callSelectedFragment(Constants.MYORDERFRAGMENT, mContext.getResources().getString(R.string.my_order));
                break;

            case R.id.family_mem_link:
                ((MainActivity) mContext).callSelectedFragment(Constants.FAMILYFRAGMENT, mContext.getResources().getString(R.string.my_family_member));
                break;

            case R.id.edtprof_addr:
                Intent i = new Intent(mContext, Activity_GetAddress.class);
                startActivityForResult(i, 3);
                break;
        }

    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        date = dayOfMonth + "" + "/" + (++monthOfYear) + "/" + year;
        System.out.println("myac_date  " + date);
        edtprof_dob.setText(date);
        Utility.setSharedPreference(mContext, "edtdate", date);
    }

    public class EditProfileTask extends AsyncTask<String, String, String> {
        /*http://infograins.com.208-91-199-7.md-plesk-web2.webhostbox.net/INFO01/pharmacy/api/api.php?action=UpdateSaveProfile&userid=1&
        fname=chandra&lname=prakash&
        email=kamalsir@yopmail.com&dob=12-04-2017&street_address=indor mp india&profile_image=default.php*/
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                MultipartUtility multipart = new MultipartUtility(mContext, "UTF-8");
                multipart.addFormField(Constants.ACTION, Constants.UPDATEPROFILE);
                multipart.addFormField(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
                multipart.addFormField(Constants.FNAME, params[0]);
                multipart.addFormField(Constants.LNAME, params[1]);
                multipart.addFormField(Constants.EMAIL, params[2]);
                multipart.addFormField(Constants.DOB, params[3]);
                multipart.addFormField(Constants.STREET, params[4]);
                multipart.addFormField(Constants.MOBILE, params[5]);
                multipart.addFormField(Constants.GENDER, params[6]);
                if (profile_image.equals("")) {
                    multipart.addFormField(Constants.PROFILE_IMAGE, "");
                } else {
                    multipart.addFilePart(Constants.PROFILE_IMAGE, profile_image);
                }
                return multipart.finish();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Utility.HideDialog();
            if (result == null) {
                Utility.ShowToastMessage(mContext, getResources().getString(R.string.server_fail));
            } else {
                try {
                    JSONObject json = new JSONObject(result);
                    Log.e("result result", "result result ============= " + json);
                    if (json.getString("success").equals("1")) {
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.updatedsuccess), false);
                        profile_image = "";
                        JSONObject jo = json.getJSONObject(Constants.OBJECT);
                        Utility.setSharedPreference(mContext, Constants.PROFILE_IMAGE, jo.getString("profile_image"));
                        Utility.setSharedPreference(mContext, Constants.FNAME, jo.getString("username"));
                        ((MainActivity) mContext).SetDrawerData();
                    }
                } catch (Exception e) {

                }
            }
            super.onPostExecute(result);
        }
    }

    private void requestCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), CAMERA)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(getActivity(), new String[]{CAMERA, WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE}, 1);
    }

    public void DatePickerMet() {
        //LINK:--------->>>     https://github.com/wdullaer/MaterialDateTimePicker
        Calendar now = Calendar.getInstance();

        DatePickerDialog dpd = DatePickerDialog.newInstance(MyAccountFragment.this, now.get
                        (Calendar.YEAR),
                now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));
        dpd.setMaxDate(now);
        FragmentManager fm = getActivity().getFragmentManager();

        dpd.show(fm, "Datepickerdialog");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_setting, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(mContext, ActivitySetting.class));
                return true;

           /* case R.id.menu_my_order:
//                startActivity(new Intent(mContext, ActivitySetting.class));
                return true;*/
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 3 && resultCode == Activity.RESULT_OK) {
            Updated_ScreetAddress = data.getStringExtra("PICKADDRESS");
            edtprof_addr.setText(Updated_ScreetAddress);
        }
    }

}
