package com.sehatyuser24.drawerfragment;

import android.content.Context;
import android.os.Bundle;

import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.sehatyuser24.R;
import com.sehatyuser24.fragment.EarningPointFragment;
import com.sehatyuser24.fragment.ReferFragment;
import com.sehatyuser24.serverinteraction.ResponseTask;


/**
 * Created by nilesh on 11/4/17.
 */

public class EarnFragment extends Fragment {

    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    public static int int_items = 2;
    Context mContext;
    Toolbar toolbar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /**
         *Inflate tab_layout and setup Views.
         */
        mContext = getActivity();


        View x = inflater.inflate(R.layout.fragment_earn, null);

        toolbar = (Toolbar) x.findViewById(R.id.toolbarprovidernav2);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.refer));
        ((ImageView) getActivity().findViewById(R.id.imag_tb)).setVisibility(View.GONE);

        tabLayout = (TabLayout) x.findViewById(R.id.tabs);
        viewPager = (ViewPager) x.findViewById(R.id.viewpager);

        /**
         *Set an Adapter for the View Pager
         */
        viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));

        /**
         * Now , this is a workaround ,
         * The setupWithViewPager dose't works without the runnable .
         * Maybe a Support Library Bug .
         */

        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
            }
        });

      /*  tabLayout.addTab(tabLayout.newTab()
                .setCustomView(renderTabView(mContext, 1, R.drawable.tab_orange,1))
                .setTabListener(this));*/
        return x;
    }

    public class MyAdapter extends FragmentPagerAdapter {
        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Return fragment with respect to Position .
         */

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new ReferFragment();
                case 1:
                    return new EarningPointFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return int_items;
        }

        /**
         * This method returns the title of the tab according to the position.
         */

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position) {
                case 0:
                    return mContext.getResources().getString(R.string.refer_txt);
                case 1:
                    return mContext.getResources().getString(R.string.earn_point);
                /*case 2:
                    return "Price";*/
                /*case 3:
                    return "Scheduled Booking";
                case 4:
                    return "Post a Project";*/
            }
            return null;
        }
    }
}
