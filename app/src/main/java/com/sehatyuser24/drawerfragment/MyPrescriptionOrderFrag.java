package com.sehatyuser24.drawerfragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import com.sehatyuser24.R;
import com.sehatyuser24.adapter.MyPrescriptionAdap;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Utility;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;


/**
 * Created by and-04 on 14/9/17.
 */

public class MyPrescriptionOrderFrag extends Fragment {

    View rv;
    ResponseTask rt;
    Context mContext;
    ArrayList<JSONObject> presc_arr = new ArrayList<>();
    boolean MORERESULT = true;
    ListView my_presc_list;
    int page = 0;
    Toolbar toolbar;
    MyPrescriptionAdap mpadap;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rv = inflater.inflate(R.layout.frag_mypresc_lst, null);
        mContext = getContext();
        toolbar = (Toolbar) rv.findViewById(R.id.toolbarprovidernav2);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.my_pres));

        ((ImageView) getActivity().findViewById(R.id.imag_tb)).setVisibility(View.GONE);

        Listner();

        return rv;
    }

    public void Listner() {

        my_presc_list = (ListView) rv.findViewById(R.id.my_presc_list);
        if (Utility.isConnectingToInternet(mContext)) {
            GetPrescriptionList();
        } else {
            Ttoast.show(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
        }
        Init();

    }

    public void Init() {
        Utility.hideKeyboard(mContext);
        mpadap = new MyPrescriptionAdap(mContext, presc_arr);
        my_presc_list.setAdapter(mpadap);

       /* my_presc_list.setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (Utility.isConnectingToInternet(mContext)) {
                    if (MORERESULT) {
                       page = page + 1;
                        GetPrescriptionList();
                    } else {
                        my_presc_list.onLoadMoreComplete();
                    }
                } else {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
                }
            }
        });*/
    }

    public void GetPrescriptionList() {
        try {
            JSONObject j = new JSONObject();
            j.put(Constants.ACTION, Constants.PRESCRIPTIONLIST);
            j.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
//            j.put(Constants.USERID, "165");
//            j.put(Constants.PAGE, page);
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));

            rt = new ResponseTask(mContext, j);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {

                    Utility.HideDialog();

//                    my_presc_list.onLoadMoreComplete();

                    if (result == null) {
//                        page = page - 1;
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.server_fail), false);
                    } else {
                        try {
                            JSONObject jo = new JSONObject(result);
                            if (jo.getString("success").equals("1")) {
                                JSONArray ja = jo.getJSONArray(Constants.OBJECT);
                                for (int i = 0; i < ja.length(); i++) {
                                    presc_arr.add(ja.getJSONObject(i));
                                }
                                mpadap.notifyDataSetChanged();

                            } else {
//                                MORERESULT = false;
                                Ttoast.ShowToast(mContext, jo.getString(Constants.SERVER_MSG), false);
                            }

                        } catch (JSONException je) {
                            MORERESULT = false;
                            page = page - 1;
                            je.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException je) {
            je.printStackTrace();
        }
    }

}