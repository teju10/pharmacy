package com.sehatyuser24.drawerfragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.sehatyuser24.R;
import com.sehatyuser24.activity.other.Activity_Contactus;
import com.sehatyuser24.activity.other.Activity_Help_Support;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by nilesh on 11/4/17.
 */

public class AboutusFragment extends Fragment implements View.OnClickListener {

    Context mContext;
    Toolbar toolbar;
    Dialog feedback_dia;
    View rv;
    ResponseTask rt;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Utility.ChangeLang(getActivity(), Utility.getIngerSharedPreferences(getActivity(), Constants.LANG));

        rv = inflater.inflate(R.layout.about_us, container, false);
        mContext = getActivity();
        toolbar = (Toolbar) rv.findViewById(R.id.toolbarprovidernav2);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.about_us));
        ((ImageView) getActivity().findViewById(R.id.imag_tb)).setVisibility(View.GONE);

        Find();

        LISTNER();

        return rv;
    }


    public void Find() {

        rv.findViewById(R.id.feedback_btn).setOnClickListener(this);

        rv.findViewById(R.id.privacy_btn).setOnClickListener(this);

        rv.findViewById(R.id.contact_us_btn).setOnClickListener(this);

    }

    public void LISTNER() {


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.feedback_btn:
                GiveRattingDialog();
                break;

            case R.id.privacy_btn:
                startActivity(new Intent(mContext, Activity_Help_Support.class));
                break;

            case R.id.contact_us_btn:
                startActivity(new Intent(mContext, Activity_Contactus.class));
                break;
        }
    }

    public void GiveRattingDialog() {

        feedback_dia = new Dialog(mContext, R.style.MyDialog);
        feedback_dia.requestWindowFeature(Window.FEATURE_NO_TITLE);
        feedback_dia.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        feedback_dia.setContentView(R.layout.dialog_feedback_screen);
        feedback_dia.setCancelable(true);
        feedback_dia.show();

        feedback_dia.findViewById(R.id.edt_feedback).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.edt_feedback) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        try {
            LayerDrawable stars = (LayerDrawable) ((RatingBar) feedback_dia.findViewById(R.id.feed_rate_bar)).getProgressDrawable();
            stars.getDrawable(2).setColorFilter(mContext.getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(0).setColorFilter(mContext.getResources().getColor(R.color.grey), PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(1).setColorFilter(mContext.getResources().getColor(R.color.grey), PorterDuff.Mode.SRC_ATOP);
        } catch (Exception e) {
            e.printStackTrace();
        }

       /* try {
            ((RatingBar) feedback_dia.findViewById(R.id.rate_bar)).setRating(Float.parseFloat(jlist.get(pos1).getString("product_rate")));
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        feedback_dia.findViewById(R.id.submit_feedback).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float rat = ((RatingBar) feedback_dia.findViewById(R.id.feed_rate_bar)).getRating();
                if (rat > 0) {
                    if (feedback_dia != null && feedback_dia.isShowing()) {
                        feedback_dia.cancel();
                    }
                    if (feedback_dia.findViewById(R.id.edt_feedback).equals("")) {
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_cmnt), false);
                    } else if (Utility.isConnectingToInternet(mContext)) {
                        SubmitFeedback("" + rat, feedback_dia.findViewById(R.id.edt_feedback));
                    } else {
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
                    }
                } else {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.rat_rev), false);
                }
            }
        });

        feedback_dia.findViewById(R.id.dialog_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (feedback_dia != null && feedback_dia.isShowing()) {
                    feedback_dia.cancel();
                }
            }
        });
    }

    public void SubmitFeedback(String rate, View cmmnt) {

        /*http://www.infograins.in/INFO01/Pharm01/api/api.php?action=
SubmitUserFeedBack&userid=155&comment=hii thanks&rating=3*/
        try {
            JSONObject j = new JSONObject();
            j.put(Constants.ACTION, Constants.SUBMITFEEDBACK);
            j.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
            j.put("comment", cmmnt);
            j.put("rating", rate);

            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));

            rt = new ResponseTask(mContext, j);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {
                        if (result == null) {
                            Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.server_fail), false);
                        } else {
                            JSONObject object = new JSONObject(result);
                            if (object.getString("success").equals("1")) {
                                Ttoast.show(mContext, mContext.getResources().getString(R.string.feedback_submit), false);
                                if (feedback_dia != null && feedback_dia.isShowing()) {
                                    feedback_dia.cancel();
                                }
                            } else {
                                Ttoast.ShowToast(mContext, object.getString("msg"), false);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
            rt.execute();
        } catch (JSONException j) {
            Utility.HideDialog();
            j.printStackTrace();
        }

    }

}
