package com.sehatyuser24.drawerfragment;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.sehatyuser24.DBHelper;
import com.sehatyuser24.R;
import com.sehatyuser24.activity.other.Activity_AddBillingAddress;
import com.sehatyuser24.activity.other.Activity_AddShippingAddress;
import com.sehatyuser24.activity.other.Activity_AddtoCart_List;
import com.sehatyuser24.activity.registration.MainActivity;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.MultiPartFileUpload;
import com.sehatyuser24.utility.Utility;
import com.soundcloud.android.crop.Crop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;


/**
 * Created by nilesh on 11/4/17.
 */


public class UploadFragment extends Fragment implements View.OnClickListener {
    Context mContext;
    Toolbar toolbar;
    View rv;
    ResponseTask rt;
    ArrayList<String> imgPaths = new ArrayList<>();
    String imagepath1 = "", imagepath2 = "", imagepath3 = "",
            imagepath4 = "", imagepath5 = "", getpicturepath = "";
    RecyclerView pic_list;
    ImageViewHori setAdapter;
    JSONObject ship_jasn, bill_jsn;
    Bundle bun = new Bundle();
    Boolean Status = true;
    DBHelper db;
    Dialog dia_upload_presc;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Utility.ChangeLang(getActivity(),Utility.getIngerSharedPreferences(getActivity(),Constants.LANG));

        rv = inflater.inflate(R.layout.activity_upload_prescription, container, false);

        mContext = getActivity();

        toolbar = (Toolbar) rv.findViewById(R.id.toolbarprovidernav2);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.upload_pres));
        ((ImageView) getActivity().findViewById(R.id.imag_tb)).setVisibility(View.GONE);

        db = new DBHelper(mContext);

        Find();

        return rv;
    }

    public void Find() {
        pic_list = (RecyclerView) rv.findViewById(R.id.pic_list);

        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        pic_list.setLayoutManager(horizontalLayoutManager);

        Listner();
    }

    public void Listner() {
        if (imgPaths.size() < 0) {
            pic_list.setVisibility(View.GONE);
        }

        rv.findViewById(R.id.add_img).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    if (ContextCompat.checkSelfPermission(getActivity(), CAMERA) == PackageManager.PERMISSION_GRANTED
                            && ContextCompat.checkSelfPermission(getActivity(), WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        if (imgPaths.size() < 5) {
                            selectImage();
                        } else {
                            Ttoast.ShowToast(mContext, mContext.getString(R.string.imageselect), false);
                        }
                    } else {
                        requestCameraPermission();
                    }
                } else {
                    if (imgPaths.size() < 5) {
                        selectImage();
                    } else {
                        Ttoast.ShowToast(mContext, mContext.getString(R.string.imageselect), false);
                    }
                }
            }
        });


        rv.findViewById(R.id.edit_shipping_address_btn).setOnClickListener(this);
        rv.findViewById(R.id.add_billing_address_btn).setOnClickListener(this);
        rv.findViewById(R.id.cnfrm_prescription_order).setOnClickListener(this);
//        rv.findViewById(R.id.call_us).setOnClickListener(this);

    }

    @Override
    public void onResume() {
        if (Utility.isConnectingToInternet(mContext)) {
            GetAddressTask();
        } else {
            Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
        }

        super.onResume();
    }

    public void GetAddressTask() {

//infograins.com.208-91-199-7.md-plesk-web2.webhostbox.net/INFO01/pharmacy/api/api.php?
        // action=ListShippingAddressById&userid=1
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, Constants.DELIVERYADDRESS);
            jsonObject.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(mContext, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Ttoast.ShowToast(mContext, getResources().getString(R.string.server_fail), false);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString("success").equals("1")) {
                                Status = true;
                                Utility.setSharedPreference(mContext, Constants.SHIPPING_STATUS, "");
                                JSONObject userdetail = jobj.getJSONObject(Constants.OBJECT);
                                JSONObject jo = userdetail.getJSONObject("shipping_address");
                                JSONObject jo1 = userdetail.getJSONObject("billing_address");

                                String obj2 = jo1.toString();
                                System.out.println("BUNDLE BILLING " + obj2);
                                bill_jsn = new JSONObject(obj2);

                                String obj1 = jo.toString();
                                ship_jasn = new JSONObject(obj1);

                                ((CustomTextView) rv.findViewById(R.id.shippingadd_uname)).setText(jo1.getString("fname") + " "
                                        + jo1.getString("lname"));
                                ((CustomTextView) rv.findViewById(R.id.shippingadd_email)).setText(jo1.getString("email"));

                                ((CustomTextView) rv.findViewById(R.id.shippingadd_mobile)).setText(jo1.getString("mobile"));

                                if (jo.getString("fullname").equals("")) {
                                    ((CustomTextView) rv.findViewById(R.id.shippingadd_username)).setText(mContext.getResources().getString(R.string.name));

                                } else {
                                    ((CustomTextView) rv.findViewById(R.id.shippingadd_username)).setText(jo.getString("fullname"));
                                }
                                if (jo.getString("city").equals("")) {
                                    ((CustomTextView) rv.findViewById(R.id.shippingadd_city)).setText(mContext.getResources().getString(R.string.city));

                                } else {
                                    ((CustomTextView) rv.findViewById(R.id.shippingadd_city)).setText(jo.getString("city"));

                                }
                                if (jo.getString("mobile").equals("")) {
                                    ((CustomTextView) rv.findViewById(R.id.shippingadd_mob)).setText(mContext.getResources().getString(R.string.mob_no));

                                } else {
                                    ((CustomTextView) rv.findViewById(R.id.shippingadd_mob)).setText(jo.getString("mobile"));
                                }

                                if (jo.getString("flat_number").equals("")) {
                                    ((CustomTextView) rv.findViewById(R.id.shipping_addr_flatno)).setText(mContext.getResources().getString(R.string.flat_no));

                                } else {
                                    ((CustomTextView) rv.findViewById(R.id.shipping_addr_flatno)).setText(jo.getString("flat_number"));
                                }

                                if (jo.getString("building_number").equals("")) {
                                    ((CustomTextView) rv.findViewById(R.id.shippingadd_buildno)).setText(mContext.getResources().getString(R.string.building_no));

                                } else {
                                    ((CustomTextView) rv.findViewById(R.id.shippingadd_buildno)).setText(jo.getString("building_number"));
                                }

                                if (jo.getString("floor_number").equals("")) {
                                    ((CustomTextView) rv.findViewById(R.id.shipping_floor)).setText(mContext.getResources().getString(R.string.floor_no));

                                } else {
                                    ((CustomTextView) rv.findViewById(R.id.shipping_floor)).setText(jo.getString("floor_number"));
                                }

                                if (jo.getString("district").equals("")) {
                                    ((CustomTextView) rv.findViewById(R.id.shippingadd_district_txt)).setText(mContext.getResources().getString(R.string.district));

                                } else {
                                    ((CustomTextView) rv.findViewById(R.id.shippingadd_district_txt)).setText(jo.getString("district"));
                                }

                                if (jo.getString("landmark").equals("")) {
                                    ((CustomTextView) rv.findViewById(R.id.shippingadd_landmark)).setText(mContext.getResources().getString(R.string.streetlandmark));

                                } else {
                                    ((CustomTextView) rv.findViewById(R.id.shippingadd_landmark)).setText(jo.getString("landmark"));
                                }

                                if (jo.getString("street").equals("")) {
                                    ((CustomTextView) rv.findViewById(R.id.shippingadd_streetname)).setText(mContext.getResources().getString(R.string.street_name));

                                } else {
                                    ((CustomTextView) rv.findViewById(R.id.shippingadd_streetname)).setText(jo.getString("street"));
                                }
                                if (jo1.getString("address").equals("")) {
                                    ((CustomTextView) rv.findViewById(R.id.billing_address)).setText(mContext.getResources().getString(R.string.address));
                                } else {
                                    ((CustomTextView) rv.findViewById(R.id.billing_address)).setText(jo1.getString("address"));

                                }
                                if (jo1.getString("fname").equals("")) {
                                    ((CustomTextView) rv.findViewById(R.id.billing_name)).setText(mContext.getResources().getString(R.string.name));
                                } else {
                                    ((CustomTextView) rv.findViewById(R.id.billing_name)).setText(jo1.getString("fname"));

                                }
                                if (jo1.getString("email").equals("")) {
                                    ((CustomTextView) rv.findViewById(R.id.billing_email)).setText(mContext.getResources().getString(R.string.email));
                                } else {
                                    ((CustomTextView) rv.findViewById(R.id.billing_email)).setText(jo1.getString("email"));
                                }
                                if (jo1.getString("mobile").equals("")) {
                                    ((CustomTextView) rv.findViewById(R.id.billing_mobile)).setText(mContext.getResources().getString(R.string.mob_no));
                                } else {
                                    ((CustomTextView) rv.findViewById(R.id.billing_mobile)).setText(jo1.getString("mobile"));
                                }

                            } else if (jobj.getString("success").equals("-1")) {
                                Status = false;
                                Utility.setSharedPreference(mContext, Constants.SHIPPING_STATUS, "0");

                                ((CustomTextView) rv.findViewById(R.id.shippingadd_uname)).setText(jobj.getString("fname") + " "
                                        + jobj.getString("lname"));
                                ((CustomTextView) rv.findViewById(R.id.shippingadd_email)).setText(jobj.getString("email"));
                                ((CustomTextView) rv.findViewById(R.id.shippingadd_mobile)).setText(jobj.getString("mobile"));


                                if (jobj.getString("address").equals("")) {
                                    ((CustomTextView) rv.findViewById(R.id.billing_address)).setText(mContext.getResources().getString(R.string.address));
                                } else {
                                    ((CustomTextView) rv.findViewById(R.id.billing_address)).setText(jobj.getString("address"));

                                }
                                if (jobj.getString("fname").equals("")) {
                                    ((CustomTextView) rv.findViewById(R.id.billing_name)).setText(mContext.getResources().getString(R.string.name));
                                } else {
                                    ((CustomTextView) rv.findViewById(R.id.billing_name)).setText(jobj.getString("fname"));

                                }
                                if (jobj.getString("email").equals("")) {
                                    ((CustomTextView) rv.findViewById(R.id.billing_email)).setText(mContext.getResources().getString(R.string.email));
                                } else {
                                    ((CustomTextView) rv.findViewById(R.id.billing_email)).setText(jobj.getString("email"));
                                }
                                if (jobj.getString("mobile").equals("")) {
                                    ((CustomTextView) rv.findViewById(R.id.billing_mobile)).setText(mContext.getResources().getString(R.string.mob_no));
                                } else {
                                    ((CustomTextView) rv.findViewById(R.id.billing_mobile)).setText(jobj.getString("mobile"));
                                }

                                JSONObject userdetail = jobj.getJSONObject(Constants.OBJECT);
                                JSONObject jo = userdetail.getJSONObject("shipping_address");
                                JSONObject jo1 = userdetail.getJSONObject("billing_address");

                                String obj2 = jo1.toString();
                                System.out.println("BUNDLE BILLING " + obj2);
                                bill_jsn = new JSONObject(obj2);

                                String obj1 = jo.toString();
                                ship_jasn = new JSONObject(obj1);
                            } else {
                                Ttoast.ShowToast(mContext, jobj.getString(Constants.SERVER_MSG), false);

                            }
                        } catch (JSONException e2) {
                            Ttoast.ShowToast(mContext, getResources().getString(R.string.server_fail), false);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

    public void selectImage() {

      /*  final CharSequence[] options = {(mContext.getResources().getString(R.string.from_camera)),
                (mContext.getResources().getString(R.string.from_gallary)), mContext.getResources().getString(R.string.close)};

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(mContext.getResources().getString(R.string.attached_priscription));
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals((mContext.getResources().getString(R.string.from_camera)))) {
                    Uri outputFileUri = getCaptureImageOutputUri();
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                    startActivityForResult(intent, 1);
                } else if (options[item].equals(mContext.getResources().getString(R.string.from_gallary))) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                } else if (options[item].equals(mContext.getResources().getString(R.string.close))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();*/
        final CharSequence[] options = {getResources().getString(R.string.from_camera),
                getResources().getString(R.string.from_gallary), getResources().getString(R.string.close)};
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext);
        builder.setTitle(mContext.getResources().getString(R.string.attached_priscription));
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Uri outputFileUri = getCaptureImageOutputUri();
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                    startActivityForResult(intent, 1);
                } else if (item == 1) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                } else if (item == 2) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    /*---------------------------------FOR IMAGE SELECTION----------------------------------------*/

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 1) {
                Uri imageUri = getPickImageResultUri(data);
                beginCrop(imageUri);
            } else if (requestCode == 2) {
                Uri selectedImage = data.getData();
                beginCrop(selectedImage);
            } else if (requestCode == Crop.REQUEST_CROP) {
                handleCrop(resultCode, data);
            }
        }
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            File getImage = mContext.getExternalCacheDir();
            if (getImage != null) {
                outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
            }
        } else {
            File getImage = mContext.getExternalCacheDir();
            if (getImage != null) {
                outputFileUri = FileProvider.getUriForFile(mContext, com.sehatyuser24.BuildConfig.APPLICATION_ID + ".provider",
                        new File(getImage.getPath(), "pickImageResult.jpeg"));
            }
        }
        return outputFileUri;
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(mContext.getCacheDir(), System.currentTimeMillis() + "cropped.jpeg"));
        Crop.of(source, destination).asSquare().start(mContext, this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == Activity.RESULT_OK) {
            File f = new File(Crop.getOutput(result).getPath());

            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            String croped_file = "" + Crop.getOutput(result).getPath();
            Bitmap roteted = Utility.loadBitmap(Crop.getOutput(result).toString());
            roteted = Bitmap.createScaledBitmap(roteted, 300, 300, true);
            roteted = Utility.rotateImage(roteted, f);
           /* picture_path = Utility.encodeTobase64(roteted);
            System.out.println("PICTURE PATH=======>" + picture_path);*/
            if (imagepath1.equals("")) {
//                imagepath1 = Utility.encodeTobase64(roteted);
                imagepath1 = croped_file;
                System.out.println("ENCODED IMAGE============>" + imagepath1);
            } else if (!imagepath1.equals("") && imagepath2.equals("")) {
//                imagepath2 = Utility.encodeTobase64(roteted);
                imagepath2 = croped_file;
                System.out.println("ENCODED IMAGE============>" + imagepath2);
            } else if (!imagepath1.equals("") && !imagepath2.equals("") && imagepath3.equals("")) {
//                imagepath3 = Utility.encodeTobase64(roteted);
                imagepath3 = croped_file;
                System.out.println("ENCODED IMAGE============>" + imagepath3);
            } else if (!imagepath1.equals("") && !imagepath2.equals("") && !imagepath3.equals("") &&
                    imagepath4.equals("")) {
                imagepath4 = croped_file;
//                imagepath4 = Utility.encodeTobase64(roteted);
                System.out.println("ENCODED IMAGE============>" + imagepath4);
            } else if (!imagepath1.equals("") && !imagepath2.equals("") && !imagepath3.equals("")
                    && !imagepath4.equals("") && imagepath5.equals("")) {
                imagepath5 = croped_file;
//  imagepath5 = Utility.encodeTobase64(roteted);
                System.out.println("ENCODED IMAGE============>" + imagepath5);
            }
            getpicturepath = croped_file;
            imgPaths.add(getpicturepath);

/*
            for (int i = 0; i < imgPaths.size(); i++) {
                String singleimg = imgPaths.get(i);
                Log.w("SINGLE IMAGE====>", singleimg);
            }*/
            setAdapter = new ImageViewHori(mContext, R.layout.adap_pricription_singleimge, imgPaths);
            pic_list.setAdapter(setAdapter);

        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(mContext, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edit_shipping_address_btn:
                if (Status == false) {
                    startActivity(new Intent(mContext, Activity_AddShippingAddress.class));
                } else {
                    try {
                        System.out.println("SHIPPING BUNDLE " + ship_jasn);
                        Intent in = new Intent(mContext, Activity_AddShippingAddress.class);
                        bun.putString(Constants.ShippingBundle, ship_jasn.toString());
                        in.putExtras(bun);
                        startActivity(in);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

            case R.id.add_billing_address_btn:
                try {
                    System.out.println("BILLING BUNDLE " + bill_jsn);
                    Intent in = new Intent(mContext, Activity_AddBillingAddress.class);
                    bun.putString(Constants.BillingBundle, bill_jsn.toString());
                    in.putExtras(bun);
                    startActivity(in);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.cnfrm_prescription_order:
//                Utility.setIntegerSharedPreference(mContext,Constants.UPLOADPRESCACTION,0);
                if (imgPaths.size() == 0) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.selectpic), false);
                } else if (Utility.isConnectingToInternet(mContext)) {
                    new UploadPrescription().execute();
                } else {
                    UploadPresc_Dialog();
//                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
                }
                break;

            default:
                break;
        }
    }

    private void requestCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), CAMERA)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(getActivity(), new String[]{CAMERA, WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE}, 1);
    }

    public void UploadPresc_Dialog() {
        dia_upload_presc = new Dialog(mContext, R.style.MyDialog);
        dia_upload_presc.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dia_upload_presc.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        dia_upload_presc.setContentView(R.layout.dialog_alert_msg);
        dia_upload_presc.setCancelable(true);
        dia_upload_presc.show();
        CustomTextView alert_txt;
        String s = mContext.getResources().getString(R.string.alert_msg_upload_presc);


        alert_txt = (CustomTextView) dia_upload_presc.findViewById(R.id.alert_txt);

        alert_txt.setText(Html.fromHtml(s));

        dia_upload_presc.findViewById(R.id.alert_btn_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.InsertImage(imagepath1, imagepath2, imagepath3, imagepath4, imagepath5);
                if (dia_upload_presc.isShowing()) {
                    dia_upload_presc.dismiss();
                }
            }
        });

        dia_upload_presc.findViewById(R.id.dialog_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dia_upload_presc != null && dia_upload_presc.isShowing()) {
                    dia_upload_presc.cancel();
                }
            }
        });

        dia_upload_presc.findViewById(R.id.alert_cancel_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dia_upload_presc != null && dia_upload_presc.isShowing()) {
                    dia_upload_presc.cancel();
                }
            }
        });

    }

    public class ImageViewHori extends RecyclerView.Adapter<ImageViewHori.SimpleViewHolder> {

        Context appContext;
        ArrayList<String> imglist;
        int res;

        public ImageViewHori(Context context, int res, ArrayList<String> mlist) {
            this.appContext = context;
            this.res = res;
            this.imglist = mlist;

        }

        @Override
        public ImageViewHori.SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View convertView = LayoutInflater.from(parent.getContext()).inflate(res, parent, false);
            return new SimpleViewHolder(convertView);
        }

        @Override
        public void onBindViewHolder(ImageViewHori.SimpleViewHolder holder, int position) {
            String imageString = imglist.get(position);
            System.out.println("ADAP_IMAGEPATH=====>" + imglist);
           /* byte[] decodedString = Base64.decode(imageString, Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            holder.presc_singleimage.setImageBitmap(decodedByte);*/
            Glide.with(appContext)
                    .load(imageString)
                    /*.listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {

                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {

                            return false;
                        }
                    })*/
                    .into(holder.presc_singleimage);

            holder.img_delete.setOnClickListener(new DeleteImg(position));
        }

        @Override
        public int getItemCount() {
            return imglist.size();
        }

        class DeleteImg implements View.OnClickListener {
            int position;

            public DeleteImg(int pos) {
                position = pos;
            }

            @Override
            public void onClick(View view) {
                imglist.remove(position);
                notifyDataSetChanged();
                System.out.println("ArraySize=====" + imglist.size());
            }
        }

        class SimpleViewHolder extends RecyclerView.ViewHolder {
            ImageView presc_singleimage, img_delete;

            public SimpleViewHolder(View v) {
                super(v);
                presc_singleimage = (ImageView) v.findViewById(R.id.presc_singleimage);
                img_delete = (ImageView) v.findViewById(R.id.img_delete);
            }
        }
    }

    public class UploadPrescription extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
        }

        @Override
        protected String doInBackground(String... params) {

/*http://infograins.com.208-91-199-7.md-plesk-web2.webhostbox.net/INFO01/pharmacy/api/api.php?action=
UploadPrescription&product_id=1&prescription_image=files_type*/
            try {
                MultiPartFileUpload multipart = new MultiPartFileUpload(mContext, "UTF-8");
                if (Utility.getIngerSharedPreferences(mContext, Constants.UPLOADPRESCACTION) == 1) {
                    multipart.addFormField(Constants.ACTION, Constants.CARTPRESCRIPTION);
                } else if (Utility.getIngerSharedPreferences(mContext, Constants.UPLOADPRESCACTION) == 0) {
                    multipart.addFormField(Constants.ACTION, Constants.UPLOADPRESCRIPTION);
                }
                multipart.addFormField(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
                if (imgPaths.size() > 0) {
                    for (int i = 0; i < (imgPaths.size()); i++) {
                        if (!(imgPaths.get(i).equals("") && (imgPaths.get(i) != null))) {
                            multipart.addFilePart("prescription_image[]", new File(imgPaths.get(i)));
                            System.out.println("sourceFile[i] " + imgPaths.get(i));
                        }
                    }
                }
                multipart.addFormField("prescription_image[]", imgPaths.toString());

                return multipart.finish();
            } catch (Exception e) {
                e.printStackTrace();

                return mContext.getResources().getString(R.string.server_fail);
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Utility.HideDialog();
            System.out.println("Server result ====> " + result);
            if (result == null) {
                Ttoast.show(mContext, mContext.getResources().getString(R.string.server_fail), true);

            } else {
                try {
                    JSONObject jobj = new JSONObject(result);
                    if (jobj.get(Constants.RESULT_KEY).equals("1")) {
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.upload_presc_success), false);
//                        startActivity(new Intent(mContext, Activity_AddtoCart_List.class));
                        if (Utility.getIngerSharedPreferences(mContext, Constants.UPLOADPRESCACTION) == 1) {
                            startActivity(new Intent(mContext, Activity_AddtoCart_List.class));
                        } else if (Utility.getIngerSharedPreferences(mContext, Constants.UPLOADPRESCACTION) == 0) {
                            ((MainActivity) mContext).callSelectedFragment(Constants.MYPRESCORDERFRAGMENT,
                                    mContext.getResources().getString(R.string.my_pres));
                        }
                    } else {
                        Utility.showCroutonWarn(jobj.getString(Constants.SERVER_MSG), getActivity());
                    }
                } catch (JSONException e) {
                    Utility.showCroutonWarn(getResources().getString(R.string.server_fail), getActivity());
                }
            }
            super.onPostExecute(result);
        }

    }
}
