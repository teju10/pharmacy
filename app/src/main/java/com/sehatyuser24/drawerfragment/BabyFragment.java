package com.sehatyuser24.drawerfragment;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.sehatyuser24.R;


/**
 * Created by nilesh on 11/4/17.
 */

public class BabyFragment extends Fragment {

    Toolbar toolbar;
    CollapsingToolbarLayout collapsingtoolbar;

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_account, container, false);

        toolbar = (Toolbar) rootView.findViewById(R.id.toolbarprovidernav2);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.baby));
        ((ImageView) getActivity().findViewById(R.id.imag_tb)).setVisibility(View.GONE);

        return rootView;
    }


}
