package com.sehatyuser24.utility;

public class Constants {
    public static final String ACTION = "action";

    public static final String MYFOLDER = "octobooking";
    public static final String RESULT_KEY = "success";
    public static final String SERVER_MSG = "msg";
    public static final String USER_LOGOUT = "User_Logout";
    public static final String USERID = "userid";
    public static final String OBJECT = "object";
    public static final String SPLASHCK = "SPLASHCK";
    public static final String AMOUNT = "amount";
    public static final String SHIPPING_CHARGES = "shipping_charge";
    public static final String TRANSACTION_ID = "transaction_id";
    public static final String PAYMENT_METHOD = "payment_method";
    public static final String PAYMENT_TYPE = "payment_type";
    public static final String PAYMENT_BTN = "payment_btn";
    public static final String PRSCORDER_DETAIL = "prescriptionCreateOrderById";
    public static final String COUPON_ID = "coupon_id";
    public static final String TOTAL_DIS = "totol_discount";

    /*ACTION*/

    public static final String REGISTRATION = "Register";
    public static final String LOGIN = "Login";
    public static final String LOGOUT = "Logout";
    public static final String FPASSWORD = "ForgotPassword";
    public static final String ACCOUNTVERIFY = "AccountVerify";
    public static final String S_LOGIN = "SocialLogin";
    public static final String GETPROFILEID = "ProfileById";
    public static final String UPDATEPROFILE = "UpdateSaveProfile";
    public static final String SELLINGPRODUCTLIST = "SellingProduct";
    public static final String RESETPW = "ChangePassword";
    public static final String SELLINGPRODUCT_DETAIL = "SellingProductDetailById";
    public static final String UPLOADPRESCRIPTION = "UploadPrescription";
    public static final String CARTPRESCRIPTION = "CartPrescriptionByUserId";
    public static final String ADDTOCART = "AddToCart";
    public static final String CARTLIST = "CartListByUserId";
    public static final String REMOVESINGLECART = "RemoveProductById";
    public static final String ResendEmailVerification = "ResendVerificationCode";
    public static final String ADD_SHIPPINGADDRESS = "AddShippingAddressByUserId";
    public static final String UPDATESHIPPINGADDRESS = "UpdateShippingAddressById";
    public static final String DELIVERYADDRESS = "ListShippingAddressById";
    public static final String SELECTQUANTITY = "CartUpdateById";
    public static final String PAYMENT_CHECKOUT = "OrderPayment";
    public static final String PRSCORDERPAYMENT = "prescriptionCreateOrderPayment";
    public static final String UPDATEBILLINGADDRESS = "UpdateBillingAddress";
    public static final String LISTFAMILYMEMBER = "ListFamilyMember";
    public static final String GETORDERLIST = "getOrderByUserId";
    public static final String GETORDERHIS = "getOrderHistoryByOrderNumber";
    public static final String REORDER = "ReOrderByUser";
    public static final String SHAREPROMOCODE = "SharePromoCode";
    public static final String ORDERNO = "order_number";
    public static final String ORDERID = "orderid";
    public static final String SEARCHMEDICINE = "SearchByProductName";
    public static final String MEDICATIONLIST = "ProductListByCategory";
    public static final String MANUFACTURELIST = "ManufacturerListByCategory";
    public static final String PRODUCTRATTING = "OrderProductRating";
    public static final String SENDPROMOTIONALCODE = "ApplyPromoCode";
    public static final String GETWALLETAMOUNT = "GetWalletAmount";
    public static final String SUBMITFEEDBACK = "SubmitUserFeedBack";
    public static final String OVERALL_ORDERFEED = "OverallOrderFeedback";
    public static final String HELPSUPPORT = "UserPolicyAndContentList";
    public static final String APPLYCOUPEN = "ApplyCouponCode";
    public static final String PRODUCTREVIEWLIST = "AllProductRatingByUser";
    public static final String OFFERLIST = "OfferList";
    public static final String EVERYDAYPRDCT = "getEveryDayProducts";
    public static final String BABYMOTHCARE = "getBabyAndMotherCare";
    public static final String GETPRODCTBYCAT = "getProductsByCatId";
    public static final String GEMALECARE_PRDCT = "getFemaleCareProductList";
    public static final String BTNCLK = "btnclck";
    public static final String SUBTOTAL = "btnclck";
    public static final String GETDOSESLST = "getDosesListByOrderid";
    public static final String NOTITYPE = "type";


    public static final String DEL_STATUS = "status";
    public static final String lay_click = "lay_click";
    public static final String SP_FAMILYMEM = "SP_FAMILYMEM";
    public static final String CONTACTUS = "UserContactUs";
    public static final String CANCELORDER = "CancelledOrderByUser";
    public static final String RECENTVIEWPRODUCT = "RecentViewProduct";
    public static final String PRESCRIPTIONLIST = "prescriptionListUserid";

    public static final String FNAME = "fname";
    public static final String FULLNAME = "fullname";
    public static final String LNAME = "lname";
    public static final String GENDER = "gender";
    public static final String DOB = "dob";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String CITY = "city";
    public static final String DIST = "district";
    public static final String STREET = "street_address";
    public static final String PROFILE_IMAGE = "profile_image";
    public static final String LANDMARK = "landmark";
    public static final String BUILDINGNUMBER = "building_number";
    public static final String FLATNUMBER = "flat_number";
    public static final String FLORNUMBER = "floor_number";
    public static final String DEVICETYPE = "device_type";
    public static final String DEVICEID = "deviceid";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String USERTYPE = "usertype";
    public static final String MOBILE = "mobile";
    public static final String OUTH_ID = "outhid";
    public static final String OUTH_PROVIDER = "outh_provider";
    public static final String LOGINTYPE = "social_login";
    public static final String LANG = "LANG";
    public static final String LANG_PREF = "LANG_PREF";
    public static final String INTRO = "INTRO";
    public static final String PRODUCTID = "product_id";
    public static final String PRODUCTTITLE = "title";
    public static final String QUANTITY = "quantity";
    public static final String CARTID = "cart_id";
    public static final String SHIPPINGTID = "shipping_id";
    public static final String SHIPPING_STATUS = "SHIPPING_STATUS";
    public static final String ShippingCheck = "ShippingCheck";
    public static final String BillingCheck = "BillingCheck";
    public static final String PAGE = "page";
    public static final String GUEST_USER = "guest_user";
    public static final String COUNT = "count";
    public static final String FAMILYMEMBERSINGLEVIEW = "singleview";
    public static final String youneedlogin = "you need to login";
    public static final String SEX = "sex";
    public static final String AGE = "age";
    public static final String RELATION = "relation";
    public static final String HEALTH_STATUS = "health_status";
    public static final String SEARCHKEY = "searchkey";
    public static final String TREATEMENT = "treatment";
    public static final String DOCTORNAME = "doctor";
    public static final String MEMBER_ID = "member_id";
    public static final String CATEGORYID = "cat_id";
    public static final String CATNAME = "category";
    public static final String PROMOCODE = "promo_code";
    public static final String COUPON_CODE = "coupon_code";
    public static final String PRESCRIPTION_STATUS = "prescription_status";
    public static final String PRECRIPTIONID = "prescrition_id";

    public static final String UPLOADSTATUS= "UploadStatus";
    public static final String UPLOADPRESCACTION= "UploadPrescaction";
    public static final String TOTAL_SAV= "TOTAL_SAV";

    /*DELIVERYORDER*/
    public static class STATUS {
        public static final String ASSIGNED = "0";
        public static final String DISPATCH = "4";
        public static final String DELIVERED = "1";
        public static final String CANCEL = "2";

    }

    public static final String UPLOADFRAGMENT = "UploadFragment";
    public static final String MYORDERFRAGMENT = "MyOrderFragment";
    public static final String FAMILYFRAGMENT = "FamillyFragment";
    public static final String MYPRESCORDERFRAGMENT = "MyPrescriptionOrderFrag";

    /* FCM messaging */
//    public static String AUTOADDRESSKEY = "key=AIzaSyDaWJJSLhToOkHrVa6v_-qoeN4X8ZS2KPg";
    public static String AUTOADDRESSKEY = "key=AIzaSyD-p959Iq8fq73nF8E95GSLLFB1awf3qPA";

    public static final String FCMID = "FCM_ID";
    public static final String TOKEN = "deviceid";
    public static String GOOGLE_KEY = "google";
    public static String FACEBOOK_KEY = "facebook";
    public static final String SEND = "send.jpg";
    public static final String ShippingBundle = "ShippingBundle";
    public static final String BillingBundle = "BillingBundle";
    public static final String GETTEXT = "getText().toString()";

}
