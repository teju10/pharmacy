package com.sehatyuser24.utility;


public interface Path {

    String top="M 540.00,0.00\n" +
            "           C 540.00,0.00 540.00,313.00 540.00,313.00\n" +
            "             540.00,313.00 0.00,313.00 0.00,313.00\n" +
            "             0.00,313.00 0.00,0.00 0.00,0.00\n" +
            "             0.00,0.00 540.00,0.00 540.00,0.00 Z\n" +
            "           M 247.00,8.00\n" +
            "           C 247.00,8.00 247.00,29.00 247.00,29.00\n" +
            "             267.18,24.94 268.12,23.30 289.00,28.00\n" +
            "             289.00,28.00 289.00,8.00 289.00,8.00\n" +
            "             289.00,8.00 247.00,8.00 247.00,8.00 Z";






    String bouttom="M 512.00,0.00\n" +
            "           C 512.00,0.00 512.00,297.00 512.00,297.00\n" +
            "             512.00,297.00 0.00,297.00 0.00,297.00\n" +
            "             0.00,297.00 0.00,0.00 0.00,0.00\n" +
            "             0.00,0.00 512.00,0.00 512.00,0.00 Z\n" +
            "           M 248.00,144.00\n" +
            "           C 248.00,144.00 233.00,143.00 233.00,143.00\n" +
            "             233.00,143.00 233.00,162.00 233.00,162.00\n" +
            "             233.00,162.00 275.00,162.00 275.00,162.00\n" +
            "             275.00,162.00 275.00,141.00 275.00,141.00\n" +
            "             261.52,144.03 261.92,144.06 248.00,144.00 Z";

        String left="M 512.00,0.00\n" +
                "           C 512.00,0.00 512.00,297.00 512.00,297.00\n" +
                "             512.00,297.00 0.00,297.00 0.00,297.00\n" +
                "             0.00,297.00 0.00,0.00 0.00,0.00\n" +
                "             0.00,0.00 512.00,0.00 512.00,0.00 Z\n" +
                "           M 233.00,131.00\n" +
                "           C 233.04,133.11 232.93,135.57 234.60,137.15\n" +
                "             236.55,138.98 256.46,143.65 258.67,133.98\n" +
                "             260.10,127.71 252.34,119.41 248.57,115.00\n" +
                "             238.86,103.63 223.83,89.03 215.75,77.00\n" +
                "             215.75,77.00 207.42,61.02 207.42,61.02\n" +
                "             205.74,59.76 202.06,60.00 200.00,60.00\n" +
                "             200.00,60.00 173.00,60.00 173.00,60.00\n" +
                "             173.00,60.00 173.00,102.00 173.00,102.00\n" +
                "             173.00,102.00 233.00,102.00 233.00,102.00\n" +
                "             233.00,102.00 233.00,131.00 233.00,131.00 Z";

        String right="M 512.00,0.00\n" +
                "           C 512.00,0.00 512.00,297.00 512.00,297.00\n" +
                "             512.00,297.00 0.00,297.00 0.00,297.00\n" +
                "             0.00,297.00 0.00,0.00 0.00,0.00\n" +
                "             0.00,0.00 512.00,0.00 512.00,0.00 Z\n" +
                "           M 275.00,31.00\n" +
                "           C 274.96,28.86 275.08,26.46 273.40,24.85\n" +
                "             270.38,21.95 250.99,18.55 249.64,29.00\n" +
                "             248.93,34.49 256.60,43.49 260.00,47.42\n" +
                "             260.00,47.42 293.84,86.00 293.84,86.00\n" +
                "             293.84,86.00 302.56,100.98 302.56,100.98\n" +
                "             304.29,102.24 307.90,102.00 310.00,102.00\n" +
                "             310.00,102.00 336.00,102.00 336.00,102.00\n" +
                "             336.00,102.00 336.00,60.00 336.00,60.00\n" +
                "             336.00,60.00 275.00,60.00 275.00,60.00\n" +
                "             275.00,60.00 275.00,31.00 275.00,31.00 Z";

         String twotop="M 512.00,0.00\n" +
                 "           C 512.00,0.00 512.00,297.00 512.00,297.00\n" +
                 "             512.00,297.00 0.00,297.00 0.00,297.00\n" +
                 "             0.00,297.00 0.00,0.00 0.00,0.00\n" +
                 "             0.00,0.00 512.00,0.00 512.00,0.00 Z\n" +
                 "           M 312.37,144.00\n" +
                 "           C 315.71,129.96 289.69,128.43 287.62,141.01\n" +
                 "             287.16,143.81 287.99,145.51 289.00,148.00\n" +
                 "             289.00,148.00 292.00,147.00 292.00,147.00\n" +
                 "             288.89,137.44 294.77,135.50 303.00,136.00\n" +
                 "             303.00,136.00 286.00,163.00 286.00,163.00\n" +
                 "             286.00,163.00 313.00,162.00 313.00,162.00\n" +
                 "             313.00,162.00 313.00,159.00 313.00,159.00\n" +
                 "             313.00,159.00 303.00,160.00 303.00,160.00\n" +
                 "             305.82,155.50 311.21,148.89 312.37,144.00 Z";







        String fourtop="M 512.00,0.00\n" +
                "           C 512.00,0.00 512.00,297.00 512.00,297.00\n" +
                "             512.00,297.00 0.00,297.00 0.00,297.00\n" +
                "             0.00,297.00 0.00,0.00 0.00,0.00\n" +
                "             0.00,0.00 512.00,0.00 512.00,0.00 Z\n" +
                "           M 319.96,146.01\n" +
                "           C 318.34,147.68 314.96,150.85 314.17,152.94\n" +
                "             312.11,158.38 320.52,156.70 323.00,156.00\n" +
                "             323.00,156.00 323.00,162.00 323.00,162.00\n" +
                "             323.00,162.00 336.00,162.00 336.00,162.00\n" +
                "             336.00,162.00 336.00,157.00 336.00,157.00\n" +
                "             336.00,157.00 340.00,156.00 340.00,156.00\n" +
                "             340.00,156.00 340.00,154.00 340.00,154.00\n" +
                "             340.00,154.00 339.00,153.00 339.00,153.00\n" +
                "             339.00,153.00 336.00,154.00 336.00,154.00\n" +
                "             336.00,154.00 336.00,131.00 336.00,131.00\n" +
                "             329.93,133.88 324.74,141.09 319.96,146.01 Z\n" +
                "           M 323.00,154.00\n" +
                "           C 323.00,154.00 317.00,154.00 317.00,154.00\n" +
                "             317.00,154.00 322.00,148.00 322.00,148.00\n" +
                "             322.00,148.00 323.00,154.00 323.00,154.00 Z";


        String s="M 540.00,0.00\n" +
                "           C 540.00,0.00 540.00,313.00 540.00,313.00\n" +
                "             540.00,313.00 0.00,313.00 0.00,313.00\n" +
                "             0.00,313.00 0.00,0.00 0.00,0.00\n" +
                "             0.00,0.00 540.00,0.00 540.00,0.00 Z\n" +
                "           M 30.00,219.29\n" +
                "           C 23.82,220.95 16.91,224.55 14.70,231.01\n" +
                "             13.99,233.09 13.96,235.81 14.02,238.00\n" +
                "             14.32,249.72 23.70,251.57 33.00,253.66\n" +
                "             35.64,254.26 46.28,256.45 44.10,260.74\n" +
                "             42.89,263.14 35.89,265.79 33.00,258.00\n" +
                "             33.00,258.00 14.00,258.00 14.00,258.00\n" +
                "             14.03,260.87 13.92,263.26 15.00,266.00\n" +
                "             21.97,283.65 57.03,283.11 63.15,265.00\n" +
                "             67.60,251.83 56.86,245.72 46.00,243.03\n" +
                "             42.96,242.28 35.50,241.15 33.74,238.59\n" +
                "             31.65,235.54 35.53,234.10 38.00,234.16\n" +
                "             41.80,234.26 42.91,235.71 44.00,239.00\n" +
                "             44.00,239.00 64.00,239.00 64.00,239.00\n" +
                "             62.45,221.89 45.04,215.25 30.00,219.29 Z";




    String e="M 512.00,0.00\n" +
            "           C 512.00,0.00 512.00,297.00 512.00,297.00\n" +
            "             512.00,297.00 0.00,297.00 0.00,297.00\n" +
            "             0.00,297.00 0.00,0.00 0.00,0.00\n" +
            "             0.00,0.00 512.00,0.00 512.00,0.00 Z\n" +
            "           M 112.21,222.00\n" +
            "           C 102.10,207.88 78.02,206.19 66.19,219.04\n" +
            "             59.24,226.58 58.89,233.40 59.00,243.00\n" +
            "             59.41,277.44 108.85,280.13 116.00,250.00\n" +
            "             116.00,250.00 102.00,250.00 102.00,250.00\n" +
            "             92.09,250.06 88.55,257.83 81.38,251.43\n" +
            "             79.97,250.17 79.02,248.56 78.00,247.00\n" +
            "             78.00,247.00 117.00,247.00 117.00,247.00\n" +
            "             117.73,237.68 117.96,230.03 112.21,222.00 Z\n" +
            "           M 98.00,235.00\n" +
            "           C 98.00,235.00 78.00,235.00 78.00,235.00\n" +
            "             82.76,224.69 93.61,224.29 98.00,235.00 Z";



        String h="M 512.00,0.00\n" +
                "           C 512.00,0.00 512.00,297.00 512.00,297.00\n" +
                "             512.00,297.00 0.00,297.00 0.00,297.00\n" +
                "             0.00,297.00 0.00,0.00 0.00,0.00\n" +
                "             0.00,0.00 512.00,0.00 512.00,0.00 Z\n" +
                "           M 130.00,193.00\n" +
                "           C 130.00,193.00 130.00,270.00 130.00,270.00\n" +
                "             130.00,270.00 149.00,270.00 149.00,270.00\n" +
                "             149.00,270.00 149.00,246.00 149.00,246.00\n" +
                "             149.00,241.84 148.50,236.75 150.57,233.04\n" +
                "             155.03,225.05 168.75,225.58 169.00,239.00\n" +
                "             169.00,239.00 169.00,270.00 169.00,270.00\n" +
                "             169.00,270.00 188.00,270.00 188.00,270.00\n" +
                "             188.00,270.00 188.00,242.00 188.00,242.00\n" +
                "             188.00,236.73 188.45,229.93 186.75,225.00\n" +
                "             183.51,215.58 172.95,208.78 163.00,210.33\n" +
                "             157.46,211.19 153.36,213.67 149.00,217.00\n" +
                "             149.00,217.00 149.00,193.00 149.00,193.00\n" +
                "             149.00,193.00 130.00,193.00 130.00,193.00 Z";



    String a="M 512.00,0.00\n" +
            "           C 512.00,0.00 512.00,297.00 512.00,297.00\n" +
            "             512.00,297.00 0.00,297.00 0.00,297.00\n" +
            "             0.00,297.00 0.00,0.00 0.00,0.00\n" +
            "             0.00,0.00 512.00,0.00 512.00,0.00 Z\n" +
            "           M 203.28,226.00\n" +
            "           C 196.65,239.25 200.56,258.33 213.00,266.78\n" +
            "             216.73,269.31 220.54,270.32 225.00,270.68\n" +
            "             232.31,271.26 237.53,268.48 243.00,264.00\n" +
            "             243.00,264.00 243.00,270.00 243.00,270.00\n" +
            "             243.00,270.00 262.00,270.00 262.00,270.00\n" +
            "             262.00,270.00 262.00,211.00 262.00,211.00\n" +
            "             262.00,211.00 243.00,211.00 243.00,211.00\n" +
            "             243.00,211.00 243.00,217.00 243.00,217.00\n" +
            "             229.15,205.05 211.25,210.06 203.28,226.00 Z\n" +
            "           M 233.00,253.76\n" +
            "           C 218.13,255.96 213.01,231.17 230.00,227.65\n" +
            "             247.71,227.62 246.10,251.83 233.00,253.76 Z";


    String t="M 512.00,0.00\n" +
            "           C 512.00,0.00 512.00,297.00 512.00,297.00\n" +
            "             512.00,297.00 0.00,297.00 0.00,297.00\n" +
            "             0.00,297.00 0.00,0.00 0.00,0.00\n" +
            "             0.00,0.00 512.00,0.00 512.00,0.00 Z\n" +
            "           M 280.00,197.00\n" +
            "           C 280.00,197.00 280.00,211.00 280.00,211.00\n" +
            "             280.00,211.00 275.00,211.00 275.00,211.00\n" +
            "             275.00,211.00 275.00,227.00 275.00,227.00\n" +
            "             275.00,227.00 280.00,227.00 280.00,227.00\n" +
            "             280.00,227.00 280.00,244.00 280.00,244.00\n" +
            "             280.00,255.41 279.47,265.89 293.00,269.39\n" +
            "             296.70,270.35 306.66,270.00 311.00,270.00\n" +
            "             311.00,270.00 311.00,254.00 311.00,254.00\n" +
            "             307.97,253.99 303.78,254.30 301.31,252.26\n" +
            "             298.59,250.02 299.01,246.16 299.00,243.00\n" +
            "             299.00,243.00 299.00,227.00 299.00,227.00\n" +
            "             299.00,227.00 311.00,227.00 311.00,227.00\n" +
            "             311.00,227.00 311.00,211.00 311.00,211.00\n" +
            "             311.00,211.00 299.00,211.00 299.00,211.00\n" +
            "             299.00,211.00 299.00,197.00 299.00,197.00\n" +
            "             299.00,197.00 280.00,197.00 280.00,197.00 Z";



    String y="M 540.00,0.00\n" +
            "           C 540.00,0.00 540.00,313.00 540.00,313.00\n" +
            "             540.00,313.00 0.00,313.00 0.00,313.00\n" +
            "             0.00,313.00 0.00,0.00 0.00,0.00\n" +
            "             0.00,0.00 540.00,0.00 540.00,0.00 Z\n" +
            "           M 330.00,219.00\n" +
            "           C 330.00,219.00 344.42,256.00 344.42,256.00\n" +
            "             344.42,256.00 351.58,276.00 351.58,276.00\n" +
            "             351.58,276.00 348.60,285.00 348.60,285.00\n" +
            "             348.60,285.00 340.00,305.00 340.00,305.00\n" +
            "             340.00,305.00 352.00,305.00 352.00,305.00\n" +
            "             352.00,305.00 359.59,303.98 359.59,303.98\n" +
            "             359.59,303.98 368.46,286.00 368.46,286.00\n" +
            "             368.46,286.00 388.60,239.00 388.60,239.00\n" +
            "             388.60,239.00 397.00,219.00 397.00,219.00\n" +
            "             397.00,219.00 377.65,219.99 377.65,219.99\n" +
            "             377.65,219.99 372.50,231.00 372.50,231.00\n" +
            "             372.50,231.00 363.00,255.00 363.00,255.00\n" +
            "             363.00,255.00 352.00,219.00 352.00,219.00\n" +
            "             352.00,219.00 330.00,219.00 330.00,219.00 Z";



    String two="M 512.00,0.00\n" +
            "           C 512.00,0.00 512.00,297.00 512.00,297.00\n" +
            "             512.00,297.00 0.00,297.00 0.00,297.00\n" +
            "             0.00,297.00 0.00,0.00 0.00,0.00\n" +
            "             0.00,0.00 512.00,0.00 512.00,0.00 Z\n" +
            "           M 430.24,239.28\n" +
            "           C 433.70,235.18 436.94,230.11 438.64,225.00\n" +
            "             448.44,195.58 407.68,182.73 392.57,203.01\n" +
            "             388.95,207.86 388.05,214.13 388.00,220.00\n" +
            "             390.99,220.00 402.69,220.37 404.69,219.40\n" +
            "             408.09,217.76 407.45,210.19 414.00,210.37\n" +
            "             423.15,210.63 419.71,222.36 416.64,227.00\n" +
            "             411.71,234.45 404.66,239.96 398.09,245.86\n" +
            "             395.53,248.16 390.47,252.16 389.02,255.01\n" +
            "             387.52,257.98 388.00,265.44 388.00,269.00\n" +
            "             388.00,269.00 441.00,269.00 441.00,269.00\n" +
            "             441.00,269.00 441.00,253.00 441.00,253.00\n" +
            "             441.00,253.00 416.00,252.00 416.00,252.00\n" +
            "             421.34,249.54 426.47,243.76 430.24,239.28 Z";


    String four="M 540.00,0.00\n" +
            "           C 540.00,0.00 540.00,313.00 540.00,313.00\n" +
            "             540.00,313.00 0.00,313.00 0.00,313.00\n" +
            "             0.00,313.00 0.00,0.00 0.00,0.00\n" +
            "             0.00,0.00 540.00,0.00 540.00,0.00 Z\n" +
            "           M 518.00,216.00\n" +
            "           C 518.00,213.34 518.48,205.49 516.98,203.60\n" +
            "             515.22,201.38 508.70,202.00 506.00,202.00\n" +
            "             503.75,202.00 500.22,201.86 498.18,202.74\n" +
            "             494.45,204.36 488.46,214.35 485.86,218.00\n" +
            "             485.86,218.00 471.34,239.00 471.34,239.00\n" +
            "             469.38,241.86 465.41,246.93 464.51,250.00\n" +
            "             463.72,252.67 464.00,261.69 464.00,265.00\n" +
            "             464.00,265.00 499.00,265.00 499.00,265.00\n" +
            "             499.00,265.00 499.00,278.00 499.00,278.00\n" +
            "             499.00,278.00 518.00,278.00 518.00,278.00\n" +
            "             518.00,278.00 518.00,265.00 518.00,265.00\n" +
            "             518.00,265.00 526.00,265.00 526.00,265.00\n" +
            "             526.00,265.00 526.00,248.00 526.00,248.00\n" +
            "             526.00,248.00 518.00,248.00 518.00,248.00\n" +
            "             518.00,248.00 518.00,216.00 518.00,216.00 Z\n" +
            "           M 500.00,248.00\n" +
            "           C 500.00,248.00 486.00,248.00 486.00,248.00\n" +
            "             486.00,248.00 499.00,226.00 499.00,226.00\n" +
            "             499.00,226.00 500.00,248.00 500.00,248.00 Z";


}
