package com.sehatyuser24.activity.other;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.bumptech.glide.Glide;
import com.paypal.android.sdk.m;
import com.sehatyuser24.R;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.customwidget.LoadMoreListView;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.Constant_Urls;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeSet;

/**
 * Created by and-04 on 15/6/17.
 */

public class Activity_Medication_Medicine extends AppCompatActivity {

    Context mContext;
    ResponseTask rt;
    private static final int TYPE_ITEM = 0;
    private static final int TYPE_SEPARATOR = 1;
    MedicineListAdap adap;
    Bundle b;
    String Cat_Id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medication_medicine);

        Utility.ChangeLang(getApplicationContext(),Utility.getIngerSharedPreferences(getApplicationContext(),Constants.LANG));

        mContext = this;

        setSupportActionBar((Toolbar) findViewById(R.id.h_toolbar));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((Toolbar) findViewById(R.id.h_toolbar)).setNavigationIcon(R.drawable.ic_navigate_before_white);
        ((CustomTextView) findViewById(R.id.app_name)).setText
                (mContext.getResources().getString(R.string.prescri));

        b = new Bundle();
        b = getIntent().getExtras();
        Cat_Id = b.getString(Constants.CATEGORYID);
        System.out.println("CATIDANOTHER===========>" + Cat_Id);

        if (Utility.isConnectingToInternet(mContext)) {
            GetMedicationProductlist();
        } else {
            Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
        }

        Utility.hideKeyboard(mContext);
    }

    public void GetMedicationProductlist() {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, Constants.MANUFACTURELIST);
            jsonObject.put(Constants.CATEGORYID, Cat_Id);
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(mContext, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    System.out.println("server Result=====>" + result);
                    if (result == null) {
                        Ttoast.show(mContext, getResources().getString(R.string.server_fail), false);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(Constants.RESULT_KEY).equals("1")) {
                                adap = new MedicineListAdap(mContext);
                                HashMap<String, String> hm;
                                JSONArray jsonArray = jobj.getJSONArray("object");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    hm = new HashMap<String, String>();
                                    hm.put("manufacture_name", jsonArray.getJSONObject(i).getString("manufacture_name"));
                                    adap.addSectionHeaderItem(hm);
                                    JSONArray jsonArray1 = jsonArray.getJSONObject(i).getJSONArray("medicine_name");
                                    for (int j = 0; j < jsonArray1.length(); j++) {
                                        hm = new HashMap<String, String>();
                                        hm.put("product_id", jsonArray1.getJSONObject(j).getString("product_id"));
                                        hm.put("title", jsonArray1.getJSONObject(j).getString("title"));
                                        adap.addItem(hm);
                                    }
                                }

                                ((ListView) findViewById(R.id.all_medicine_lst)).setAdapter(adap);
                            } else {
                                Ttoast.show(mContext, jobj.getString(Constants.SERVER_MSG), false);
                            }
                        } catch (JSONException e2) {
                            e2.printStackTrace();
                            Ttoast.show(mContext, mContext.getResources().getString(R.string.server_fail), false);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public class MedicineListAdap extends BaseAdapter {
        Context mContext;
        ArrayList<HashMap<String, String>> list = new ArrayList<>();

        private TreeSet<Integer> sectionHeader = new TreeSet<Integer>();

        public MedicineListAdap(Context mContext) {
            this.mContext = mContext;
        }

        public void addItem(HashMap<String, String> item) {
            list.add(item);
            notifyDataSetChanged();
        }

        public void addSectionHeaderItem(HashMap<String, String> item) {
            list.add(item);
            sectionHeader.add(list.size() - 1);
            notifyDataSetChanged();
        }

        @Override
        public int getItemViewType(int position) {
            return sectionHeader.contains(position) ? TYPE_SEPARATOR : TYPE_ITEM;
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final ViewHolder viewHolder;
            int rowType = getItemViewType(position);
            LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                viewHolder = new ViewHolder();
                switch (rowType) {
                    case TYPE_SEPARATOR:
                        convertView = li.inflate(R.layout.medication_medicine_single_header, null);
                        viewHolder.chatlist_header = (CustomTextView) convertView.findViewById(R.id.medi_list_header);
                        viewHolder.chatlist_header.setText(list.get(position).get("manufacture_name"));

                        break;
                    case TYPE_ITEM:
                        convertView = li.inflate(R.layout.medication_medicine_single_child, null);
                        viewHolder.chat_username = (CustomTextView) convertView.findViewById(R.id.medi_list_child);

                        viewHolder.chat_username.setText(list.get(position).get("title"));
//                        convertView.setOnClickListener(new MyClick(position));
                        break;
                }
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            switch (rowType) {
                case TYPE_SEPARATOR:
                    viewHolder.chatlist_header.setText(list.get(position).get("manufacture_name"));
                    break;
                case TYPE_ITEM:
                    viewHolder.chat_username.setText(list.get(position).get("title"));
                    convertView.setOnClickListener(new MyClick(position));
                    break;
            }

            return convertView;
        }

        class MyClick implements View.OnClickListener {
            int position;

            public MyClick(int mposition) {
                position = mposition;
            }

            @Override
            public void onClick(View v) {
                try {

                    mContext.startActivity(new Intent(mContext, ActivityProductDetail.class).
                            putExtra(Constants.PRODUCTID, list.get(position).get("product_id")));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        public class ViewHolder {
            CustomTextView chat_username, chatlist_header;
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
}
