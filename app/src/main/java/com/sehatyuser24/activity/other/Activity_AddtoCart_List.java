package com.sehatyuser24.activity.other;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;

import com.bumptech.glide.Glide;
import com.sehatyuser24.R;
import com.sehatyuser24.activity.registration.MainActivity;
import com.sehatyuser24.adapter.RelatedHorizontalListAdapter;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Helper;
import com.sehatyuser24.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Tejas on 9/5/17.
 */

public class Activity_AddtoCart_List extends AppCompatActivity implements View.OnClickListener {
    Context mContext;
    ListView cart_item_list;
    AddCartItemAdapter adap;
    ResponseTask rt;
    ArrayList<JSONObject> jlist, relatedarray;
    String grand_total = "", ship_charges = "", upload_presc_status = "", rx_status = "", amount = "", Subtotal = "", ship = "";
    Bundle b;
    JSONObject billjobj, shipjobj;
    Boolean Status = true;
    CustomTextView shipping_delivery_name, shipping_delivery_address, shipping_delivery_mobile, billing_name, billing_email, billing_address, billing_mobile, cart_item_offer;
    RelatedHorizontalListAdapter recent_adap;
    RecyclerView related_pro_list;
    Dialog dia_upload_presc;
    double saving_price, total_saving, discount_percentage;
    boolean CHECK = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utility.ChangeLang(getApplicationContext(), Utility.getIngerSharedPreferences(getApplicationContext(), Constants.LANG));
        setContentView(R.layout.activity_add_to_cart_list);

        mContext = this;
        setSupportActionBar((Toolbar) findViewById(R.id.h_toolbar));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((Toolbar) findViewById(R.id.h_toolbar)).setNavigationIcon(R.drawable.ic_navigate_before_white);
        ((CustomTextView) findViewById(R.id.app_name)).setText(mContext.getResources().getString(R.string.cart));
        b = new Bundle();
        Find();
        Listner();
        Utility.setSharedPreference(mContext, Constants.TOTAL_SAV, "0");
    }

    public void Find() {
        related_pro_list = (RecyclerView) findViewById(R.id.related_list_prodesc);
        shipping_delivery_name = (CustomTextView) findViewById(R.id.shipping_delivery_name);
        shipping_delivery_address = (CustomTextView) findViewById(R.id.shipping_delivery_address);
        shipping_delivery_mobile = (CustomTextView) findViewById(R.id.shipping_delivery_mobile);
        billing_name = (CustomTextView) findViewById(R.id.billing_name);
        billing_email = (CustomTextView) findViewById(R.id.billing_email);
        billing_address = (CustomTextView) findViewById(R.id.billing_address);
        billing_mobile = (CustomTextView) findViewById(R.id.billing_mobile);
        cart_item_offer = (CustomTextView) findViewById(R.id.cart_item_offer);
    }


    @Override
    protected void onResume() {
        if (Utility.isConnectingToInternet(mContext)) {

            if (Utility.getSharedPreferences(mContext, Constants.TOTAL_SAV).equals("1")) {
                GetCartList();
//                Utility.ClearSP(mContext);
            }
        } else {
            Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
        }
        super.onResume();
    }

    public void Listner() {
        cart_item_list = (ListView) findViewById(R.id.cart_item_list);
        findViewById(R.id.proceed_to_pay).setOnClickListener(this);
        findViewById(R.id.continue_shop_btn).setOnClickListener(this);
        findViewById(R.id.edit_billaddress_btn).setOnClickListener(this);
        findViewById(R.id.edit_shipaddress_btn).setOnClickListener(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        related_pro_list.setLayoutManager(linearLayoutManager);

        GetCartList();
    }

    public void SetAdapter() {
        adap = new AddCartItemAdapter(mContext, R.layout.adap_cartlist_item, jlist);
        cart_item_list.setAdapter(adap);
        Helper.getListViewSize(cart_item_list);
    }

    public void SetAdapterRecent() {
        recent_adap = new RelatedHorizontalListAdapter(mContext, R.layout.adap_relatedproduct_itemview, relatedarray);
        related_pro_list.setAdapter(recent_adap);
    }

    public void GetCartList() {
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constants.ACTION, Constants.CARTLIST);
            jo.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));

            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.server_fail), false);
                    } else {
                        try {
                            JSONObject j = new JSONObject(result);
                            if (j.getString("success").equals("1")) {
                                String sub = j.getString("sum") + " " + mContext.getResources().getString(R.string.currncy_symbl);
                                ship = j.getString("shipping_charge") + " " + mContext.getResources().getString(R.string.currncy_symbl);
                                String gt = j.getString("grand_total") + " " + mContext.getResources().getString(R.string.currncy_symbl);
                                amount = j.getString("grand_total");

                                upload_presc_status = j.getString("prescription_status");

                                jlist = new ArrayList<JSONObject>();
                                billjobj = new JSONObject();
                                shipjobj = new JSONObject();

                                Subtotal = "";
                                grand_total = "";
                                ship_charges = "";

                                Subtotal = j.getString("sum");
                                grand_total = j.getString("grand_total");
                                ship_charges = j.getString("shipping_charge");
                                ((CustomTextView) findViewById(R.id.cart_item_subtotal)).setText(sub);
                                ((CustomTextView) findViewById(R.id.cart_item_ship_charge)).setText(ship);
                                ((CustomTextView) findViewById(R.id.cart_item_order_total)).setText(gt);

                                JSONObject jsonObject = j.getJSONObject(Constants.OBJECT);
                                JSONObject shipjson = jsonObject.getJSONObject("shipping_address");
                                if (j.getString("shipping_status").equals("0")) {
                                    Status = false;
                                    Utility.setSharedPreference(mContext, Constants.SHIPPING_STATUS, "0");

                                } else {
                                    Utility.setSharedPreference(mContext, Constants.SHIPPING_STATUS, "1");
                                    Status = true;
                                    shipjobj = shipjson;
                                }

                                if (shipjson.getString("fullname").equals("")) {
                                    ((CustomTextView) findViewById(R.id.shipping_delivery_name)).setHint(mContext.getResources().getString(R.string.name));
                                } else {
                                    ((CustomTextView) findViewById(R.id.shipping_delivery_name)).setText(shipjson.getString("fullname"));
                                }

                                if (shipjson.getString("street").equals("")) {
                                    ((CustomTextView) findViewById(R.id.shipping_delivery_address)).setHint(mContext.getResources().getString(R.string.delivery_adress));
                                } else {
                                    ((CustomTextView) findViewById(R.id.shipping_delivery_address)).setText(shipjson.getString("street"));
                                }

                                if (shipjson.getString("mobile").equals("")) {
                                    ((CustomTextView) findViewById(R.id.shipping_delivery_mobile)).setHint(mContext.getResources().getString(R.string.mob_no));
                                } else {
                                    ((CustomTextView) findViewById(R.id.shipping_delivery_mobile)).setText(shipjson.getString("mobile"));
                                }

                                JSONObject billjson = jsonObject.getJSONObject("billing_address");
                                billjobj = billjson;
                                ((CustomTextView) findViewById(R.id.billing_name)).setText(billjson.getString("fname"));
                                ((CustomTextView) findViewById(R.id.billing_address)).setText(billjson.getString("address"));
                                ((CustomTextView) findViewById(R.id.billing_mobile)).setText(billjson.getString("mobile"));
                                ((CustomTextView) findViewById(R.id.billing_email)).setText(billjson.getString("email"));


                                JSONArray jsonArray = jsonObject.getJSONArray("cart_detailt");
                                jlist.clear();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    saving_price = Double.parseDouble(jsonObject1.getString("price")) - Double.parseDouble(jsonObject1.getString("discount_price"));
                                    total_saving += saving_price;

                                    jlist.add(jsonArray.getJSONObject(i));
                                }
                                if (CHECK) {
                                    discount_percentage = (total_saving * 100) / (Double.parseDouble(Subtotal) + total_saving);
                                    cart_item_offer.setText(String.format("%.2f", discount_percentage) + " " + "%");
                                }
                                SetAdapter();

                                /*FOR RELATED LIST*/
                                relatedarray = new ArrayList<JSONObject>();
                                JSONArray relatedarraylist = jsonObject.getJSONArray("similar_product");
                                relatedarray.clear();
                                for (int i = 0; i < relatedarraylist.length(); i++) {
                                    relatedarray.add(relatedarraylist.getJSONObject(i));
                                }

                                SetAdapterRecent();


                            } else if (j.getString("cart_empty").equals("2")) {
                                ((LinearLayout) findViewById(R.id.cart_ll)).setVisibility(View.VISIBLE);
                                ((ScrollView) findViewById(R.id.main)).setVisibility(View.GONE);
                            } else {
                                Ttoast.ShowToast(mContext, j.getString("msg"), true);
                            }
                        } catch (JSONException t) {
                            t.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException j) {
            j.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.proceed_to_pay:
                if (upload_presc_status.equals("0") && rx_status.equals("1")) {
                    UploadPresc_Dialog();
                } else if (shipping_delivery_name.getText().toString().equals("")) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_fullname), false);
                } else if (shipping_delivery_address.getText().toString().equals("")) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_address), false);
                } else if (shipping_delivery_mobile.getText().toString().equals("")) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_mobile), false);
                } else if (billing_name.getText().toString().equals("")) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_billing_name), false);
                } else if (billing_mobile.getText().toString().equals("")) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_billing_mob), false);
                } else if (billing_address.getText().toString().equals("")) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_billing_address), false);
                } else if (billing_email.getText().toString().equals("")) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_billing_email), false);
                } else {
                    Utility.setIntegerSharedPreference(mContext, Constants.PAYMENT_TYPE, 0);
                    Utility.setIntegerSharedPreference(mContext, Constants.PAYMENT_BTN, 0);

                    Intent i = new Intent(mContext, Activity_PaymentDeduct.class);
                    b.putString(Constants.SUBTOTAL, Subtotal);
                    b.putString(Constants.AMOUNT, grand_total);
                    b.putString(Constants.SHIPPING_CHARGES, ship_charges);
                    i.putExtras(b);
                    startActivity(i);
                }
                break;
            case R.id.continue_shop_btn:
                startActivity(new Intent(mContext, MainActivity.class));
                finish();
                break;

            case R.id.edit_billaddress_btn:
                Intent in = new Intent(mContext, Activity_AddBillingAddress.class);
                b.putString(Constants.BillingBundle, billjobj.toString());
                in.putExtras(b);
                startActivity(in);
                break;

            case R.id.edit_shipaddress_btn:

                if (Status == false) {
                    startActivity(new Intent(mContext, Activity_AddShippingAddress.class));
                } else {
                    Intent inn = new Intent(mContext, Activity_AddShippingAddress.class);
                    b.putString(Constants.ShippingBundle, shipjobj.toString());
                    System.out.println("SHPPING STATUS " + shipjobj);
                    inn.putExtras(b);
                    startActivity(inn);
                    break;
                }
        }
    }

    public void UploadPresc_Dialog() {

        dia_upload_presc = new Dialog(mContext, R.style.MyDialog);
        dia_upload_presc.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dia_upload_presc.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        dia_upload_presc.setContentView(R.layout.dialog_alert_msg);
        dia_upload_presc.setCancelable(true);
        dia_upload_presc.show();

        CustomTextView alert_txt, alert_txt_heading;
        String s = mContext.getResources().getString(R.string.presc_mandatory);


        alert_txt = (CustomTextView) dia_upload_presc.findViewById(R.id.alert_txt);
        alert_txt_heading = (CustomTextView) dia_upload_presc.findViewById(R.id.alert_txt_heading);
        alert_txt_heading.setVisibility(View.VISIBLE);
        alert_txt_heading.setText(Html.fromHtml(s));

        dia_upload_presc.findViewById(R.id.alert_btn_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, MainActivity.class).putExtra(Constants.UPLOADSTATUS, "1"));
            }
        });

        dia_upload_presc.findViewById(R.id.dialog_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dia_upload_presc != null && dia_upload_presc.isShowing()) {
                    dia_upload_presc.cancel();
                }
            }
        });

        dia_upload_presc.findViewById(R.id.alert_cancel_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dia_upload_presc != null && dia_upload_presc.isShowing()) {
                    dia_upload_presc.cancel();
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mContext, MainActivity.class));
        finish();
        super.onBackPressed();
    }

  /*  @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {

            super.onActivityResult(requestCode, resultCode, data);
        }

//        super.onActivityResult(requestCode, resultCode, data);
    }*/

    public class AddCartItemAdapter extends BaseAdapter {
        Context mContext;
        ArrayList<JSONObject> jlist;
        int res;
        ResponseTask rt;

        public AddCartItemAdapter(Context context, int res, ArrayList<JSONObject> jobj) {
            this.mContext = context;
            this.res = res;
            this.jlist = jobj;
        }

        @Override
        public int getCount() {
            return jlist.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = li.inflate(res, null);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            JSONObject j = jlist.get(position);
            try {
                String yousave = mContext.getResources().getString(R.string.save_off) + " " + j.getString("discount") + "%";
                rx_status = jlist.get(position).getString("prescription_status");
                viewHolder.medi_c_name.setText(j.getString("brand"));
                viewHolder.add_ship_charges.setText(getResources().getString(R.string.add_ship_charge) + " " + ship);

                String dis_sym = j.getString("discount_price") + mContext.getResources().getString(R.string.currncy_symbl);
                String price = j.getString("price") + mContext.getResources().getString(R.string.currncy_symbl);

                if (j.getString("discount_price").equals("0")) {
                    viewHolder.discount_price.setText(price);
                    viewHolder.cart_price.setVisibility(View.GONE);
                    viewHolder.dis_txt.setVisibility(View.GONE);
                } else {
                    viewHolder.discount_price.setText(dis_sym);
                    viewHolder.cart_price.setPaintFlags(viewHolder.cart_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    viewHolder.cart_price.setText(price);
                }

                viewHolder.medi_name.setText(j.getString("title"));
                viewHolder.dis_txt.setText(yousave);
                viewHolder.quan_txt.setText(j.getString("cart_quantity"));

                if (j.getString(Constants.PRESCRIPTION_STATUS).equals("1")) {
                    viewHolder.pro_det_rx.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.pro_det_rx.setVisibility(View.GONE);
                }

                Glide.with(mContext).load(j.getString("product_image")).into(viewHolder.cart_image);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            viewHolder.remove_cartitem.setOnClickListener(new RemoveItem(position));
            viewHolder.add_quan.setOnClickListener(new SelectSpinnerPlus(position));
            viewHolder.minus_quan.setOnClickListener(new SelectSpinnerMinus(position));
            return convertView;
        }

        public void DeleteAlert(int pos) {
            try {
                final int pos1 = pos;
                final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle(R.string.delete);
                builder.setIcon(R.drawable.ic_launcher);
                builder.setMessage(R.string.doyou_delete)
                        .setCancelable(false).setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        try {
                            RemoveCart(jlist.get(pos1).getString("cart_id"), (pos1));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                        dialog.dismiss();
                    }
                });
                final AlertDialog alert = builder.create();
                alert.show();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        public void RemoveCart(final String cart_pos, final int pos) {

        /*http://infograins.com.208-91-199-7.md-plesk-web2.webhostbox.net/INFO01/pharmacy/api/api.php?
        action=RemoveProductById&cart_id=1*/
            try {
                JSONObject j = new JSONObject();
                j.put(Constants.ACTION, Constants.REMOVESINGLECART);
                j.put(Constants.CARTID, cart_pos);
                Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
                rt = new ResponseTask(mContext, j);
                rt.setListener(new ResponseListener() {
                    @Override
                    public void onGetPickSuccess(String result) {
                        Utility.HideDialog();
                        if (result == null) {
                            Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.server_fail), false);
                        } else {
                            try {
                                JSONObject j = new JSONObject(result);
                                if (j.getString("success").equals("1")) {
                                    Ttoast.show(mContext, mContext.getResources().getString(R.string.removesuccess), false);
                                    jlist.remove(pos);
                                    notifyDataSetChanged();
                                    Helper.getListViewSize(cart_item_list);
                                    Utility.setIntegerSharedPreference(mContext, Constants.COUNT, Integer.parseInt(j.getString("count")));
                                    if (jlist.size() == 0) {
                                        ((LinearLayout) findViewById(R.id.cart_ll)).setVisibility(View.VISIBLE);
                                        ((ScrollView) findViewById(R.id.main)).setVisibility(View.GONE);
                                    }
                                    ((CustomTextView) findViewById(R.id.cart_item_subtotal)).setText("$" + j.getString("sum"));
                                    ((CustomTextView) findViewById(R.id.cart_item_order_total)).setText("$" + j.getString("grand_total"));
                                } else {
                                    Ttoast.ShowToast(mContext, j.getString(Constants.SERVER_MSG), false);
                                }

                            } catch (JSONException je) {
                                je.printStackTrace();
                            }
                        }
                    }
                });
                rt.execute();
            } catch (JSONException h) {
                h.printStackTrace();
            }
        }

        public void SelectQuantity(final String product_id, final int quan) {
            //infograins.com.208-91-199-7.md-plesk-web2.webhostbox.net/INFO01/pharmacy/api/api.php?action=
            // CartUpdateById&userid=24&product_id=2&quantity=2
            try {
                JSONObject j = new JSONObject();
                j.put(Constants.ACTION, Constants.SELECTQUANTITY);
                j.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
                j.put(Constants.PRODUCTID, product_id);
                j.put(Constants.QUANTITY, quan);
                Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
                rt = new ResponseTask(mContext, j);
                rt.setListener(new ResponseListener() {
                    @Override
                    public void onGetPickSuccess(String result) {
                        Utility.HideDialog();
                        if (result == null) {
                            Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.server_fail), false);
                        } else {
                            try {
                                JSONObject j = new JSONObject(result);
                                if (j.getString("success").equals("1")) {
                                    CHECK = false;
                                    notifyDataSetChanged();
                                    jlist.clear();
                                    GetCartList();
                                } else {
                                    Ttoast.ShowToast(mContext, j.getString(Constants.SERVER_MSG), false);
                                }
                            } catch (JSONException je) {
                                je.printStackTrace();
                            }
                        }
                    }
                });
                rt.execute();
            } catch (JSONException h) {
                h.printStackTrace();
            }
        }

        class RemoveItem implements View.OnClickListener {
            Integer position;

            public RemoveItem(int pos) {
                this.position = pos;
            }

            @Override
            public void onClick(View v) {
                DeleteAlert(position);
//            ((Activity_AddtoCart_List)mContext).DeleteAlert(position);
            }
        }

        class SelectSpinnerPlus implements View.OnClickListener {
            int position, quant;
            String dav = "";

            public SelectSpinnerPlus(int pos) {
                this.position = pos;
            }

            @Override
            public void onClick(View v) {
                try {

                    dav = jlist.get(position).getString("cart_quantity");
                    quant = Integer.parseInt(dav) + 1;
                    if (quant > 12) {
                        System.out.println("QUANT " + quant);
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.quant_not_available), false);

                    } else {
                        SelectQuantity(jlist.get(position).getString("product_id"), quant);
                    }
                } catch (JSONException j) {
                    j.printStackTrace();
                }

            }

        }

        class SelectSpinnerMinus implements View.OnClickListener {
            int position, quantpos = 0;

            public SelectSpinnerMinus(int pos) {
                this.position = pos;
            }

            @Override
            public void onClick(View v) {
                try {
                    int dev = Integer.parseInt(jlist.get(position).getString("cart_quantity"));
                    quantpos = dev - 1;
                    if (quantpos < 1) {

                    } else {
                        SelectQuantity(jlist.get(position).getString("product_id"), quantpos);
                    }
                } catch (JSONException j) {
                    j.printStackTrace();
                }
            }

        }

        class ViewHolder {

            CustomTextView medi_name, medi_c_name, add_ship_charges, cart_price,
                    discount_price, quan_txt, dis_txt;
            ImageView cart_image, pro_det_rx;
            LinearLayout remove_cartitem;
            Spinner spnr_quant_ar;
            Button add_quan, minus_quan;


            public ViewHolder(View base) {

                medi_name = (CustomTextView) base.findViewById(R.id.medi_name);
                medi_c_name = (CustomTextView) base.findViewById(R.id.medi_c_name);
                add_ship_charges = (CustomTextView) base.findViewById(R.id.add_ship_charges);
                discount_price = (CustomTextView) base.findViewById(R.id.discount_price);
                quan_txt = (CustomTextView) base.findViewById(R.id.quan_txt);
                cart_image = (ImageView) base.findViewById(R.id.cart_image);
                pro_det_rx = (ImageView) base.findViewById(R.id.pro_det_rx);
                cart_price = (CustomTextView) base.findViewById(R.id.cart_price);
                dis_txt = (CustomTextView) base.findViewById(R.id.dis_txt);
                remove_cartitem = (LinearLayout) base.findViewById(R.id.remove_cartitem);
//                spnr_quant = (Spinner) base.findViewById(R.id.spnr_quant);
                spnr_quant_ar = (Spinner) base.findViewById(R.id.spnr_quant_ar);
                minus_quan = (Button) base.findViewById(R.id.minus_quan);
                add_quan = (Button) base.findViewById(R.id.add_quan);


            }
        }
    }
}
