package com.sehatyuser24.activity.other;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;

import com.sehatyuser24.R;
import com.sehatyuser24.activity.registration.MainActivity;
import com.sehatyuser24.customwidget.CustomButton;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.Constant_Urls;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by and-04 on 4/7/17.
 */

public class Activity_Help_Support extends AppCompatActivity implements View.OnClickListener {

    Context mContext;
    ResponseTask rt;
    CustomTextView privacy_txt, terms_txt, asked_q_txt;
    CustomButton privacy_up_btn, privacy_dwn_btn, terms_up_btn,
            terms_dwn_btn, asked_q_up_btn, asked_q_dwn_btn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Utility.ChangeLang(getApplicationContext(),Utility.getIngerSharedPreferences(getApplicationContext(),Constants.LANG));

        setContentView(R.layout.activity_help_support);
        mContext = this;

        setSupportActionBar((Toolbar) findViewById(R.id.h_toolbar));

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((Toolbar) findViewById(R.id.h_toolbar)).setNavigationIcon(R.drawable.ic_navigate_before_white);
        ((CustomTextView) findViewById(R.id.app_name)).setText(mContext.getResources().getString(R.string.help_support));

        Find();
        Utility.hideKeyboard(mContext);
    }


    public void Find() {

        privacy_txt = (CustomTextView) findViewById(R.id.privacy_txt);
        terms_txt = (CustomTextView) findViewById(R.id.terms_txt);
        asked_q_txt = (CustomTextView) findViewById(R.id.asked_q_txt);

        privacy_up_btn = (CustomButton) findViewById(R.id.privacy_up_btn);
        privacy_dwn_btn = (CustomButton) findViewById(R.id.privacy_dwn_btn);
        terms_up_btn = (CustomButton) findViewById(R.id.terms_up_btn);
        terms_dwn_btn = (CustomButton) findViewById(R.id.terms_dwn_btn);
        asked_q_up_btn = (CustomButton) findViewById(R.id.asked_q_up_btn);
        asked_q_dwn_btn = (CustomButton) findViewById(R.id.asked_q_dwn_btn);

        if (Utility.isConnectingToInternet(mContext)) {
            GetHelpSupportData();
        } else {
            Ttoast.show(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
        }
        findViewById(R.id.privacy_up_btn).setOnClickListener(this);
        findViewById(R.id.terms_up_btn).setOnClickListener(this);
        findViewById(R.id.asked_q_up_btn).setOnClickListener(this);
        findViewById(R.id.privacy_dwn_btn).setOnClickListener(this);
        findViewById(R.id.terms_dwn_btn).setOnClickListener(this);
        findViewById(R.id.asked_q_dwn_btn).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.privacy_up_btn:

                privacy_txt.setVisibility(View.GONE);
                privacy_up_btn.setVisibility(View.GONE);
                privacy_dwn_btn.setVisibility(View.VISIBLE);

                break;

            case R.id.privacy_dwn_btn:

                privacy_up_btn.setVisibility(View.VISIBLE);
                privacy_txt.setVisibility(View.VISIBLE);
                privacy_dwn_btn.setVisibility(View.GONE);

                break;

            case R.id.terms_up_btn:

                terms_dwn_btn.setVisibility(View.VISIBLE);
                terms_txt.setVisibility(View.GONE);
                terms_up_btn.setVisibility(View.GONE);

                break;

            case R.id.terms_dwn_btn:

                terms_dwn_btn.setVisibility(View.GONE);
                terms_txt.setVisibility(View.VISIBLE);
                terms_up_btn.setVisibility(View.VISIBLE);

                break;

            case R.id.asked_q_up_btn:

                asked_q_up_btn.setVisibility(View.GONE);
                asked_q_txt.setVisibility(View.GONE);
                asked_q_dwn_btn.setVisibility(View.VISIBLE);

                break;

            case R.id.asked_q_dwn_btn:

                asked_q_dwn_btn.setVisibility(View.GONE);
                asked_q_up_btn.setVisibility(View.VISIBLE);
                asked_q_txt.setVisibility(View.VISIBLE);

                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void GetHelpSupportData() {

        try {
            JSONObject j = new JSONObject();
            j.put(Constants.ACTION, Constants.HELPSUPPORT);
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));

            rt = new ResponseTask(mContext, j);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.server_fail), false);
                    } else {

                        try {

                            JSONObject j = new JSONObject(result);
                            if (j.getString("success").equals("1")) {

                                JSONObject jo = j.getJSONObject(Constants.OBJECT);
                                String htmlfile1 = jo.getString("term_and_condition");
                                ((CustomTextView) findViewById(R.id.privacy_txt)).
                                        setText(Html.fromHtml(htmlfile1));

                                String htmlfile2 = jo.getString("about_us");
                                ((CustomTextView) findViewById(R.id.terms_txt)).
                                        setText(Html.fromHtml(htmlfile2));

                                String htmlfile3 = jo.getString("help_support");
                                ((CustomTextView) findViewById(R.id.asked_q_txt)).
                                        setText(Html.fromHtml(htmlfile3));

                            } else {
                                Ttoast.show(mContext, j.getString("msg"), false);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
