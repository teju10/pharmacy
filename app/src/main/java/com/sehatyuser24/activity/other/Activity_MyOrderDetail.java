package com.sehatyuser24.activity.other;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.sehatyuser24.R;
import com.sehatyuser24.customwidget.CustomEditText;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.ExpandableHeightListView;
import com.sehatyuser24.utility.Helper;
import com.sehatyuser24.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Tejas on 20/5/17.
 */

public class Activity_MyOrderDetail extends AppCompatActivity {
    public static String Status;
    Context mContext;
    String Order_no;
    ResponseTask rt;
    ExpandableHeightListView address_list;
    PriceAdapter adapter;
    ArrayList<JSONObject> jlist = new ArrayList<>();
    RecyclerView prescribe_image;
    Dialog cancel_status, alertdia;
    LinearLayoutManager horizontalLayoutManager;
    CustomTextView ctv_discount;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Utility.ChangeLang(getApplicationContext(), Utility.getIngerSharedPreferences(getApplicationContext(), Constants.LANG));

        setContentView(R.layout.activity_myorder_detail);
        mContext = this;

        setSupportActionBar((Toolbar) findViewById(R.id.h_toolbar));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((Toolbar) findViewById(R.id.h_toolbar)).setNavigationIcon(R.drawable.ic_navigate_before_white);
        ((CustomTextView) findViewById(R.id.app_name)).setText(mContext.getResources().getString(R.string.order_his));

        Init();
        Utility.hideKeyboard(mContext);
    }

    public void Init() {
        address_list = (ExpandableHeightListView) findViewById(R.id.address_list);
        ctv_discount = (CustomTextView) findViewById(R.id.discount);

        prescribe_image = (RecyclerView) findViewById(R.id.prescribe_image);
        Bundle b = new Bundle();

        if (!b.equals("")) {
            b = getIntent().getExtras();
            Order_no = b.getString(Constants.ORDERNO);
        }

        if (Utility.isConnectingToInternet(mContext)) {
            GetOrderHistory();
        } else {
            Ttoast.show(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
        }

        horizontalLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        prescribe_image.setLayoutManager(horizontalLayoutManager);

        findViewById(R.id.order_cancel_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GiveFeedbackDialog();
            }
        });

        findViewById(R.id.reorder_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Ttoast.show(mContext, mContext.getResources().getString(R.string.c_soon), false);

                AlertReorderask();
            }
        });

        findViewById(R.id.cb_reorder).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Ttoast.show(mContext, mContext.getResources().getString(R.string.c_soon), false);

                AlertReorderask();
            }
        });

        /*findViewById(R.id.dosage_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Ttoast.show(mContext, mContext.getResources().getString(R.string.c_soon), false);

                startActivity(new Intent(mContext, Act_DosageList.class).putExtra(Constants.ORDERNO, Order_no));
            }
        });*/
    }

    public void GetOrderHistory() {
        try {
            JSONObject j = new JSONObject();
            j.put(Constants.ACTION, Constants.GETORDERHIS);
            j.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
            j.put(Constants.ORDERID, Order_no);
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(mContext, j);

            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Ttoast.show(mContext, mContext.getResources().getString(R.string.server_fail), false);
                    } else {
                        try {
                            JSONObject j = new JSONObject(result);
                            if (j.getString("success").equals("1")) {
                                String sb = j.getString("subtotal") + " " + mContext.getResources().getString(R.string.currncy_symbl);
                                String sch = j.getString("shipping_charge") + " " + mContext.getResources().getString(R.string.currncy_symbl);
                                String amt = j.getString("amount") + " " + mContext.getResources().getString(R.string.currncy_symbl);
                                if (j.getString("status").equals("1") || j.getString("status").equals("2")) {
                                    findViewById(R.id.ll_cancel_order).setVisibility(View.GONE);
                                    findViewById(R.id.cb_reorder).setVisibility(View.VISIBLE);
                                }

                                Float discount = Float.parseFloat(j.getString("subtotal")) + Float.parseFloat(j.getString("shipping_charge"));
                                if (Float.parseFloat(j.getString("amount")) < discount) {
                                    Float final_discount = discount - Float.parseFloat(j.getString("amount"));
                                    // Utility.showToast(mContext,"final dis---"+ final_discount);
                                    ctv_discount.setText("-" + " " + String.valueOf(final_discount) + " " + getResources().getString(R.string.currncy_symbl));
                                }

                                ((CustomTextView) findViewById(R.id.ship_address)).setText(j.getString("shipping_address"));
                                ((CustomTextView) findViewById(R.id.subtotal)).setText(sb);
                                ((CustomTextView) findViewById(R.id.ship_charges)).setText(sch);
                                ((CustomTextView) findViewById(R.id.total_charges)).setText(amt);
                                JSONArray jsonArray = j.getJSONArray(Constants.OBJECT);
                                jlist.clear();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    jlist.add(jsonArray.getJSONObject(i));
                                }
                                String Date[] = j.getString("datetime").split(" ");

                                String data = Date[0];

                                String Formate_date = Utility.ChangeDateFormat("yyyy-MM-dd", "dd MMM", data);
                                System.out.println("Formate_date=======>" + Formate_date);
                                SetAdapter();
                                if (j.getString("dispacth_date").equals("")) {
                                    setJobStatus(j.getString(Constants.DEL_STATUS), Formate_date);
                                } else {
                                    setJobStatus1(j.getString(Constants.DEL_STATUS), Formate_date);
                                }

                            } else {

                            }

                        } catch (JSONException h) {
                            h.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException g) {
            g.printStackTrace();
        }

    }

    public void GiveFeedbackDialog() {
        cancel_status = new Dialog(mContext, R.style.MyDialog);
        cancel_status.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cancel_status.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        cancel_status.setContentView(R.layout.dialog_cancel_order_status);
        cancel_status.setCancelable(true);
        cancel_status.show();

        cancel_status.findViewById(R.id.edtbox).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.edt_feedback) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });


        cancel_status.findViewById(R.id.submit_cancel_order).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (((CustomEditText) cancel_status.findViewById(R.id.edtbox)).getText().toString().equals("")) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_cmnt), false);
                } else if (Utility.isConnectingToInternet(mContext)) {
                    SendCancelFeedback(((CustomEditText) cancel_status.findViewById(R.id.edtbox)).getText().toString());
                } else {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
                }
            }
        });

        cancel_status.findViewById(R.id.dialog_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cancel_status != null && cancel_status.isShowing()) {
                    cancel_status.cancel();
                }
            }
        });
    }

    public void SetAdapter() {
        adapter = new PriceAdapter(mContext, R.layout.orderdetail_order_itemlist, jlist);
        address_list.setAdapter(adapter);
        Helper.getListViewSize(address_list);
        address_list.setExpanded(true);
    }

    private void setJobStatus(String status, String Date) {
        Status = status;
        switch (status) {
            case Constants.STATUS.ASSIGNED:
                ((ImageView) findViewById(R.id.img_assign)).setImageResource(R.drawable.ic_check);
                ((CustomTextView) findViewById(R.id.date_assign)).setText(Date);
                break;

            case Constants.STATUS.DISPATCH:

                ((ImageView) findViewById(R.id.img_assign)).setImageResource(R.drawable.ic_check);
                ((ImageView) findViewById(R.id.img_pickup)).setImageResource(R.drawable.ic_check);
                ((CustomTextView) findViewById(R.id.date_assign)).setText(Date);
                ((CustomTextView) findViewById(R.id.date_dispatch)).setText(Date);
                break;

            case Constants.STATUS.DELIVERED:
                ((ImageView) findViewById(R.id.img_assign)).setImageResource(R.drawable.ic_check);
                ((ImageView) findViewById(R.id.img_complete)).setImageResource(R.drawable.ic_check);
                ((ImageView) findViewById(R.id.img_pickup)).setImageResource(R.drawable.ic_check);
                ((CustomTextView) findViewById(R.id.date_assign)).setText(Date);
                ((CustomTextView) findViewById(R.id.date_dispatch)).setText(Date);
                ((CustomTextView) findViewById(R.id.date_deliver)).setText(Date);

                break;

            case Constants.STATUS.CANCEL:

                ((ImageView) findViewById(R.id.img_assign)).setImageResource(R.drawable.ic_check);
                ((ImageView) findViewById(R.id.img_complete)).setImageResource(R.drawable.ic_cross_grey);
                ((ImageView) findViewById(R.id.img_pickup)).setImageResource(R.drawable.ic_cross_ornge);
                ((CustomTextView) findViewById(R.id.date_assign)).setText(Date);
                ((CustomTextView) findViewById(R.id.date_dispatch)).setText(mContext.getResources().getString(R.string.cancel_order));

                break;


        }
    }

    private void setJobStatus1(String status, String Date) {
        Status = status;
        switch (status) {
            case Constants.STATUS.ASSIGNED:
                ((ImageView) findViewById(R.id.img_assign)).setImageResource(R.drawable.ic_check);
                break;

            case Constants.STATUS.DISPATCH:

                ((ImageView) findViewById(R.id.img_assign)).setImageResource(R.drawable.ic_check);
                ((ImageView) findViewById(R.id.img_pickup)).setImageResource(R.drawable.ic_check);
                ((CustomTextView) findViewById(R.id.date_assign)).setText(Date);
                ((CustomTextView) findViewById(R.id.date_dispatch)).setText(Date);

                break;

            case Constants.STATUS.DELIVERED:

                ((ImageView) findViewById(R.id.img_assign)).setImageResource(R.drawable.ic_check);
                ((ImageView) findViewById(R.id.img_complete)).setImageResource(R.drawable.ic_check);
                ((ImageView) findViewById(R.id.img_pickup)).setImageResource(R.drawable.ic_check);
                ((CustomTextView) findViewById(R.id.date_assign)).setText(Date);
                ((CustomTextView) findViewById(R.id.date_dispatch)).setText(Date);
                ((CustomTextView) findViewById(R.id.date_deliver)).setText(Date);
                break;

            case Constants.STATUS.CANCEL:

                ((ImageView) findViewById(R.id.img_assign)).setImageResource(R.drawable.ic_check);
                ((ImageView) findViewById(R.id.img_pickup)).setImageResource(R.drawable.ic_check);
                ((ImageView) findViewById(R.id.img_complete)).setImageResource(R.drawable.ic_cross_ornge);
                ((CustomTextView) findViewById(R.id.date_assign)).setText(Date);
                ((CustomTextView) findViewById(R.id.date_deliver)).setText(Date);
                ((CustomTextView) findViewById(R.id.date_dispatch)).setText(mContext.getResources().getString(R.string.cancel_order));

                break;
        }
    }

    public void ReOrderTask() {
        try {
            JSONObject j = new JSONObject();
            j.put(Constants.ACTION, Constants.REORDER);
            j.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
            j.put(Constants.ORDERID, Order_no);
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(mContext, j);

            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Ttoast.show(mContext, mContext.getResources().getString(R.string.server_fail), false);
                    } else {
                        try {
                            JSONObject j = new JSONObject(result);
                            if (j.getString("success").equals("1")) {
                                if (alertdia.isShowing()) {
                                    alertdia.dismiss();
                                }
                                Utility.setIntegerSharedPreference(mContext, Constants.COUNT, Integer.parseInt(j.getString("count")));
                                startActivity(new Intent(mContext, Activity_AddtoCart_List.class));
                                finish();
                            } else {
                                Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.server_fail), false);
                            }

                        } catch (JSONException h) {
                            h.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException g) {
            g.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    public void SendCancelFeedback(String _msg) {

/*http://www.infograins.in/INFO01/Pharm01/api/api.php?action=
CancelledOrderByUser&orderid=OD1495689830&userid=95&comment=Hi*/

        try {

            JSONObject jo = new JSONObject();
            jo.put(Constants.ACTION, Constants.CANCELORDER);
            jo.put(Constants.ORDERID, Order_no);
            jo.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
            jo.put("comment", _msg);
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Ttoast.show(mContext, mContext.getString(R.string.server_fail), false);
                    }
                    try {

                        JSONObject j = new JSONObject(result);
                        if (j.getString("success").equals("1")) {
                            Ttoast.show(mContext, mContext.getResources().getString(R.string.ordercancel_msg), false);
                            if (j.getString("status").equals("1") || j.getString("status").equals("2")) {
                                findViewById(R.id.ll_cancel_order).setVisibility(View.GONE);
                            }
                            if (cancel_status.isShowing()) {
                                cancel_status.dismiss();
                            }
                            GetOrderHistory();
                        } else {
                            if (cancel_status.isShowing()) {
                                cancel_status.dismiss();
                            }
                            Ttoast.show(mContext, j.getString("msg"), false);
                        }
                    } catch (JSONException je) {
                        je.printStackTrace();
                    }

                }
            });
            rt.execute();
        } catch (JSONException j) {

        }

    }

    public void AlertReorderask() {
        alertdia = new Dialog(mContext, R.style.MyDialog);
        alertdia.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertdia.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        alertdia.setContentView(R.layout.dialog_alert_msg);
        alertdia.setCancelable(true);
        alertdia.show();
        CustomTextView alert_txt;
        alert_txt = (CustomTextView) alertdia.findViewById(R.id.alert_txt);

        alert_txt.setText(mContext.getResources().getString(R.string.alertmsg));

        alertdia.findViewById(R.id.alert_btn_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ReOrderTask();
            }
        });

        alertdia.findViewById(R.id.dialog_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertdia != null && alertdia.isShowing()) {
                    alertdia.cancel();
                }
            }
        });

        alertdia.findViewById(R.id.alert_cancel_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertdia != null && alertdia.isShowing()) {
                    alertdia.cancel();
                }
            }
        });
    }

    /*----------------------------ADAPTER CLASS-------------------------------*/

    public class PriceAdapter extends BaseAdapter {

        Context mContext;
        ArrayList<JSONObject> jlist;
        int res;
        Dialog ratdialog;

        public PriceAdapter(Context context, int res, ArrayList<JSONObject> jobj) {
            this.mContext = context;
            this.res = res;
            this.jlist = jobj;
        }

        @Override
        public int getCount() {
            return jlist.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = li.inflate(res, null);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            JSONObject j = jlist.get(position);

            try {
                String pri = j.getString("price") + mContext.getResources().getString(R.string.currncy_symbl);
                viewHolder.my_order_prodctname.setText(j.getString("title"));
                viewHolder.order_quant.setText(j.getString("quantity"));
                viewHolder.order_price.setText(pri);
                viewHolder.odesc_orderid.setText(j.getString("order_number"));

                String date[] = j.getString("datetime").split(" ");
                String datestr = date[0];
                String Formate_date = Utility.ChangeDateFormat("yyyy-MM-dd", "dd MMM", datestr);
                System.out.println("AdapFormate_date=======>" + Formate_date);

                viewHolder.order_deliveron.setText(Formate_date);

                viewHolder.status.setText(j.getString("status"));

                Glide.with(mContext).load(j.getString("image")).
                        listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                findViewById(R.id.order_det_pb).setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                findViewById(R.id.order_det_pb).setVisibility(View.GONE);
                                return false;
                            }
                        }).
                        into(viewHolder.my_order_img);

                if (j.getString("order_status").equals("1")) {
                    viewHolder.status.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
                }
                if (j.getString("order_status").equals("1") && j.getString("rating_status").equals("0")) {
                    viewHolder.give_rate.setVisibility(View.VISIBLE);
                    viewHolder.rate_bar.setVisibility(View.GONE);
                } else {
                    viewHolder.give_rate.setVisibility(View.GONE);
                }

                float rate = Float.parseFloat(j.getString("product_rate"));
                if (rate >= 0 && rate < 0.5) {
                    viewHolder.star1_rat.setImageResource(R.drawable.empty_star_grey);
                    viewHolder.star2_rat.setImageResource(R.drawable.empty_star_grey);
                    viewHolder.star3_rat.setImageResource(R.drawable.empty_star_grey);
                    viewHolder.star4_rat.setImageResource(R.drawable.empty_star_grey);
                    viewHolder.star5_rat.setImageResource(R.drawable.empty_star_grey);

                } else if (rate >= 0.5 && rate < 1) {
                    viewHolder.star1_rat.setImageResource(R.drawable.star_half);
                    viewHolder.star2_rat.setImageResource(R.drawable.empty_star_grey);
                    viewHolder.star3_rat.setImageResource(R.drawable.empty_star_grey);
                    viewHolder.star4_rat.setImageResource(R.drawable.empty_star_grey);
                    viewHolder.star5_rat.setImageResource(R.drawable.empty_star_grey);

                } else if (rate >= 1 && rate < 1.5) {
                    viewHolder.star1_rat.setImageResource(R.drawable.star_green);
                    viewHolder.star2_rat.setImageResource(R.drawable.empty_star_grey);
                    viewHolder.star3_rat.setImageResource(R.drawable.empty_star_grey);
                    viewHolder.star4_rat.setImageResource(R.drawable.empty_star_grey);
                    viewHolder.star5_rat.setImageResource(R.drawable.empty_star_grey);
                } else if (rate >= 1.5 && rate < 2) {
                    viewHolder.star1_rat.setImageResource(R.drawable.star_green);
                    viewHolder.star2_rat.setImageResource(R.drawable.star_half);
                    viewHolder.star3_rat.setImageResource(R.drawable.empty_star_grey);
                    viewHolder.star4_rat.setImageResource(R.drawable.empty_star_grey);
                    viewHolder.star5_rat.setImageResource(R.drawable.empty_star_grey);
                } else if (rate >= 2 && rate < 2.5) {
                    viewHolder.star1_rat.setImageResource(R.drawable.star_green);
                    viewHolder.star2_rat.setImageResource(R.drawable.star_green);
                    viewHolder.star3_rat.setImageResource(R.drawable.empty_star_grey);
                    viewHolder.star4_rat.setImageResource(R.drawable.empty_star_grey);
                    viewHolder.star5_rat.setImageResource(R.drawable.empty_star_grey);
                } else if (rate >= 2.5 && rate < 3) {
                    viewHolder.star1_rat.setImageResource(R.drawable.star_green);
                    viewHolder.star2_rat.setImageResource(R.drawable.star_green);
                    viewHolder.star3_rat.setImageResource(R.drawable.star_half);
                    viewHolder.star4_rat.setImageResource(R.drawable.empty_star_grey);
                    viewHolder.star5_rat.setImageResource(R.drawable.empty_star_grey);
                } else if (rate >= 3 && rate < 3.5) {
                    viewHolder.star1_rat.setImageResource(R.drawable.star_green);
                    viewHolder.star2_rat.setImageResource(R.drawable.star_green);
                    viewHolder.star3_rat.setImageResource(R.drawable.star_green);
                    viewHolder.star4_rat.setImageResource(R.drawable.empty_star_grey);
                    viewHolder.star5_rat.setImageResource(R.drawable.empty_star_grey);
                } else if (rate >= 3.5 && rate < 4) {
                    viewHolder.star1_rat.setImageResource(R.drawable.star_green);
                    viewHolder.star2_rat.setImageResource(R.drawable.star_green);
                    viewHolder.star3_rat.setImageResource(R.drawable.star_green);
                    viewHolder.star4_rat.setImageResource(R.drawable.star_half);
                    viewHolder.star5_rat.setImageResource(R.drawable.empty_star_grey);
                } else if (rate >= 4 && rate < 4.5) {
                    viewHolder.star1_rat.setImageResource(R.drawable.star_green);
                    viewHolder.star2_rat.setImageResource(R.drawable.star_green);
                    viewHolder.star3_rat.setImageResource(R.drawable.star_green);
                    viewHolder.star4_rat.setImageResource(R.drawable.star_green);
                    viewHolder.star5_rat.setImageResource(R.drawable.empty_star_grey);
                } else if (rate >= 4.5 && rate < 5) {
                    viewHolder.star1_rat.setImageResource(R.drawable.star_green);
                    viewHolder.star2_rat.setImageResource(R.drawable.star_green);
                    viewHolder.star3_rat.setImageResource(R.drawable.star_green);
                    viewHolder.star4_rat.setImageResource(R.drawable.star_green);
                    viewHolder.star5_rat.setImageResource(R.drawable.star_half);
                } else if (rate == 5) {
                    viewHolder.star1_rat.setImageResource(R.drawable.star_green);
                    viewHolder.star2_rat.setImageResource(R.drawable.star_green);
                    viewHolder.star3_rat.setImageResource(R.drawable.star_green);
                    viewHolder.star4_rat.setImageResource(R.drawable.star_green);
                    viewHolder.star5_rat.setImageResource(R.drawable.star_green);
                }
                viewHolder.give_rate.setOnClickListener(new RatebtnClick(position));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return convertView;
        }

        public void GiveRattingDialog(int posi) {
            final int pos1 = posi;
            ratdialog = new Dialog(mContext, R.style.MyDialog);
            ratdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            ratdialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
            ratdialog.setContentView(R.layout.dialog_give_ratting);
            ratdialog.setCancelable(true);
            ratdialog.show();
            try {
                LayerDrawable stars = (LayerDrawable) ((RatingBar) ratdialog.findViewById(R.id.rate_bar)).getProgressDrawable();
                stars.getDrawable(2).setColorFilter(mContext.getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
                stars.getDrawable(0).setColorFilter(mContext.getResources().getColor(R.color.grey), PorterDuff.Mode.SRC_ATOP);
                stars.getDrawable(1).setColorFilter(mContext.getResources().getColor(R.color.grey), PorterDuff.Mode.SRC_ATOP);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                ((RatingBar) ratdialog.findViewById(R.id.rate_bar)).setRating(Float.parseFloat(jlist.get(pos1).getString("product_rate")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            ratdialog.findViewById(R.id.submit_rate).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    float rat = ((RatingBar) ratdialog.findViewById(R.id.rate_bar)).getRating();
                    if (rat > 0) {
                        if (ratdialog != null && ratdialog.isShowing()) {
                            ratdialog.cancel();
                        }
                        try {
                            RateProduct("" + rat, jlist.get(pos1).getString("product_id"), jlist.get(pos1).getString("order_number"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.rat_rev), false);
                    }

                }
            });

            ratdialog.findViewById(R.id.dialog_close).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ratdialog != null && ratdialog.isShowing()) {
                        ratdialog.cancel();
                    }
                }
            });
        }

        public void RateProduct(String rate, String pro_id, String oid) {
        /*
http://infograins.in/INFO01/Pharm01/api/api.php?action=
OrderProductRating&userid=149&orderid=6565641&comment=jii&rating=4*/
            try {
                JSONObject j = new JSONObject();
                j.put(Constants.ACTION, Constants.PRODUCTRATTING);
                j.put(Constants.PRODUCTID, pro_id);
                j.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
                j.put("orderid", oid);
                j.put("rating", rate);
                Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));

                rt = new ResponseTask(mContext, j);
                rt.setListener(new ResponseListener() {
                    @Override
                    public void onGetPickSuccess(String result) {
                        Utility.HideDialog();
                        try {
                            if (result == null) {
                                Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.server_fail), false);
                            } else {
                                JSONObject object = new JSONObject(result);
                                if (object.getString("success").equals("1")) {
                                    jlist.clear();
                                    if (ratdialog != null && ratdialog.isShowing()) {
                                        ratdialog.cancel();
                                    }
                                    GetOrderHistory();
                                } else {
                                    Ttoast.ShowToast(mContext, object.getString("msg"), false);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
                rt.execute();
            } catch (JSONException j) {
                Utility.HideDialog();
                j.printStackTrace();
            }

        }

        public class RatebtnClick implements View.OnClickListener {
            int pos;

            public RatebtnClick(int pos) {
                this.pos = pos;
            }

            @Override
            public void onClick(View v) {
                GiveRattingDialog(pos);
            }
        }

        class ViewHolder {

            CustomTextView my_order_prodctname, order_price, order_quant, order_deliveron,
                    odesc_orderid, give_rate, status;
            ImageView my_order_img, star1_rat, star2_rat, star3_rat, star4_rat, star5_rat;
            LinearLayout rate_bar;

            public ViewHolder(View base) {

                my_order_prodctname = (CustomTextView) base.findViewById(R.id.my_order_prodctname);
                order_price = (CustomTextView) base.findViewById(R.id.order_price);
                my_order_img = (ImageView) base.findViewById(R.id.my_order_img);
                order_quant = (CustomTextView) base.findViewById(R.id.order_quant);
                order_deliveron = (CustomTextView) base.findViewById(R.id.order_deliveron);
                odesc_orderid = (CustomTextView) base.findViewById(R.id.odesc_orderid);
                give_rate = (CustomTextView) base.findViewById(R.id.give_rate);
                status = (CustomTextView) base.findViewById(R.id.status);
                star1_rat = (ImageView) base.findViewById(R.id.star1_rat);
                star2_rat = (ImageView) base.findViewById(R.id.star2_rat);
                star3_rat = (ImageView) base.findViewById(R.id.star3_rat);
                star4_rat = (ImageView) base.findViewById(R.id.star4_rat);
                star5_rat = (ImageView) base.findViewById(R.id.star5_rat);
                rate_bar = (LinearLayout) base.findViewById(R.id.rate_bar);
            }
        }
    }
}


