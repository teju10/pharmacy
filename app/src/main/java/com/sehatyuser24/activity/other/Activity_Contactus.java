package com.sehatyuser24.activity.other;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import com.sehatyuser24.R;
import com.sehatyuser24.customwidget.CustomEditText;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by and-04 on 6/7/17.
 */

public class Activity_Contactus extends AppCompatActivity {

    Context mContext;
    ResponseTask rt;
    CustomEditText contact_uname, login_email, contact_us_message;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Utility.ChangeLang(getApplicationContext(),Utility.getIngerSharedPreferences(getApplicationContext(),Constants.LANG));

        setContentView(R.layout.activity_contactus);

        mContext = this;

        setSupportActionBar((Toolbar) findViewById(R.id.h_toolbar));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((Toolbar) findViewById(R.id.h_toolbar)).setNavigationIcon(R.drawable.ic_navigate_before_white);
        ((CustomTextView) findViewById(R.id.app_name)).setText(mContext.getResources().getString(R.string.contact_us));

        FIND();
        Utility.hideKeyboard(mContext);

    }

    /*  public void PrintStar() {
          int space = 10;

          for (int i = 1; i <= 10; i++) {

              for (int j = 1; j <= space; j++) {
                  System.out.print(" ");
              }
              for (int k = 1; k <=i; k++) {
                  System.out.print("* ");
              }
              System.out.println();
              space--;
          }

      }
  */

    public void FIND() {
        contact_us_message = (CustomEditText) findViewById(R.id.contact_us_message);
        contact_uname = (CustomEditText) findViewById(R.id.contact_uname);
        login_email = (CustomEditText) findViewById(R.id.login_email);

        contact_us_message.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.contact_us_message) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        findViewById(R.id.submit_contactus).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (login_email.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_email), false);
                } else if (contact_uname.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_fullname), false);

                } else if (contact_us_message.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_comment), false);
                } else if (!Utility.isValidEmail((login_email).getText().toString())) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.corect_email), false);

                } else if (Utility.isConnectingToInternet(mContext)) {
                    SendContactus(login_email.getText().toString(), contact_uname.getText().toString(),
                            contact_us_message.getText().toString());
                } else {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void SendContactus(String email, String fname, String msg) {
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constants.ACTION, Constants.CONTACTUS);
            jo.put(Constants.EMAIL, email);
            jo.put(Constants.FNAME, fname);
            jo.put("message", msg);
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Ttoast.show(mContext, mContext.getResources().getString(R.string.server_fail), false);
                    } else {
                        try {
                            JSONObject jo = new JSONObject(result);
                            if (jo.getString("success").equals("1")) {
                                Ttoast.show(mContext, mContext.getResources().getString(R.string.answer_snd_feedback), false);
                                contact_us_message.setText("");
                                contact_uname.setText("");
                                login_email.setText("");
                            } else {
                                Ttoast.show(mContext, jo.getString("msg"), false);
                            }
                        } catch (JSONException j) {
                            j.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
