package com.sehatyuser24.activity.other;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.sehatyuser24.R;
import com.sehatyuser24.adapter.RelatedProductAdapter;
import com.sehatyuser24.customwidget.CustomEditText;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by Tejas on 17/5/17.
 */

public class Activity_PaymentDeduct extends AppCompatActivity implements View.OnClickListener {

    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
    private static final String CONFIG_CLIENT_ID = "AaAzMM6rSGjzlCbrfDgf_HtsRl5euvX6tVrKNbFvlLz7yDj8NyLZ7ax_XAv1jjGZTlJwPg48FC7y-vgB";
    private static final int PAYPAL_REQUEST_CODE = 1;
    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID);

    public String TAG = Activity_PaymentDeduct.class.getSimpleName();
    Context mContext;
    Bundle bundle;
    ResponseTask responseTask;
    String ship_charge = "", amount = "", Prsc_id = "";
    GridView related_pro_list;
    ArrayList<JSONObject> relatedarray = new ArrayList<>();
    RelatedProductAdapter adap;
    String t_dis = "", after_dis = "", t_amt = "", cou_code = "", cou_id = "", Subtotal = "";
    double conversion_amt = 0.0;
    CustomTextView total_amt;

    boolean PROMOCODE = false;
    double tempamt = 0.0;

    @Override

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utility.ChangeLang(getApplicationContext(), Utility.getIngerSharedPreferences(getApplicationContext(), Constants.LANG));

        setContentView(R.layout.activity_payment_deduct);


        mContext = this;
        bundle = new Bundle();
        bundle = getIntent().getExtras();

        Utility.hideKeyboard(mContext);
        find();

        init();

        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);
        Utility.hideKeyboard(mContext);
    }

    public void find() {

        setSupportActionBar((Toolbar) findViewById(R.id.h_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        ((Toolbar) findViewById(R.id.h_toolbar)).setNavigationIcon(R.drawable.ic_navigate_before_white);

        ((CustomTextView) findViewById(R.id.app_name)).setText(mContext.getResources().getString(R.string.check_out));
        total_amt = (CustomTextView) findViewById(R.id.total_amt);
        related_pro_list = (GridView) findViewById(R.id.related_pro_list);
    }

    public void init() {
        try {
            tempamt = Double.valueOf(bundle.getString(Constants.AMOUNT));
            ship_charge = bundle.getString(Constants.SHIPPING_CHARGES);
            amount = bundle.getString(Constants.AMOUNT);
            Prsc_id = bundle.getString(Constants.PRECRIPTIONID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        findViewById(R.id.pay_paypal_btn).setOnClickListener(this);
        findViewById(R.id.pay_credit_card).setOnClickListener(this);
        findViewById(R.id.pay_cod).setOnClickListener(this);
        findViewById(R.id.check_promocode).setOnClickListener(this);

        ((CustomTextView) findViewById(R.id.total_payment)).setText(amount + " " + mContext.getResources().getString(R.string.currncy_symbl));

        total_amt.setText(amount + " " + mContext.getResources().getString(R.string.currncy_symbl));

       /* ((LinearLayout)findViewById(R.id.frst_lay)).setVisibility(View.VISIBLE);
        ((LinearLayout)findViewById(R.id.secnd_lay)).setVisibility(View.GONE);*/
        if (PROMOCODE) {
            ((CustomEditText) findViewById(R.id.promo_code)).setClickable(false);
            ((CustomEditText) findViewById(R.id.promo_code)).setFocusable(false);
        } else {

        }

        System.out.println("SUBTOTAL========>" + Subtotal);

    }

    public void SetAdapter() {
        adap = new RelatedProductAdapter(mContext, relatedarray);
        related_pro_list.setAdapter(adap);
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void PayPayment() {
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(mContext, PaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, PAYPAL_REQUEST_CODE);
    }

    private PayPalPayment getThingToBuy(String paymentIntent) {
        if (PROMOCODE) {
            return new PayPalPayment(new BigDecimal(conversion_amt), "USD", mContext.getResources().getString(R.string.payment), paymentIntent);
        } else {
            return new PayPalPayment(new BigDecimal(conversion_amt), "USD", mContext.getResources().getString(R.string.payment), paymentIntent);
        }
//        return new PayPalPayment(new BigDecimal("50"), "USD", mContext.getResources().getString(R.string.payment), paymentIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PAYPAL_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.e(TAG, confirm.toJSONObject().toString(4));
                        Log.e(TAG, confirm.getPayment().toJSONObject().toString(4));
                        stopService(new Intent(this, PayPalService.class));
                        System.out.println("TRANSACTIONID " + confirm.toJSONObject().getJSONObject("response").getString("id"));

                        if (PROMOCODE) {
                            if (Utility.getIngerSharedPreferences(mContext, Constants.PAYMENT_BTN) == 0) {
                                PaymentConfirmation(Constants.PAYMENT_CHECKOUT, amount,
                                        confirm.toJSONObject().getJSONObject("response").getString("id"),
                                        "", "paypal", cou_code, after_dis, cou_id);
                            } else if (Utility.getIngerSharedPreferences(mContext, Constants.PAYMENT_BTN) == 1) {
                                PaymentConfirmation(Constants.PRSCORDERPAYMENT, amount,
                                        confirm.toJSONObject().getJSONObject("response").getString("id"),
                                        Prsc_id, "paypal", cou_code, after_dis, cou_id);
                            }
                        } else {
                            if (Utility.getIngerSharedPreferences(mContext, Constants.PAYMENT_BTN) == 0) {
                                PaymentConfirmation(Constants.PAYMENT_CHECKOUT, amount, confirm.toJSONObject().getJSONObject("response").getString("id"),
                                        "", "paypal", "", "", "");
                            } else if (Utility.getIngerSharedPreferences(mContext, Constants.PAYMENT_BTN) == 1) {
                                PaymentConfirmation(Constants.PRSCORDERPAYMENT, amount, confirm.toJSONObject().getJSONObject("response").getString("id"),
                                        Prsc_id, "paypal", "", "", "");
                            }
                        }


                    } catch (JSONException e) {
                        Log.e(TAG, "an extremely unlikely failure occurred:", e);
                    }
                    //  PaymentConfirmation();
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.e(TAG, "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.e(TAG, "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }

    public void PaymentConfirmation(String action, String amt, String transid,
                                    String p_id, String pay_type, String c_code, String t_dis, String cou_id) {

        //infograins.com.208-91-199-7.md-plesk-web2.webhostbox.net/INFO01/pharmacy/api/api.php?action=
        // OrderPayment&userid=95&amount=373328&shipping_charge=0

        /*https://www.infograins.in/INFO01/Pharm01/api/api.php?action=prescriptionCreateOrderPayment&
        userid=165&amount=150&shipping_charge=50&transaction_id=TXT201709161048&prescrition_id=53&
        payment_method=cod*/

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, action);
            jsonObject.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
            jsonObject.put(Constants.AMOUNT, amt);
            jsonObject.put(Constants.SHIPPING_CHARGES, ship_charge);
            jsonObject.put(Constants.TRANSACTION_ID, transid);
            jsonObject.put(Constants.PRECRIPTIONID, p_id);
            jsonObject.put(Constants.PAYMENT_METHOD, pay_type);
            jsonObject.put(Constants.COUPON_CODE, c_code);
            jsonObject.put(Constants.TOTAL_DIS, t_dis);
            jsonObject.put(Constants.COUPON_ID, cou_id);
            Utility.ShowLoading(mContext, getResources().getString(R.string.loading_msg));
            responseTask = new ResponseTask(mContext, jsonObject);
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    System.out.println("Server result ====> " + result);
                    if (result == null) {
                        Ttoast.show(mContext, mContext.getResources().getString(R.string.payment_incomplete), false);

                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString("success").equals("1")) {
                                Ttoast.show(mContext, "payment successfully done", false);
                                Utility.setIntegerSharedPreference(mContext, Constants.COUNT, Integer.parseInt(jobj.getString("count")));

                               /* ((LinearLayout)findViewById(R.id.frst_lay)).setVisibility(View.GONE);
                                ((LinearLayout)findViewById(R.id.secnd_lay)).setVisibility(View.VISIBLE);*/

                              /*  startActivity(new Intent(mContext, Activity_MyOrderDetail.class).putExtra(Constants.ORDERNO, jobj.getString("orderid")));
                                finish();
                                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);*/

                            } else {
                                Ttoast.show(mContext, jobj.getString(Constants.SERVER_MSG), false);
                            }
                        } catch (JSONException e2) {
                            Ttoast.show(mContext, mContext.getResources().getString(R.string.server_fail), false);
                        }
                    }
                }

            });
            responseTask.execute();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pay_paypal_btn:
//                PayPayment();
                MoneyConversion();
                break;

            case R.id.pay_credit_card:
                Ttoast.show(mContext, mContext.getResources().getString(R.string.c_soon), false);
                break;

            case R.id.pay_cod:
                if (PROMOCODE) {
                    if (Utility.getIngerSharedPreferences(mContext, Constants.PAYMENT_BTN) == 1) {
                        PaymentConfirmation(Constants.PRSCORDERPAYMENT, after_dis, "0", Prsc_id, "cod", cou_code, after_dis, cou_id);
                    } else {
                        PaymentConfirmation(Constants.PAYMENT_CHECKOUT, after_dis, "0", "", "cod", cou_code, after_dis, cou_id);
                    }
                } else {
                    if (Utility.getIngerSharedPreferences(mContext, Constants.PAYMENT_BTN) == 1) {
                        PaymentConfirmation(Constants.PRSCORDERPAYMENT, amount, "0", Prsc_id, "cod", "", "", "");
                    } else {
                        PaymentConfirmation(Constants.PAYMENT_CHECKOUT, amount, "0", "", "cod", "", "", "");
                    }
                }
                break;

            case R.id.check_promocode:
                if (((CustomEditText) findViewById(R.id.promo_code)).getText().toString().equals("")) {
                } else {
                    PromoCodeCheck(((CustomEditText) findViewById(R.id.promo_code)).getText().toString(), tempamt);
                }
                break;
        }
    }

    public void PromoCodeCheck(String _coupon, double _amount) {
        try {
            JSONObject j = new JSONObject();
            j.put(Constants.ACTION, Constants.APPLYCOUPEN);
            j.put(Constants.COUPON_CODE, _coupon);
            j.put(Constants.AMOUNT, _amount);
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));

            responseTask = new ResponseTask(mContext, j);
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.server_fail), false);
                    } else {
                        try {
                            JSONObject j = new JSONObject(result);
                            if (j.getString("success").equals("1")) {
                                Utility.hideKeyboard(mContext);
                                PROMOCODE = true;
                                t_dis = j.getString("totol_discount");
                                t_amt = j.getString("total_amount");
                                cou_code = j.getString("coupon_code");
                                cou_id = j.getString("coupon_id");

                                after_dis = j.getString("afetr_discount_amount");

                                total_amt.setText(j.getString("afetr_discount_amount") + " " + mContext.getResources().getString(R.string.currncy_symbl));

                                ((CustomEditText) findViewById(R.id.promo_code)).setClickable(false);
                                ((CustomEditText) findViewById(R.id.promo_code)).setFocusable(false);
                                ((CustomTextView) findViewById(R.id.check_promocode)).setText(mContext.getResources().getString(R.string.applied));

                            } else {
                                Ttoast.ShowToast(mContext, j.getString("msg"), false);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }
            });
            responseTask.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void MoneyConversion() {
        try {
            JSONObject j = new JSONObject();
            j.put(Constants.ACTION, "ConvertCurrecnyEGPTOUSD");

            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));

            responseTask = new ResponseTask(mContext, j);
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.server_fail), false);
                    } else {
                        try {
                            JSONObject j = new JSONObject(result);
                            if (j.getString("success").equals("1")) {
                                JSONObject jo = j.getJSONObject(Constants.OBJECT);
                                if (!amount.equals("")) {
                                    conversion_amt = jo.getDouble("current_rate") * Double.valueOf(amount);
                                } else if (!after_dis.equals("")) {
                                    conversion_amt = jo.getDouble("current_rate") * Double.valueOf(after_dis);
                                }
                                PayPayment();
                            } else {
                                Ttoast.ShowToast(mContext, j.getString("msg"), false);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }
            });
            responseTask.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*========>FALTUUUUUUUU FOR RELETED LIST<========*/

    public void ViewReletedListTask() {
        /*http://infograins.in/INFO01/Pharm01/api/api.php?action=
        RecentViewProduct&deviceid=111111&userid=155*/

        try {
            JSONObject j = new JSONObject();
            j.put(Constants.ACTION, Constants.RECENTVIEWPRODUCT);
            j.put(Constants.DEVICEID, Utility.getSharedPreferences(mContext, Constants.FCMID));
            j.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
            responseTask = new ResponseTask(mContext, j);
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Ttoast.show(mContext, mContext.getResources().getString(R.string.server_fail), false);
                    } else {
                        try {
                            JSONObject j = new JSONObject(result);
                            if (j.getString("success").equals("1")) {

                                JSONArray jsonArray = j.getJSONObject(Constants.OBJECT).getJSONArray("recent_view");

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    relatedarray.add(jsonArray.getJSONObject(i));
                                }
                                SetAdapter();
                            } else {
                                Ttoast.show(mContext, j.getString("msg"), false);
                            }

                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    }
                }
            });
            responseTask.execute();

        } catch (JSONException je) {
            je.printStackTrace();
        }
    }
}
