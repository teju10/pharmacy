package com.sehatyuser24.activity.other;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SimpleAdapter;
import android.widget.Spinner;

import com.bumptech.glide.Glide;
import com.sehatyuser24.ImageSelection.ImageSelection;
import com.sehatyuser24.R;
import com.sehatyuser24.activity.registration.MainActivity;
import com.sehatyuser24.customwidget.CustomButton;
import com.sehatyuser24.customwidget.CustomEditText;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.utility.Constant_Urls;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.MultipartUtility;
import com.sehatyuser24.utility.Util;
import com.sehatyuser24.utility.Utility;
import com.soundcloud.android.crop.Crop;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by Tejas on 1/6/17.
 */

public class Activity_AddFamily_Mem_Account extends AppCompatActivity implements View.OnClickListener {

    Context mContext;
    Bundle b;
    JSONObject j;
    String jobj, gen = "";
    CustomEditText f_mem_edt_name, f_mem_edt_age,
            f_mem_edt_relation, f_mem_edt_fine, f_mem_edt_treat, f_mem_edt_doctr;
    Dialog sdialog;
    ImageSelection imageSelection;
    String profile_image = "", MemberId = "";
    Spinner f_mem_spnr_sex;
    String[] Gender;
    ArrayList<HashMap<String, String>> gender_spinner;
    int age;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utility.ChangeLang(getApplicationContext(),  Utility.getIngerSharedPreferences(getApplicationContext(), Constants.LANG));

        setContentView(R.layout.activity_addfamily_mem_account);
        mContext = this;

        setSupportActionBar((Toolbar) findViewById(R.id.h_toolbar));

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((Toolbar) findViewById(R.id.h_toolbar)).setNavigationIcon(R.drawable.ic_navigate_before_white);
        ((CustomTextView) findViewById(R.id.app_name)).setText(mContext.getResources().getString(R.string.my_family_member));

        Gender = new String[]{mContext.getResources().getString(R.string.select_gender),
                mContext.getResources().getString(R.string.male), mContext.getResources().getString(R.string.female)};

        Find();
        Listner();
        b = new Bundle();
        try {
            if (!b.equals("")) {
                b = getIntent().getExtras();
                jobj = b.getString(Constants.FAMILYMEMBERSINGLEVIEW);
                j = new JSONObject(jobj);
                System.out.println("JSONOBJ------>>>>> " + j);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Utility.hideKeyboard(mContext);

        SetText();

        Utility.hideKeyboard(mContext);
    }

    public void Find() {
        f_mem_edt_name = (CustomEditText) findViewById(R.id.f_mem_edt_name);
        f_mem_spnr_sex = (Spinner) findViewById(R.id.f_mem_edt_sex);
        f_mem_edt_age = (CustomEditText) findViewById(R.id.f_mem_edt_age);
        f_mem_edt_relation = (CustomEditText) findViewById(R.id.f_mem_edt_relation);
        f_mem_edt_fine = (CustomEditText) findViewById(R.id.f_mem_edt_fine);
        f_mem_edt_treat = (CustomEditText) findViewById(R.id.f_mem_edt_treat);
        f_mem_edt_doctr = (CustomEditText) findViewById(R.id.f_mem_edt_doctr);

        if (Utility.getSharedPreferences(mContext, Constants.LANG_PREF).equals("en")) {
            f_mem_spnr_sex.setBackground(mContext.getResources().getDrawable(R.drawable.select_spinner_trans));
        } else if (Utility.getSharedPreferences(mContext, Constants.LANG_PREF).equals("ar")) {
            f_mem_spnr_sex.setBackground(mContext.getResources().getDrawable(R.drawable.select_spinner_trans_ar));

        }
        findViewById(R.id.add_pic).setOnClickListener(this);
        findViewById(R.id.update_mem_btn).setOnClickListener(this);
        findViewById(R.id.edit_btn).setOnClickListener(this);
    }

    public void SetText() {
        if (Utility.getIngerSharedPreferences(mContext, Constants.SP_FAMILYMEM) == 1) {
            findViewById(R.id.view_ll).setVisibility(View.GONE);
            findViewById(R.id.edit_ll).setVisibility(View.VISIBLE);

            ((CustomButton) findViewById(R.id.update_mem_btn)).setText
                    (mContext.getResources().getString(R.string.add_member));
        } else if (Utility.getIngerSharedPreferences(mContext, Constants.SP_FAMILYMEM) == 3) {
            findViewById(R.id.view_ll).setVisibility(View.VISIBLE);
            findViewById(R.id.edit_ll).setVisibility(View.GONE);
            ((LinearLayout) findViewById(R.id.edt_ntn_lay)).setVisibility(View.VISIBLE);
            try {
                ((CustomTextView) findViewById(R.id.f_mem_txt_name)).setText(j.getString("fname"));
                if (j.getString("sex").equals("0")) {
                    ((CustomTextView) findViewById(R.id.f_mem_txt_sex)).setText(mContext.getResources().getString(R.string.male));
                } else {
                    ((CustomTextView) findViewById(R.id.f_mem_txt_sex)).setText(mContext.getResources().getString(R.string.female));
                }
                ((CustomTextView) findViewById(R.id.f_mem_txt_age)).setText(j.getString("age"));
                ((CustomTextView) findViewById(R.id.f_mem_txt_relation)).setText(j.getString("relation"));
                ((CustomTextView) findViewById(R.id.f_mem_txt_health_status)).setText(j.getString("health_status"));
                ((CustomTextView) findViewById(R.id.f_mem_txt_treat)).setText(j.getString("treatment"));
                ((CustomTextView) findViewById(R.id.f_mem_txt_doct)).setText(j.getString("doctor"));
                Glide.with(mContext).load(j.getString("image")).into((ImageView) findViewById(R.id.f_mem_txt_img));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (Utility.getIngerSharedPreferences(mContext, Constants.SP_FAMILYMEM) == 2) {
            ((LinearLayout) findViewById(R.id.edt_ntn_lay)).setVisibility(View.GONE);
            findViewById(R.id.view_ll).setVisibility(View.GONE);
            findViewById(R.id.edit_ll).setVisibility(View.VISIBLE);
            try {
                MemberId = j.getString("member_id");

                (f_mem_edt_name).setText(j.getString("fname"));
                (f_mem_edt_age).setText(j.getString("age"));
                (f_mem_edt_relation).setText(j.getString("relation"));
                (f_mem_edt_fine).setText(j.getString("health_status"));
                (f_mem_edt_treat).setText(j.getString("treatment"));
                (f_mem_edt_doctr).setText(j.getString("doctor"));
                Glide.with(mContext).load(j.getString("image")).into((ImageView) findViewById(R.id.f_mem_txt_img));
                if (j.getString("sex").equals("0")) {
                    f_mem_spnr_sex.setSelection(1);
                    gen = "0";
                } else if (j.getString("sex").equals("1")) {
                    f_mem_spnr_sex.setSelection(2);
                    gen = "1";
                } else {
                    f_mem_spnr_sex.setSelection(0);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public void Listner() {
        f_mem_edt_treat.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.f_mem_edt_treat) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        getcurrency_spinner();
    }

    public void getcurrency_spinner() {
        gender_spinner = new ArrayList<>();
        for (int i = 0; i < Gender.length; i++) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("Type", Gender[i]);
            gender_spinner.add(hm);
        }
        int[] to = new int[]{R.id.card};
        String[] from = new String[]{"Type"};
        // Creating a SimpleAdapter for the Spinner
        SimpleAdapter adapter = new SimpleAdapter(mContext, gender_spinner, R.layout.text_single_add, from, to);
        f_mem_spnr_sex.setAdapter(adapter);

        f_mem_spnr_sex.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
//                    Ttoast.show(mContext, mContext.getResources().getString(R.string.select_gender), false);
                } else if (position == 1) {
                    gen = "0";
                } else if (position == 2) {
                    gen = "1";
                }
                System.out.println("GENDERRRR  " + gen);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                gen = "";
            }
        });
    }

    public void AddMEMBERMETHOD() {

        age = Integer.valueOf(f_mem_edt_age.getText().toString());
        if (f_mem_edt_name.getText().toString().equals("")) {
            Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_family_mem_name), false);
        } else if (f_mem_edt_age.getText().toString().equals("")) {
            Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_family_mem_age), false);
        } else if (age > 110) {
            Ttoast.show(mContext, mContext.getResources().getString(R.string.age_vali), false);
        } else if (gen.equals("")) {
            Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_gender), false);
        } else if (f_mem_edt_relation.getText().toString().equals("")) {
            Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_family_mem_relation), false);
        } else if (f_mem_edt_fine.getText().toString().equals("")) {
            Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_family_mem_health_status), false);
        } else if (f_mem_edt_treat.getText().toString().equals("")) {
            Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_family_mem_treatment), false);
        } else if (f_mem_edt_doctr.getText().toString().equals("")) {
            Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_family_mem_doct), false);

        } else if (Utility.getIngerSharedPreferences(mContext, Constants.SP_FAMILYMEM) == 1) {


            new AddFamilyMemberTask().execute("addFamilyMember", f_mem_edt_name.getText().toString(),
                    gen, f_mem_edt_age.getText().toString(),
                    f_mem_edt_relation.getText().toString(), f_mem_edt_fine.getText().toString(),
                    f_mem_edt_treat.getText().toString(), f_mem_edt_doctr.getText().toString());
        } else if (Utility.getIngerSharedPreferences(mContext, Constants.SP_FAMILYMEM) == 2) {
            ((CustomButton) findViewById(R.id.update_mem_btn)).setText
                    (mContext.getResources().getString(R.string.update));
            new AddFamilyMemberTask().execute("UpdateFamilyMember", f_mem_edt_name.getText().toString(),
                    gen, f_mem_edt_age.getText().toString(),
                    f_mem_edt_relation.getText().toString(), f_mem_edt_fine.getText().toString(),
                    f_mem_edt_treat.getText().toString(),
                    f_mem_edt_doctr.getText().toString(), MemberId);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_pic:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(this, CAMERA) == PackageManager.PERMISSION_GRANTED
                            && ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        SelectImage();
                    } else {
                        requestCameraPermission();
                    }
                } else {
                    SelectImage();
                }
                break;

            case R.id.update_mem_btn:
                AddMEMBERMETHOD();
                break;

            case R.id.edit_btn:
                findViewById(R.id.view_ll).setVisibility(View.GONE);
                findViewById(R.id.edit_ll).setVisibility(View.VISIBLE);
                Utility.setIntegerSharedPreference(mContext, Constants.SP_FAMILYMEM, 2);
                try {
                    MemberId = j.getString("member_id");

                    (f_mem_edt_name).setText(j.getString("fname"));
                    (f_mem_edt_age).setText(j.getString("age"));
                    (f_mem_edt_relation).setText(j.getString("relation"));
                    (f_mem_edt_fine).setText(j.getString("health_status"));
                    (f_mem_edt_treat).setText(j.getString("treatment"));
                    (f_mem_edt_doctr).setText(j.getString("doctor"));
                    Glide.with(mContext).load(j.getString("image")).into((ImageView) findViewById(R.id.f_mem_txt_img));
                    if (j.getString("sex").equals("0")) {
                        f_mem_spnr_sex.setSelection(1);
                        gen = "0";
                    } else if (j.getString("sex").equals("1")) {
                        f_mem_spnr_sex.setSelection(2);
                        gen = "1";
                    } else {
                        f_mem_spnr_sex.setSelection(0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    public class AddFamilyMemberTask extends AsyncTask<String, String, String> {
        /*http://infograins.com.208-91-199-7.md-plesk-web2.webhostbox.net/INFO01/pharmacy/api/api.php?action=
        addFamilyMember&userid=138&fname=rahul%20saxena&sex=male&age=12&relation=brother&
        health_status=Fine&treatment=none&doctor=Mehta*/
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                MultipartUtility multipart = new MultipartUtility(mContext, "UTF-8");
                multipart.addFormField(Constants.ACTION, params[0]);
                multipart.addFormField(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
                multipart.addFormField(Constants.FNAME, params[1]);
                multipart.addFormField(Constants.SEX, params[2]);
                multipart.addFormField(Constants.AGE, params[3]);
                multipart.addFormField(Constants.RELATION, params[4]);
                multipart.addFormField(Constants.HEALTH_STATUS, params[5]);
                multipart.addFormField(Constants.TREATEMENT, params[6]);
                multipart.addFormField(Constants.DOCTORNAME, params[7]);
                if (params[0].equals("UpdateFamilyMember")) {
                    multipart.addFormField(Constants.MEMBER_ID, params[8]);
                }
                if (profile_image.equals("")) {
                    multipart.addFormField(Constants.PROFILE_IMAGE, "");
                } else {
                    multipart.addFilePart(Constants.PROFILE_IMAGE, profile_image);
                }
                return multipart.finish();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Utility.HideDialog();
            if (result == null) {
                Utility.ShowToastMessage(mContext, getResources().getString(R.string.server_fail));
            } else {
                try {
                    JSONObject json = new JSONObject(result);
                    Log.e("result result", "result result ============= " + json);
                    if (json.getString("success").equals("1")) {
                        Ttoast.ShowToast(mContext, json.getString("msg"), false);
                        profile_image = "";
                        JSONObject jo = json.getJSONObject(Constants.OBJECT);
                        Utility.setSharedPreference(mContext, Constants.PROFILE_IMAGE, jo.getString("profile_image"));
                        Utility.setSharedPreference(mContext, Constants.FNAME, jo.getString("username"));
                        ((MainActivity) mContext).SetDrawerData();
                    }
                } catch (Exception e) {

                }
            }
            super.onPostExecute(result);
        }
    }

    private void requestCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, CAMERA)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{CAMERA, WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE}, 1);
    }

    public void setListener(ImageSelection listener) {
        imageSelection = listener;
    }

    public void SelectImage() {
        sdialog = new Dialog(mContext, R.style.MyDialog);
        sdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        sdialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        sdialog.setContentView(R.layout.dialog_imgeselect);
        sdialog.setCancelable(true);
        sdialog.show();
//        ((CustomTextView) sdialog.findViewById(R.id.dialog_pack_name)).setText(getResources().getString(R.string.selectpic));
        sdialog.findViewById(R.id.dialog_close).setOnClickListener(new MyDialogClick());
        sdialog.findViewById(R.id.camera).setOnClickListener(new MyDialogClick());
        sdialog.findViewById(R.id.gallery).setOnClickListener(new MyDialogClick());
        sdialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dialog.dismiss();
                    getFragmentManager().popBackStackImmediate();
                }
                return true;
            }
        });
    }

    public class MyDialogClick implements View.OnClickListener {

        public MyDialogClick() {

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.dialog_close:
                    if (sdialog.isShowing() && sdialog != null) {
                        sdialog.dismiss();
                    }
                    break;
                case R.id.camera:
                    if (sdialog.isShowing() && sdialog != null) {
                        sdialog.dismiss();
                    }
//                    Ttoast.ShowToast(mContext,mContext.getResources().getString(R.string.c_soon);
                    Uri outputFileUri = getCaptureImageOutputUri();
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                    startActivityForResult(intent, 1);
                    break;
                case R.id.gallery:
                    if (sdialog.isShowing() && sdialog != null) {
                        sdialog.dismiss();
                    }
                    Intent intent1 = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent1, 2);
                    break;
            }
        }
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            File getImage = mContext.getExternalCacheDir();
            if (getImage != null) {
                outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
            }
        } else {
            File getImage = mContext.getExternalCacheDir();
            if (getImage != null) {
                outputFileUri = FileProvider.getUriForFile(mContext, com.sehatyuser24.BuildConfig.APPLICATION_ID + ".provider",
                        new File(getImage.getPath(), "pickImageResult.jpeg"));
            }
        }
        return outputFileUri;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // new functions
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 1) {
                Uri imageUri = getPickImageResultUri(data);
                beginCrop(imageUri);
            } else if (requestCode == 2) {
                Uri selectedImage = data.getData();
                beginCrop(selectedImage);
            } else if (requestCode == Crop.REQUEST_CROP) {
                handleCrop(resultCode, data);
            }
        }
    }

    private void beginCrop(Uri source) {
        File pro = new File(Utility.MakeDir(Constants.MYFOLDER, mContext), System.currentTimeMillis() + Constants.SEND);
        Uri destination1 = Uri.fromFile(pro);
        Crop.of(source, destination1).asSquare().withMaxSize(200, 200).start(this);
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == Activity.RESULT_OK) {
            File f = new File(Crop.getOutput(result).getPath());
            Log.e("profile image path", "profile image path ======= " + f);
            String croped_file = "" + Crop.getOutput(result).getPath();
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            Bitmap bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bmOptions);
            bitmap = Bitmap.createScaledBitmap(bitmap, 300, 300, true);
            bitmap = Utility.rotateImage(bitmap, f);
            profile_image = croped_file;
            Glide.with(mContext).load(profile_image).into((ImageView) findViewById(R.id.f_mem_txt_img));
        } else if (resultCode == Crop.RESULT_ERROR) {
            Utility.ShowToastMessage(mContext, Crop.getError(result).getMessage());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
