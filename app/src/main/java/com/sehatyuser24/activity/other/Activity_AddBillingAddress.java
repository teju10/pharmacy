package com.sehatyuser24.activity.other;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.SimpleAdapter;


import com.sehatyuser24.R;
import com.sehatyuser24.activity.registration.MainActivity;
import com.sehatyuser24.customwidget.CustomAutoCompleteTextView;
import com.sehatyuser24.customwidget.CustomEditText;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.serverinteraction.GetResponseTask;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.Constant_Urls;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.PlaceJSONParser;
import com.sehatyuser24.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Tejas on 12/5/17.
 */

public class Activity_AddBillingAddress extends AppCompatActivity {

    Context mContext;
    Bundle b;
    CustomTextView bill_address;
    CustomEditText bill_mobile, bill_city, edt_email;
    String pickupaddress = "";
    ResponseTask rt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utility.ChangeLang(getApplicationContext(),Utility.getIngerSharedPreferences(getApplicationContext(),Constants.LANG));
        setContentView(R.layout.activity_add_bill_address);
        mContext = this;
        b = new Bundle();
        Find();
        Listner();

        setSupportActionBar((Toolbar) findViewById(R.id.h_toolbar));

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((Toolbar) findViewById(R.id.h_toolbar)).setNavigationIcon(R.drawable.ic_navigate_before_white);
        ((CustomTextView) findViewById(R.id.app_name)).setText
                (mContext.getResources().getString(R.string.add_bill_address));

        Utility.hideKeyboard(mContext);
    }

    public void Find() {
        bill_address = (CustomTextView) findViewById(R.id.bill_address);
        bill_mobile = (CustomEditText) findViewById(R.id.bill_mobile);
        bill_city = (CustomEditText) findViewById(R.id.bill_city);
        edt_email = (CustomEditText) findViewById(R.id.edt_email);
    }

    public void Listner() {

        try {
            b = new Bundle();
            if (!b.equals("")) {
                String shipbundle;
                b = getIntent().getExtras();
                shipbundle = b.getString(Constants.BillingBundle);
                System.out.println("JSONOBJ " + shipbundle);
                JSONObject j = new JSONObject(shipbundle);
                ((CustomTextView) findViewById(R.id.bill_fname)).setText(j.getString("fname"));
                ((CustomTextView) findViewById(R.id.bill_lname)).setText(j.getString("lname"));
                KeyListener variable;
                variable = edt_email.getKeyListener();

                if (j.getString("email").equals("")) {
                    edt_email.setKeyListener(variable);
                } else {
                    edt_email.setKeyListener(null);
                }

                edt_email.setText(j.getString("email"));
                bill_address.setText(j.getString("address"));
                bill_city.setText(j.getString("city"));
                bill_mobile.setText(j.getString("mobile"));

            }
        } catch (Exception h) {
            h.printStackTrace();
        }
        SetClick();
    }

    public void SetClick() {

        bill_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, Activity_GetAddress.class);
                startActivityForResult(i, 5);
            }
        });


        findViewById(R.id.update_bill_address).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bill_mobile.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_mobile), false);
                } else if (bill_city.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_city), false);
                } else if (!Utility.isValidEmail((edt_email).getText().toString())) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.corect_email), false);
                } else if (edt_email.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_billing_email), false);
                } else if (Utility.isConnectingToInternet(mContext)) {
                    UpdateBillingDetailTask(bill_mobile.getText().toString(),
                            bill_city.getText().toString(), bill_address.getText().toString(),
                            edt_email.getText().toString());
                } else {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
                }
            }
        });

    }

    public void UpdateBillingDetailTask(String mo, String city, String strt_addr, String email) {

        //infograins.com.208-91-199-7.md-plesk-web2.webhostbox.net/INFO01/pharmacy/api/api.php?action=
        // UpdateBillingAddress&userid=95&mobile=99865745&city=indore&longitude=75.8550918&latitude=22.7184678
        try {
            JSONObject j = new JSONObject();
            j.put(Constants.ACTION, Constants.UPDATEBILLINGADDRESS);
            j.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
            j.put(Constants.MOBILE, mo);
            j.put(Constants.CITY, city);
            j.put(Constants.STREET, strt_addr);
            j.put(Constants.EMAIL, email);
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(mContext, j);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.server_fail), false);
                    } else {
                        try {
                            JSONObject k = new JSONObject(result);
                            if (k.getString("success").equals("1")) {
                                Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.billaddress_updated), false);

                                Utility.setSharedPreference(mContext, Constants.TOTAL_SAV, "1");

                                Utility.setSharedPreference(mContext, Constants.BillingCheck, "2");
                                /*Intent intent = new Intent();
                                intent.putExtra("PICKADDRESS", pickupaddress);
                                setResult(Activity.RESULT_OK, intent);
                                finish();*/

                            } else {
                                Ttoast.ShowToast(mContext, k.getString(Constants.SERVER_MSG), false);
                            }
                        } catch (JSONException j) {
                            j.printStackTrace();
                        }

                    }
                }
            });
            rt.execute();
        } catch (JSONException h) {
            h.printStackTrace();
        }

    }


    /*protected void getLatLng(final String address) {
        String input = "";
        try {
            input = "address=" + URLEncoder.encode(address, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        String uri = "http://maps.google.com/maps/api/geocode/json?" + input + "&sensor=false";
        System.out.println("url is     " + uri);
        GetResponseTask getResponseTask = new GetResponseTask(uri, mContext, "GOOGLE");
        getResponseTask.setListener(new ResponseListener() {
            @Override
            public void onGetPickSuccess(String result) {

                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(result);
                    lati = ((JSONArray) jsonObject.get("results")).getJSONObject(0).
                            getJSONObject("geometry").getJSONObject("location")
                            .getDouble("lat");
                    longi = ((JSONArray) jsonObject.get("results")).getJSONObject(0).
                            getJSONObject("geometry").getJSONObject("location")
                            .getDouble("lng");
//                    Utility.hideKeyboard(mContext);

                   *//* Intent intent = new Intent();
                    intent.putExtra("LAT", lat);
                    intent.putExtra("ADD", address);
                    intent.putExtra("LONG", lng);
                    setResult(Activity.RESULT_OK, intent);
                    finish();*//*
//                    overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Utility.ShowToastMessage(mContext, getString(R.string.add_not_found));
                }
            }
        });
        getResponseTask.execute();
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 5 && resultCode == Activity.RESULT_OK) {
            pickupaddress = data.getStringExtra("PICKADDRESS");
            bill_address.setText(pickupaddress);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
