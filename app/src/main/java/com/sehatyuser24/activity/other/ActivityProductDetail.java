package com.sehatyuser24.activity.other;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Paint;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.sehatyuser24.R;
import com.sehatyuser24.activity.registration.MainActivity;
import com.sehatyuser24.adapter.RelatedHorizontalListAdapter;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Util;
import com.sehatyuser24.utility.Utility;
import com.sehatyuser24.utility.ZoomageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by sandeep on 28/4/17.
 */

public class ActivityProductDetail extends AppCompatActivity implements View.OnClickListener {
    public static final String mBroadcastString = "mynotification";
    public static String isopenHome;
    Context mContext;
    ResponseTask rt;
    String ProductId = "", MaxQuantity = "", ImgPath = "", review = "";
    int Quantity = 0;
    Bundle b;
    RecyclerView related_pro_list;
    RelatedHorizontalListAdapter adap;
    ImageView star1_rat, star2_rat, star3_rat, star4_rat, star5_rat;
    ArrayList<JSONObject> relatedarray = new ArrayList<>();
    int count = 0;
    Dialog dialog_imagezoom;
    CustomTextView pro_det_formlation, pro_det_manufacture, pro_det_package_type, pro_det_size;
    LinearLayout btn_add2cart;
    CustomTextView quan_txt;
    private IntentFilter mIntentFilter;
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(mBroadcastString)) {
                count = Utility.getIngerSharedPreferences(mContext, Constants.COUNT);
                System.out.println("GETCOUNT------>>>" + count);
                invalidateOptionsMenu();
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Utility.ChangeLang(getApplicationContext(), Utility.getIngerSharedPreferences(getApplicationContext(), Constants.LANG));
        setContentView(R.layout.activity_prod_desc);
        mContext = this;
        setSupportActionBar((Toolbar) findViewById(R.id.h_toolbar));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((Toolbar) findViewById(R.id.h_toolbar)).setNavigationIcon(R.drawable.ic_navigate_before_white);

        isopenHome = "Hiiii";
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(mBroadcastString);
        count = Utility.getIngerSharedPreferences(mContext, Constants.COUNT);

        Utility.hideKeyboard(mContext);
        //7415453558
        Find();

        Listner();
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            registerReceiver(mReceiver, mIntentFilter);
        } catch (Exception e) {

        }
    }

    @Override
    protected void onDestroy() {
        isopenHome = null;
        try {
            unregisterReceiver(mReceiver);
        } catch (Exception e) {
        }
        super.onDestroy();
    }

    public void Find() {
        related_pro_list = (RecyclerView) findViewById(R.id.related_pro_list);

        star1_rat = (ImageView) findViewById(R.id.star1_rat);
        star2_rat = (ImageView) findViewById(R.id.star2_rat);
        star3_rat = (ImageView) findViewById(R.id.star3_rat);
        star4_rat = (ImageView) findViewById(R.id.star4_rat);
        star5_rat = (ImageView) findViewById(R.id.star5_rat);

        btn_add2cart = (LinearLayout) findViewById(R.id.btn_add2cart);
        btn_add2cart.setOnClickListener(this);
        quan_txt = (CustomTextView) findViewById(R.id.quan_txt);


        findViewById(R.id.add_quan).setOnClickListener(this);
        findViewById(R.id.minus_quan).setOnClickListener(this);
        findViewById(R.id.pro_det_image).setOnClickListener(this);
        findViewById(R.id.review_layout).setOnClickListener(this);

        pro_det_formlation = (CustomTextView) findViewById(R.id.pro_det_formlation);
        pro_det_manufacture = (CustomTextView) findViewById(R.id.pro_det_manufacture);
        pro_det_package_type = (CustomTextView) findViewById(R.id.pro_det_package_type);
        pro_det_size = (CustomTextView) findViewById(R.id.pro_det_size);

        ((CustomTextView) findViewById(R.id.pro_det_actualprice)).setPaintFlags(((CustomTextView) findViewById(R.id.pro_det_actualprice)).getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

   /*if(Utility.getSharedPreferences(mContext, Constants.LANG).equals("en")){
       final int sdk = android.os.Build.VERSION.SDK_INT;
       if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
           spnr_quant.setBackgroundDrawable( getResources().getDrawable(R.drawable.select_spinner_black) );
       } else {
           spnr_quant.setBackground( getResources().getDrawable(R.drawable.select_spinner_black));
       }
        }else{

       final int sdk = android.os.Build.VERSION.SDK_INT;
       if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
           spnr_quant.setBackgroundDrawable( getResources().getDrawable(R.drawable.select_spinner_black_ar) );
       } else {
           spnr_quant.setBackground( getResources().getDrawable(R.drawable.select_spinner_black_ar));
       }*/

    }

    public void Listner() {
        try {
            b = new Bundle();
            b = getIntent().getExtras();
            ProductId = b.getString(Constants.PRODUCTID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (Utility.isConnectingToInternet(mContext)) {
            GetProductDetail(ProductId);
        } else {
            Ttoast.show(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        related_pro_list.setLayoutManager(linearLayoutManager);
    }

    public void setAdapter() {
        adap = new RelatedHorizontalListAdapter(mContext, R.layout.adap_relatedproduct_itemview, relatedarray);
        related_pro_list.setAdapter(adap);
    }

    public void GetProductDetail(String productid) {
        /*http://infograins.com.208-91-199-7.md-plesk-web2.webhostbox.net/INFO01/pharmacy/api/api.php?action=
       SellingProductDetailById&product_id=1*/
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constants.ACTION, Constants.SELLINGPRODUCT_DETAIL);
            jo.put(Constants.PRODUCTID, productid);
            jo.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
            jo.put(Constants.DEVICEID, Utility.getSharedPreferences(mContext, Constants.FCMID));

            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));

            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {
                        JSONObject jo = new JSONObject(result);
                        if (jo.getString("success").equals("1")) {
                            JSONObject jobj = jo.getJSONObject(Constants.OBJECT);
                            for (int i = 0; i < jobj.length(); i++) {
                                ((CustomTextView) findViewById(R.id.app_name)).setText(jobj.getString("title"));
                                Glide.with(mContext).load(jobj.getString("product_image")).
                                        listener(new RequestListener<String, GlideDrawable>() {
                                            @Override
                                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                                findViewById(R.id.order_det_pb).setVisibility(View.GONE);
                                                return false;
                                            }

                                            @Override
                                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                                findViewById(R.id.order_det_pb).setVisibility(View.GONE);
                                                return false;
                                            }
                                        }).into((ImageView) findViewById(R.id.pro_det_image));

                                ImgPath = jobj.getString("product_image");

                                if (jobj.getString(Constants.PRESCRIPTION_STATUS).equals("1")) {
                                    findViewById(R.id.pro_det_rx).setVisibility(View.VISIBLE);
                                }
                                String dp = jobj.getString("discount_price") + " " + mContext.getResources().getString(R.string.currncy_symbl);
                                String pri = jobj.getString("price") + " " + mContext.getResources().getString(R.string.currncy_symbl);
                                quan_txt.setText(jobj.getString("quantity"));

                                if (jobj.getString("discount").equals("0")) {
                                    ((CustomTextView) findViewById(R.id.pro_det_price)).setText(pri);
                                    ((CustomTextView) findViewById(R.id.pro_det_actualprice)).setVisibility(View.GONE);
                                    ((CustomTextView) findViewById(R.id.dis_txt)).setVisibility(View.GONE);
                                } else {
                                    ((CustomTextView) findViewById(R.id.pro_det_actualprice)).setText(pri);
                                    ((CustomTextView) findViewById(R.id.pro_det_price)).setText(dp);
                                    ((CustomTextView) findViewById(R.id.dis_txt)).setText(getResources().getString(R.string.save_off) + " " + jobj.getString("discount") + "%");
                                }

                                review = jobj.getString("review");
                                ((CustomTextView) findViewById(R.id.tv_total_review)).setText(jobj.getString("review"));
                                ((CustomTextView) findViewById(R.id.star_rating)).setText(Float.parseFloat(jobj.getString("product_rate")) + "/5");

                                Ratting(Float.parseFloat(jobj.getString("product_rate")));

                                MaxQuantity = jobj.getString("max_quantity");
                                Quantity = Integer.parseInt((jobj.getString("quantity")));

                                pro_det_formlation.setText(jobj.getString("formulation"));
                                pro_det_manufacture.setText(jobj.getString("manufacturer"));
                                pro_det_package_type.setText(jobj.getString("packaging_type"));
                                pro_det_size.setText(jobj.getString("size"));

                            }
                            JSONArray jj = jobj.getJSONArray("similar_product");
                            for (int i = 0; i < jj.length(); i++) {
                                relatedarray.add(jj.getJSONObject(i));
                            }
                            setAdapter();

                        } else {
                            Ttoast.show(mContext, jo.getString(Constants.SERVER_MSG), false);
                        }
                    } catch (JSONException j) {
                        j.printStackTrace();
                    }
                }
            });
            rt.execute();
        } catch (JSONException g) {
            g.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);

        MenuItem item = menu.findItem(R.id.menu_addcart);

        LayerDrawable icon = (LayerDrawable) item.getIcon();
        Util.setBadgeCount(this, icon, count);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.menu_search:
                break;

            case R.id.menu_addcart:
                if (Utility.getSharedPreferences(mContext, Constants.GUEST_USER).equals("1")) {
                    startActivity(new Intent(mContext, Activity_AddtoCart_List.class));
                } else {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.youneedlogin), false);
                }
                break;

            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mContext, MainActivity.class));
        finish();
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add2cart:
                if (Utility.getSharedPreferences(mContext, Constants.GUEST_USER).equals("0")) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.youneedlogin), false);
                } else {
                    if (Utility.isConnectingToInternet(mContext)) {
                        if (Quantity != 0) {
                            Utility.setIntegerSharedPreference(mContext,Constants.TOTAL_SAV,1);
                            AddtoCartTask();
                        } else {
                            Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.not_addded_cart), true);
                        }
                    } else {
                        Ttoast.show(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
                    }
                }
                break;
            case R.id.add_quan:
                if (Quantity == (Integer.parseInt(MaxQuantity))) {
                    System.out.println("QUANT======> " + Quantity);
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.quant_not_available), false);

                } else {
                    Quantity = Quantity + 1;
                    System.out.println("SELECTED QUANT=====> " + Quantity);
                    quan_txt.setText(String.valueOf(Quantity));
                }
                break;

            case R.id.minus_quan:
                if (Quantity == 1) {

                } else {
                    Quantity = Quantity - 1;
                    System.out.println("SELECTED QUANT=====> " + Quantity);
                    quan_txt.setText(String.valueOf(Quantity));
                }
                break;

            case R.id.pro_det_image:
                ZoomImageView();
                break;

            case R.id.review_layout:
                if (!review.equals("0")) {
                    startActivity(new Intent(mContext, Activity_Review_Screen.class).putExtra("ProductId", ProductId));
                    finish();
                } else {
                    Ttoast.show(mContext, getResources().getString(R.string.no_review), false);
                }
                break;
        }
    }

    public void ZoomImageView() {
        dialog_imagezoom = new Dialog(mContext);
        dialog_imagezoom = new Dialog(mContext, R.style.MyDialog);
        dialog_imagezoom.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_imagezoom.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        dialog_imagezoom.setContentView(R.layout.zoomage_imageview);
        dialog_imagezoom.setCancelable(true);
        dialog_imagezoom.show();

        Glide.with(mContext).load(ImgPath).diskCacheStrategy(DiskCacheStrategy.ALL)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        ((ProgressBar) dialog_imagezoom.findViewById(R.id.pb)).setVisibility(View.GONE);
                        return false;
                    }
                }).into(((ImageView) dialog_imagezoom.findViewById(R.id.img_zoom)));

        ((ZoomageView) dialog_imagezoom.findViewById(R.id.img_zoom)).setTranslatable(true);
        ((ZoomageView) dialog_imagezoom.findViewById(R.id.img_zoom)).setZoomable(true);
        ((ZoomageView) dialog_imagezoom.findViewById(R.id.img_zoom)).setAutoCenter(true);
        ((ZoomageView) dialog_imagezoom.findViewById(R.id.img_zoom)).setAnimateOnReset(true);
    }

    public void AddtoCartTask() {
/*http://infograins.com.208-91-199-7.md-plesk-web2.webhostbox.net/INFO01/pharmacy/api/api.php?
action=AddToCart&product_id=2&userid=1*/
        try {
            JSONObject j = new JSONObject();
            j.put(Constants.ACTION, Constants.ADDTOCART);
            j.put(Constants.PRODUCTID, ProductId);
            j.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
            if (Quantity == 0) {
                j.put(Constants.QUANTITY, 1);
            } else {
                j.put(Constants.QUANTITY, Quantity);
            }
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));

            rt = new ResponseTask(mContext, j);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {
                        if (result == null) {
                            Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.server_fail), false);
                        } else {
                            JSONObject jo = new JSONObject(result);
                            if (jo.getString("success").equals("1")) {
                                Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.item_addto_cart), false);
                              /*  startActivity(new Intent(mContext, Activity_AddtoCart_List.class));
                                finish();*/
                            } else {
                                Ttoast.ShowToast(mContext, jo.getString("msg"), false);
                            }
                        }

                    } catch (JSONException j) {
                        j.printStackTrace();
                    }
                }

            });
            rt.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void Ratting(float rate) {
        if (rate >= 0 && rate < 0.5) {
            star1_rat.setImageResource(R.drawable.empty_star_grey);
            star2_rat.setImageResource(R.drawable.empty_star_grey);
            star3_rat.setImageResource(R.drawable.empty_star_grey);
            star4_rat.setImageResource(R.drawable.empty_star_grey);
            star5_rat.setImageResource(R.drawable.empty_star_grey);

        } else if (rate >= 0.5 && rate < 1) {
            star1_rat.setImageResource(R.drawable.star_half);
            star2_rat.setImageResource(R.drawable.empty_star_grey);
            star3_rat.setImageResource(R.drawable.empty_star_grey);
            star4_rat.setImageResource(R.drawable.empty_star_grey);
            star5_rat.setImageResource(R.drawable.empty_star_grey);

        } else if (rate >= 1 && rate < 1.5) {
            star1_rat.setImageResource(R.drawable.star_green);
            star2_rat.setImageResource(R.drawable.empty_star_grey);
            star3_rat.setImageResource(R.drawable.empty_star_grey);
            star4_rat.setImageResource(R.drawable.empty_star_grey);
            star5_rat.setImageResource(R.drawable.empty_star_grey);
        } else if (rate >= 1.5 && rate < 2) {
            star1_rat.setImageResource(R.drawable.star_green);
            star2_rat.setImageResource(R.drawable.star_half);
            star3_rat.setImageResource(R.drawable.empty_star_grey);
            star4_rat.setImageResource(R.drawable.empty_star_grey);
            star5_rat.setImageResource(R.drawable.empty_star_grey);
        } else if (rate >= 2 && rate < 2.5) {
            star1_rat.setImageResource(R.drawable.star_green);
            star2_rat.setImageResource(R.drawable.star_green);
            star3_rat.setImageResource(R.drawable.empty_star_grey);
            star4_rat.setImageResource(R.drawable.empty_star_grey);
            star5_rat.setImageResource(R.drawable.empty_star_grey);
        } else if (rate >= 2.5 && rate < 3) {
            star1_rat.setImageResource(R.drawable.star_green);
            star2_rat.setImageResource(R.drawable.star_green);
            star3_rat.setImageResource(R.drawable.star_half);
            star4_rat.setImageResource(R.drawable.empty_star_grey);
            star5_rat.setImageResource(R.drawable.empty_star_grey);
        } else if (rate >= 3 && rate < 3.5) {
            star1_rat.setImageResource(R.drawable.star_green);
            star2_rat.setImageResource(R.drawable.star_green);
            star3_rat.setImageResource(R.drawable.star_green);
            star4_rat.setImageResource(R.drawable.empty_star_grey);
            star5_rat.setImageResource(R.drawable.empty_star_grey);
        } else if (rate >= 3.5 && rate < 4) {
            star1_rat.setImageResource(R.drawable.star_green);
            star2_rat.setImageResource(R.drawable.star_green);
            star3_rat.setImageResource(R.drawable.star_green);
            star4_rat.setImageResource(R.drawable.star_half);
            star5_rat.setImageResource(R.drawable.empty_star_grey);
        } else if (rate >= 4 && rate < 4.5) {
            star1_rat.setImageResource(R.drawable.star_green);
            star2_rat.setImageResource(R.drawable.star_green);
            star3_rat.setImageResource(R.drawable.star_green);
            star4_rat.setImageResource(R.drawable.star_green);
            star5_rat.setImageResource(R.drawable.empty_star_grey);
        } else if (rate >= 4.5 && rate < 5) {
            star1_rat.setImageResource(R.drawable.star_green);
            star2_rat.setImageResource(R.drawable.star_green);
            star3_rat.setImageResource(R.drawable.star_green);
            star4_rat.setImageResource(R.drawable.star_green);
            star5_rat.setImageResource(R.drawable.star_half);
        } else if (rate == 5) {
            star1_rat.setImageResource(R.drawable.star_green);
            star2_rat.setImageResource(R.drawable.star_green);
            star3_rat.setImageResource(R.drawable.star_green);
            star4_rat.setImageResource(R.drawable.star_green);
            star5_rat.setImageResource(R.drawable.star_green);
        }
    }

}
