package com.sehatyuser24.activity.other;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.SimpleAdapter;


import com.sehatyuser24.R;
import com.sehatyuser24.activity.registration.MainActivity;
import com.sehatyuser24.customwidget.CustomAutoCompleteTextView;
import com.sehatyuser24.customwidget.CustomButton;
import com.sehatyuser24.customwidget.CustomEditText;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.serverinteraction.GetResponseTask;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.Constant_Urls;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.PlaceJSONParser;
import com.sehatyuser24.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Tejas on 12/5/17.
 */

public class Activity_AddShippingAddress extends AppCompatActivity {

    Context mContext;
    ResponseTask rt;
    CustomEditText ship_name, ship_mobile, ship_landmark, shipping_buildingno,
            shipping_flatno, ship_flor_no, ship_disct,ship_city;
    Bundle b;
    String ShipId = "", Shippingaddress = "";
    double lati = 0.0, longi = 0.0;
    CustomTextView ship_street;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utility.ChangeLang(getApplicationContext(),Utility.getIngerSharedPreferences(getApplicationContext(),Constants.LANG));
        setContentView(R.layout.activity_add_shipping_add);
        mContext = this;

        setSupportActionBar((Toolbar) findViewById(R.id.h_toolbar));

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((Toolbar) findViewById(R.id.h_toolbar)).setNavigationIcon(R.drawable.ic_navigate_before_white);
        ((CustomTextView) findViewById(R.id.app_name)).setText(mContext.getResources().getString(R.string.add_ship_address));
        Utility.hideKeyboard(mContext);

        Find();
    }

    public void Find() {
        ship_name = (CustomEditText) findViewById(R.id.ship_name);
        ship_mobile = (CustomEditText) findViewById(R.id.ship_mobile);
        ship_landmark = (CustomEditText) findViewById(R.id.ship_landmark);
        ship_city = (CustomEditText) findViewById(R.id.ship_city);
        ship_disct = (CustomEditText) findViewById(R.id.dist);
        ship_street = (CustomTextView) findViewById(R.id.ship_street);
        shipping_buildingno = (CustomEditText) findViewById(R.id.shipping_buildingno);
        shipping_flatno = (CustomEditText) findViewById(R.id.shipping_flatno);
        ship_flor_no = (CustomEditText) findViewById(R.id.ship_flor_no);
        ship_disct = (CustomEditText) findViewById(R.id.ship_disct);

        Listner();
    }

    public void Listner() {
        if (Utility.getSharedPreferences(mContext, Constants.SHIPPING_STATUS).equals("0")) {
            ((CustomButton) findViewById(R.id.billing_add_address_btn)).
                    setText(mContext.getResources().getString(R.string.shipping_address));
        }

        try {
            b = new Bundle();
            if (!b.equals("")) {
                String shipbundle;
                b = getIntent().getExtras();
                shipbundle = b.getString(Constants.ShippingBundle);
                System.out.println("JSONOBJ " + shipbundle);
                JSONObject j = new JSONObject(shipbundle);
                ShipId = j.getString("shipping_id");
                ship_name.setText(j.getString("fullname"));
                ship_mobile.setText(j.getString("mobile"));
                ship_landmark.setText(j.getString("landmark"));
                ship_city.setText(j.getString("city"));
                shipping_buildingno.setText(j.getString("building_number"));
                shipping_flatno.setText(j.getString("flat_number"));
                ship_street.setText(j.getString("street"));
                ship_disct.setText(j.getString("district"));
                ship_flor_no.setText(j.getString("floor_number"));

            }
        } catch (Exception h) {
            h.printStackTrace();
        }

        ship_street.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, Activity_GetAddress.class);
                        startActivityForResult(i,4);
            }
        });

        findViewById(R.id.billing_add_address_btn).setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ship_name.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_fname), false);
                } else if (ship_mobile.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_mobile), false);
                } else if (ship_landmark.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_landmark), false);
                } else if (ship_city.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_city), false);
                } else if (ship_street.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_street), false);
                } else if (shipping_buildingno.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_building), false);
                } else if (ship_flor_no.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_floor), false);
                } else if (ship_disct.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_dist), false);
                } else if (shipping_flatno.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_flat), false);
                } else {
                    if (Utility.isConnectingToInternet(mContext)) {

                        if (Utility.getSharedPreferences(mContext, Constants.SHIPPING_STATUS).equals("0")) {
                            AddShippingAddress(Constants.ADD_SHIPPINGADDRESS, "", ship_name.getText().toString(), ship_mobile.getText().toString(),
                                    ship_landmark.getText().toString(), ship_city.getText().toString(),
                                    shipping_buildingno.getText().toString(),
                                    shipping_flatno.getText().toString(), ship_street.getText().toString(),
                                    ship_flor_no.getText().toString(), ship_disct.getText().toString());
                        } else {
                            AddShippingAddress(Constants.UPDATESHIPPINGADDRESS, ShipId, ship_name.getText().toString(), ship_mobile.getText().toString(),
                                    ship_landmark.getText().toString(), ship_city.getText().toString(),
                                    shipping_buildingno.getText().toString(),
                                    shipping_flatno.getText().toString(), ship_street.getText().toString(),
                                    ship_flor_no.getText().toString(), ship_disct.getText().toString());
                        }

                    } else {
                        Ttoast.show(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
                    }
                }
            }
        });


    }


    public void AddShippingAddress(String action, String shipid, String fname, String mo,
                                   String landmark, String city, String b_no,
                                   String f_no, String pick_address, String flor_no, String dist) {

/*http://infograins.com.208-91-199-7.md-plesk-web2.webhostbox.net/INFO01/pharmacy/api/api.php?action=UpdateShippingAddressById&shipping_id=1&
fullname=piyush&mobile=+91976565652&landmark=402 mhow naka indore&city=indore&district=indore&street=flat no 425*/
        try {
            JSONObject j = new JSONObject();
            j.put(Constants.ACTION, action);
            j.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
            j.put(Constants.SHIPPINGTID, shipid);
            j.put(Constants.FULLNAME, fname);
            j.put(Constants.MOBILE, mo);
            j.put(Constants.LANDMARK, landmark);
            j.put(Constants.CITY, city);
            j.put(Constants.BUILDINGNUMBER, b_no);
            j.put(Constants.FLATNUMBER, f_no);
            j.put(Constants.STREET, pick_address);
            j.put(Constants.DIST, pick_address);
            j.put(Constants.FLORNUMBER, flor_no);
            j.put(Constants.DIST, dist);

            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(mContext, j);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.server_fail), false);
                    } else {
                        try {
                            JSONObject jo = new JSONObject(result);
                            if (jo.getString("success").equals("1")) {
                                Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.shipaddress_updated), false);
                                Utility.setSharedPreference(mContext, Constants.TOTAL_SAV, "1");
                                Utility.setSharedPreference(mContext, Constants.ShippingCheck, "1");
                               /// Activity_AddShippingAddress.this.finish();
                                //startActivity(getIntent());

                            } else {

                            }
                        } catch (JSONException j) {
                            j.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException h) {
            h.printStackTrace();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 4 && resultCode == Activity.RESULT_OK) {
            Shippingaddress = data.getStringExtra("PICKADDRESS");
            ship_street.setText(Shippingaddress);
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
