package com.sehatyuser24.activity.other;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.GridView;

import com.sehatyuser24.R;
import com.sehatyuser24.adapter.AdapSubcat;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by and-04 on 21/9/17.
 */

public class Activity_Medicine_Subcategory extends AppCompatActivity {
    Context mContext;
    GridView medi_subcat_grid;
    AdapSubcat adap;
    ResponseTask rt;
    List<JSONObject> arra_list = new ArrayList<>();
    Bundle b;
    String Catid = "", CatName = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Utility.ChangeLang(getApplicationContext(), Utility.getIngerSharedPreferences(getApplicationContext(), Constants.LANG));

        setContentView(R.layout.act_medi_subcat);
        mContext = this;

        setSupportActionBar((Toolbar) findViewById(R.id.h_toolbar));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((Toolbar) findViewById(R.id.h_toolbar)).setNavigationIcon(R.drawable.ic_navigate_before_white);
        try {
            b = new Bundle();
            b = getIntent().getExtras();
            if (!b.equals("")) {
                Catid = b.getString(Constants.CATEGORYID);
                CatName = b.getString(Constants.CATNAME);
                ((CustomTextView) findViewById(R.id.app_name)).setText(CatName);

            } else {
                ((CustomTextView) findViewById(R.id.app_name)).setText(mContext.getResources().getString(R.string.category));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (Utility.isConnectingToInternet(mContext)) {
            if (Utility.getIngerSharedPreferences(mContext, Constants.BTNCLK) == 3) {
                GetSubCat(Constants.GEMALECARE_PRDCT);
            } else {
                GetSubCat(Constants.GETPRODCTBYCAT);
            }
        } else {
            Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
        }

        Utility.hideKeyboard(mContext);
    }

    public void GetSubCat(String action) {
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constants.ACTION,action);
            jo.put(Constants.CATEGORYID, Catid);
//            jo.put(Constants.CATEGORYID, "8");
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));

            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();

                    if (result == null) {
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.server_fail), false);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);

                            if (jobj.getString("success").equals("1")) {
                                JSONArray jarra = jobj.getJSONArray(Constants.OBJECT);
                                for (int i = 0; i < jarra.length(); i++) {
                                    arra_list.add(jarra.getJSONObject(i));
                                }
                                SetAdap();
                            } else {
                                Ttoast.ShowToast(mContext, jobj.getString("msg"), false);

                            }
                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (Exception je) {
            je.printStackTrace();
        }
    }

    public void SetAdap() {
        medi_subcat_grid = (GridView) findViewById(R.id.medi_subcat_grid);

        adap = new AdapSubcat(mContext, arra_list);
        medi_subcat_grid.setAdapter(adap);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mContext, Activity_Everyday_Medical_Product.class));
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        finish();
        super.onBackPressed();
    }
}
