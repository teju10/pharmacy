package com.sehatyuser24.activity.other;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ListView;

import com.sehatyuser24.R;
import com.sehatyuser24.adapter.Dosage_Adap;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by and-04 on 17/10/17.
 */

public class Act_DosageList extends AppCompatActivity {

    Context mContext;
    ResponseTask rt;
    ArrayList<JSONObject> mlist;
    Bundle b;
    String Orderno = "";
    Dosage_Adap adap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_dosage_lst);
        mContext = this;
        setSupportActionBar((Toolbar) findViewById(R.id.h_toolbar));

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((Toolbar) findViewById(R.id.h_toolbar)).setNavigationIcon(R.drawable.ic_navigate_before_white);

        ((CustomTextView) findViewById(R.id.app_name)).setText(mContext.getResources().getString(R.string.dosage));


        b = new Bundle();
        try {
            b = getIntent().getExtras();
            Orderno = b.getString(Constants.ORDERNO);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (Utility.isConnectingToInternet(mContext)) {
            GetCartList();
        } else {
            Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
        }
    }

    public void SetAdap() {
        adap = new Dosage_Adap(mContext, mlist);
        ((ListView) findViewById(R.id.doses_lst)).setAdapter(adap);
    }

    public void GetCartList() {
//        https://www.infograins.in/INFO01/Pharm01/api/api.php?action=
// getDosesListByOrderid&orderid=OD1499688892
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constants.ACTION, Constants.GETDOSESLST);
            jo.put(Constants.ORDERID, Orderno);
            jo.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));

            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.server_fail), false);
                    } else {
                        try {
                            JSONObject j = new JSONObject(result);
                            if (j.getString("success").equals("1")) {

                                JSONArray jaar = j.getJSONArray(Constants.OBJECT);

                                mlist = new ArrayList<JSONObject>();

                                for (int i = 0; i < jaar.length(); i++) {
                                    mlist.add(jaar.getJSONObject(i));
                                }
                                adap = new Dosage_Adap(mContext, mlist);
                                ((ListView) findViewById(R.id.doses_lst)).setAdapter(adap);
                                SetAdap();
                            } else {
                                Ttoast.ShowToast(mContext, j.getString("msg"), true);
                            }
                        } catch (JSONException t) {
                            t.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException j) {
            j.printStackTrace();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
}
