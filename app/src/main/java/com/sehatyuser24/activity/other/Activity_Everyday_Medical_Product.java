package com.sehatyuser24.activity.other;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ListView;
import com.sehatyuser24.R;
import com.sehatyuser24.activity.registration.MainActivity;
import com.sehatyuser24.adapter.Everyday_Prodct_avail;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Utility;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by and-04 on 20/9/17.
 */

public class Activity_Everyday_Medical_Product extends AppCompatActivity {

    Context mContext;
    List<JSONObject> mlist = new ArrayList<>();
    Everyday_Prodct_avail adap;
    ResponseTask rt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Utility.ChangeLang(getApplicationContext(), Utility.getIngerSharedPreferences(getApplicationContext(), Constants.LANG));

        setContentView(R.layout.activity_medi_category);
        mContext = this;

        setSupportActionBar((Toolbar) findViewById(R.id.h_toolbar));

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((Toolbar) findViewById(R.id.h_toolbar)).setNavigationIcon(R.drawable.ic_navigate_before_white);
        if (Utility.isConnectingToInternet(mContext)) {
            if (Utility.getIngerSharedPreferences(mContext, Constants.BTNCLK) == 0) {
                GetEveryDayPrdt(Constants.EVERYDAYPRDCT);
                ((CustomTextView) findViewById(R.id.app_name)).setText(mContext.getResources().getString(R.string.everyday_medical));

            } else if (Utility.getIngerSharedPreferences(mContext, Constants.BTNCLK) == 1) {
                GetEveryDayPrdt(Constants.BABYMOTHCARE);
                ((CustomTextView) findViewById(R.id.app_name)).setText(mContext.getResources().getString(R.string.baby));
            }
        } else {
            Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
        }

        Utility.hideKeyboard(mContext);
    }

    public void SetAdapter() {
        adap = new Everyday_Prodct_avail(mContext, mlist);
        ((ListView) findViewById(R.id.sub_cat_lst)).setAdapter(adap);
    }

    public void GetEveryDayPrdt(String action) {
        try {
            JSONObject j = new JSONObject();
            j.put(Constants.ACTION, action);
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));

            rt = new ResponseTask(mContext, j);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.server_fail), false);
                    } else {
                        try {
                            JSONObject jo = new JSONObject(result);
                            if (jo.getString("success").equals("1")) {
                                JSONArray j = jo.getJSONArray(Constants.OBJECT);
                                for (int i = 0; i < j.length(); i++) {
                                    mlist.add(j.getJSONObject(i));
                                }
                                SetAdapter();
                            } else {
                                Ttoast.ShowToast(mContext, jo.getString("msg"), false);
                            }

                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException je) {
            je.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mContext, MainActivity.class));
        finish();
        super.onBackPressed();
    }

}
