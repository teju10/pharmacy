package com.sehatyuser24.activity.other;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.sehatyuser24.R;
import com.sehatyuser24.activity.registration.ActivityChangePW;
import com.sehatyuser24.activity.registration.SelectLanguageActivity;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Utility;


/**
 * Created by Tejas on 28/4/17.
 */

public class ActivitySetting extends AppCompatActivity implements View.OnClickListener {

    Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utility.ChangeLang(getApplicationContext(),Utility.getIngerSharedPreferences(getApplicationContext(),Constants.LANG));
        Utility.ChangeLang(getApplicationContext(),  Utility.getIngerSharedPreferences(getApplicationContext(), Constants.LANG));
        setContentView(R.layout.activity_setting);
        mContext = this;

        Find();

    }

    public void Find() {
        if (Utility.getSharedPreferences(mContext, Constants.LOGINTYPE).equals("social_login")) {
            findViewById(R.id.change_pw).setVisibility(View.GONE);
        } else {
            findViewById(R.id.change_pw).setVisibility(View.VISIBLE);

        }
        findViewById(R.id.change_pw).setOnClickListener(this);
        findViewById(R.id.setting_my_order).setOnClickListener(this);
        findViewById(R.id.chnge_lang).setOnClickListener(this);
        Listner();
    }

    public void Listner() {
        findViewById(R.id.setting_my_order).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.change_pw:
                startActivity(new Intent(mContext, ActivityChangePW.class));
                finish();
                break;

            case R.id.chnge_lang:
                startActivity(new Intent(mContext, SelectLanguageActivity.class));
                overridePendingTransition(android.R.anim.slide_in_left,android.R.anim.slide_out_right);
                finish();
                break;
        }
    }

}
