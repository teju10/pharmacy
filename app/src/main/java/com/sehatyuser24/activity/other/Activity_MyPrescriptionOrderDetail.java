package com.sehatyuser24.activity.other;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.sehatyuser24.R;
import com.sehatyuser24.customwidget.CustomButton;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.ExpandableHeightListView;
import com.sehatyuser24.utility.Helper;
import com.sehatyuser24.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by and-04 on 15/9/17.
 */

public class Activity_MyPrescriptionOrderDetail extends AppCompatActivity implements View.OnClickListener {

    Context mContext;
    ResponseTask rt;
    ArrayList<String> ja = new ArrayList<>();
    String[] image;
    String Prsc_id, grand_total = "", ship_charges = "", OrderId = "";
    ProductAdapter pro_adap;
    ExpandableHeightListView pro_list;
    ArrayList<JSONObject> array_prodct = new ArrayList<>();
    CustomButton prsc_proceed_to_pay;
    Bundle b;
    JSONObject billjobj, shipjobj;
    ImageViewHori setAdapter;

    ArrayList<String> imglst = new ArrayList<>();
    DividerItemDecoration mDividerItemDecoration;
    RecyclerView prescribe_hori_image;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Utility.ChangeLang(getApplicationContext(), Utility.getIngerSharedPreferences(getApplicationContext(), Constants.LANG));

        setContentView(R.layout.acivity_my_presc_detail);
        mContext = this;
        setSupportActionBar((Toolbar) findViewById(R.id.h_toolbar));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((Toolbar) findViewById(R.id.h_toolbar)).setNavigationIcon(R.drawable.ic_navigate_before_white);
        ((CustomTextView) findViewById(R.id.app_name)).
                setText(mContext.getResources().getString(R.string.presc_detail));

        Init();
        pro_list = (ExpandableHeightListView) findViewById(R.id.pro_list);

        b = new Bundle();

        Utility.hideKeyboard(mContext);
    }

    public void Init() {
        b = new Bundle();
        try {
            if (!b.equals("")) {
                b = getIntent().getExtras();
                Prsc_id = b.getString(Constants.PRECRIPTIONID);
                System.out.println("GET ARRALIST===>  " + Prsc_id);
            }
        } catch (Exception je) {
            je.printStackTrace();
        }

        prsc_proceed_to_pay = (CustomButton) findViewById(R.id.prsc_proceed_to_pay);
        prescribe_hori_image = (RecyclerView) findViewById(R.id.prescribe_hori_image);
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        prescribe_hori_image.setLayoutManager(horizontalLayoutManager);

        prsc_proceed_to_pay.setOnClickListener(this);
        findViewById(R.id.edit_shipaddress_btn).setOnClickListener(this);
        findViewById(R.id.edit_billaddress_btn).setOnClickListener(this);
        findViewById(R.id.btn_dosage).setOnClickListener(this);

        mDividerItemDecoration = new DividerItemDecoration(prescribe_hori_image.getContext(), horizontalLayoutManager.getOrientation());
        prescribe_hori_image.addItemDecoration(mDividerItemDecoration);
    }

    @Override
    protected void onResume() {
        if (Utility.isConnectingToInternet(mContext)) {
            GetPrescOrderDetail();
        } else {
            Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
        }
        super.onResume();
    }

    public void GetPrescOrderDetail() {
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constants.ACTION, Constants.PRSCORDER_DETAIL);
            jo.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
            jo.put(Constants.PRECRIPTIONID, Prsc_id);

            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));

            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.server_fail), false);
                    } else {
                        try {
                            JSONObject j = new JSONObject(result);
                            if (j.getString("success").equals("1")) {
                                JSONObject jobj = j.getJSONObject(Constants.OBJECT);

                                if (j.getString("prescription_status").equals("5")) {
                                    prsc_proceed_to_pay.setVisibility(View.VISIBLE);
                                    findViewById(R.id.sec_ll).setVisibility(View.VISIBLE);
                                } else {
                                    prsc_proceed_to_pay.setVisibility(View.GONE);
                                }
                                JSONObject prsc_detail = jobj.getJSONObject("prescrition_detail");

                                OrderId = prsc_detail.getString("orderid");
                                String date[] = prsc_detail.getString("datetime").split(" ");
                                String Formate_date = Utility.ChangeDateFormat("yyyy-MM-dd", "dd MMM yyyy", date[0]);

                                //String time = Utility.Time(date[1]);
                                ((CustomTextView) findViewById(R.id.prsc_date)).setText(Formate_date + " | " + date[1]);

                                JSONArray jaimg = prsc_detail.getJSONArray("prescription_image");
                                imglst.clear();
                                for (int i = 0; i < jaimg.length(); i++) {
                                    String imgstr = jaimg.getString(i);
                                    imglst.add(imgstr);
                                }
                                SetImgAdap();

                                ((CustomTextView) findViewById(R.id.prsc_det_prscid)).
                                        setText(prsc_detail.getString("prescrition_id"));

                                if (prsc_detail.getString("prescription_status").equals("0") ||
                                        prsc_detail.getString("prescription_status").equals("1") ||
                                        prsc_detail.getString("prescription_status").equals("2")) {
                                    ((CustomTextView) findViewById(R.id.presc_status)).
                                            setText(mContext.getResources().getString(R.string.pending));
                                    ((CustomTextView) findViewById(R.id.presc_status)).
                                            setTextColor(mContext.getResources().getColor(R.color.orange_papya));
                                } else if (prsc_detail.getString("prescription_status").equals("3")) {
                                    ((CustomTextView) findViewById(R.id.presc_status)).
                                            setText(mContext.getResources().getString(R.string.presc_rejected));
                                    ((CustomTextView) findViewById(R.id.presc_status)).
                                            setTextColor(mContext.getResources().getColor(R.color.orange_papya));
                                } else if (prsc_detail.getString("prescription_status").equals("4")) {
                                    ((CustomTextView) findViewById(R.id.presc_status)).setText(mContext.getResources().getString(R.string.cencelled));
                                    ((CustomTextView) findViewById(R.id.presc_status)).setTextColor(mContext.getResources().getColor(R.color.orange_papya));
                                } else if (prsc_detail.getString("prescription_status").equals("5")) {
                                    ((CustomTextView) findViewById(R.id.presc_status)).
                                            setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
                                    ((CustomTextView) findViewById(R.id.presc_status)).
                                            setText(mContext.getResources().getString(R.string.approved));
                                } else if (prsc_detail.getString("prescription_status").equals("6")) {
                                    ((CustomTextView) findViewById(R.id.presc_status)).
                                            setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
                                    ((CustomTextView) findViewById(R.id.presc_status)).
                                            setText(mContext.getResources().getString(R.string.order_place));
                                }

                                JSONObject ship_add = jobj.getJSONObject("shipping_address");

                                JSONObject bill_add = jobj.getJSONObject("billing_address");

                                JSONObject PriceDet = jobj.getJSONObject("price_data");

                                SetPriceDetail(PriceDet);

                                SetShipAdd(ship_add);

                                SetBillAdd(bill_add);

                                if (j.getString("prescription_status").equals("5") || j.getString("prescription_status").equals("6")) {
                                    findViewById(R.id.prodct_lst).setVisibility(View.VISIBLE);
                                    array_prodct.clear();
                                    JSONArray ja = prsc_detail.getJSONArray("product_record");

                                    for (int i = 0; i < ja.length(); i++) {
                                        array_prodct.add(ja.getJSONObject(i));
                                    }
                                    SetAdapter();
                                } else {

                                }
                            } else {
                                Ttoast.ShowToast(mContext, j.getString("msg"), false);
                            }
                        } catch (JSONException je) {
                            je.printStackTrace();
                        }

                    }
                }
            });
            rt.execute();
        } catch (JSONException je) {
            je.printStackTrace();
        }
    }

    public void SetImgAdap() {
        setAdapter = new ImageViewHori(mContext, R.layout.adap_pricription_singleimge, imglst);
        prescribe_hori_image.setAdapter(setAdapter);
    }

    public void SetAdapter() {
        pro_adap = new ProductAdapter(mContext, R.layout.my_presc_prodct_item, array_prodct);
        pro_list.setAdapter(pro_adap);
        Helper.getListViewSize(pro_list);
        pro_list.setExpanded(true);
    }

    public void SetPriceDetail(JSONObject j) {
        try {
            String Symbol = mContext.getResources().getString(R.string.currncy_symbl);
            ((CustomTextView) findViewById(R.id.prsc_det_subtotal)).setText(j.getString("sum") + " " + getResources().getString(R.string.currncy_symbl));
            ((CustomTextView) findViewById(R.id.prsc_det_order_total)).setText(j.getString("grand_total") + " " + getResources().getString(R.string.currncy_symbl));
            ((CustomTextView) findViewById(R.id.prsc_det_ship_charges)).setText(j.getString("shipping_charge") + " " + getResources().getString(R.string.currncy_symbl));

            grand_total = j.getString("grand_total");
            ship_charges = j.getString("shipping_charge");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void SetShipAdd(JSONObject j) {
        try {
            shipjobj = j;
            ((CustomTextView) findViewById(R.id.prsc_det_sp_name)).setText(j.getString("fullname"));
            ((CustomTextView) findViewById(R.id.prsc_det_sp_mobile)).setText(j.getString("mobile"));
            ((CustomTextView) findViewById(R.id.prsc_det_sp_address)).setText(j.getString("street"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void SetBillAdd(JSONObject j) {
        try {
            billjobj = j;
            ((CustomTextView) findViewById(R.id.prsc_det_bill_name)).setText(j.getString("fname") + j.getString("lname"));
            ((CustomTextView) findViewById(R.id.prsc_det_bill_mobile)).setText(j.getString("mobile"));
            ((CustomTextView) findViewById(R.id.prsc_det_bill_email)).setText(j.getString("email"));
            ((CustomTextView) findViewById(R.id.prsc_det_bill_address)).setText(j.getString("address"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.prsc_proceed_to_pay:
                Utility.setIntegerSharedPreference(mContext, Constants.PAYMENT_BTN, 1);
                Intent i = new Intent(mContext, Activity_PaymentDeduct.class);
                b.putString(Constants.AMOUNT, grand_total);
                b.putString(Constants.SHIPPING_CHARGES, ship_charges);
                b.putString(Constants.PRECRIPTIONID, Prsc_id);
                i.putExtras(b);
                startActivity(i);
                break;

            case R.id.edit_shipaddress_btn:
                Intent inn = new Intent(mContext, Activity_AddShippingAddress.class);
                b.putString(Constants.ShippingBundle, shipjobj.toString());
                System.out.println("SHIPPING STATUS " + shipjobj);
                inn.putExtras(b);
                startActivity(inn);
                break;

            case R.id.edit_billaddress_btn:
                Intent in = new Intent(mContext, Activity_AddBillingAddress.class);
                b.putString(Constants.BillingBundle, billjobj.toString());
                System.out.println("BILLING STATUS " + billjobj);
                in.putExtras(b);
                startActivity(in);
                break;

            case R.id.btn_dosage:
                startActivity(new Intent(mContext, Act_DosageList.class).putExtra(Constants.ORDERNO, OrderId));
                break;

        }

    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    public class ImageViewHori extends RecyclerView.Adapter<ImageViewHori.SimpleViewHolder> {

        Context appContext;
        ArrayList<String> imglist;
        int res;

        public ImageViewHori(Context context, int res, ArrayList<String> mlist) {
            this.appContext = context;
            this.res = res;
            this.imglist = mlist;

        }

        @Override
        public ImageViewHori.SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View convertView = LayoutInflater.from(parent.getContext()).inflate(res, parent, false);
            return new ImageViewHori.SimpleViewHolder(convertView);
        }

        @Override
        public void onBindViewHolder(SimpleViewHolder holder, int position) {
            String imageString = imglist.get(position);
            System.out.println("ADAP_IMAGEPATH=====>" + imglist);
            holder.img_delete.setVisibility(View.GONE);
            Glide.with(appContext)
                    .load(imageString)
                    /*.listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {

                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {

                            return false;
                        }
                    })*/
                    .into(holder.presc_singleimage);
        }

        @Override
        public int getItemCount() {
            return imglist.size();
        }

        class SimpleViewHolder extends RecyclerView.ViewHolder {
            ImageView presc_singleimage, img_delete;

            public SimpleViewHolder(View v) {
                super(v);
                presc_singleimage = (ImageView) v.findViewById(R.id.presc_singleimage);
                img_delete = (ImageView) v.findViewById(R.id.img_delete);
            }
        }
    }

    public class ProductAdapter extends BaseAdapter {

        Context mContext;
        ArrayList<JSONObject> jlist;
        int res;


        public ProductAdapter(Context context, int res, ArrayList<JSONObject> jobj) {
            this.mContext = context;
            this.res = res;
            this.jlist = jobj;
        }

        @Override
        public int getCount() {
            return jlist.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = li.inflate(res, null);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            JSONObject j = jlist.get(position);
            try {
                viewHolder.prsc_ordr_medicine_n.setText(j.getString("title"));
                if (!j.getString("discount_price").equals("0")) {
                    viewHolder.prsc_ordr_price.setPaintFlags(viewHolder.prsc_ordr_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    viewHolder.prsc_ordr_price.setText(j.getString("price"));
                    viewHolder.prsc_ordr_act_price.setText(j.getString("discount_price") + " " + getResources().getString(R.string.currncy_symbl));
                } else {
                    viewHolder.prsc_ordr_price.setVisibility(View.GONE);
                    viewHolder.prsc_ordr_act_price.setText(j.getString("price"));
                }
                viewHolder.prsc_ordr_ship_chrge.setText(j.getString("shipping_charge"));
                viewHolder.prsc_ordr_quant.setText(j.getString("product_quantity"));

                double total_price = 0;
                if (!j.getString("discount_price").equals("")) {
                    total_price = (Double.valueOf(j.getString("product_quantity"))) *
                            (Double.valueOf(j.getString("discount_price")));
                } else {
                    total_price = (Integer.parseInt(j.getString("product_quantity"))) *
                            (Integer.parseInt(j.getString("price")));
                }
                System.out.println("TOTAL PRICE=======>" + total_price);
                viewHolder.prsc_ordr_total_price.setText(String.valueOf(total_price) + " " + getResources().getString(R.string.currncy_symbl));

                Glide.with(mContext).load(j.getString("product_image")).
                        listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                viewHolder.prsc_det_pb.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                viewHolder.prsc_det_pb.setVisibility(View.GONE);
                                return false;
                            }
                        }).
                        into(viewHolder.medi_img);


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return convertView;
        }

        class ViewHolder {

            CustomTextView prsc_ordr_medicine_n, prsc_ordr_comname, prsc_ordr_price,
                    prsc_ordr_act_price, prsc_ordr_ship_chrge, prsc_ordr_quant,
                    prsc_ordr_total_price, prsc_date;

            ImageView medi_img;
            ProgressBar prsc_det_pb;

            public ViewHolder(View base) {

                prsc_ordr_medicine_n = (CustomTextView) base.findViewById(R.id.prsc_ordr_medicine_n);
                prsc_ordr_comname = (CustomTextView) base.findViewById(R.id.prsc_ordr_comname);
                medi_img = (ImageView) base.findViewById(R.id.medi_img);
                prsc_ordr_price = (CustomTextView) base.findViewById(R.id.prsc_ordr_price);
                prsc_ordr_act_price = (CustomTextView) base.findViewById(R.id.prsc_ordr_act_price);
                prsc_ordr_ship_chrge = (CustomTextView) base.findViewById(R.id.prsc_ordr_ship_chrge);
                prsc_ordr_quant = (CustomTextView) base.findViewById(R.id.prsc_ordr_quant);
                prsc_ordr_total_price = (CustomTextView) base.findViewById(R.id.prsc_ordr_total_price);
                prsc_date = (CustomTextView) base.findViewById(R.id.prsc_date);

                prsc_det_pb = (ProgressBar) base.findViewById(R.id.prsc_det_pb);
            }
        }
    }
}
