package com.sehatyuser24.activity.other;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ListView;

import com.sehatyuser24.R;
import com.sehatyuser24.adapter.SearchAdapter;
import com.sehatyuser24.customwidget.CustomEditText;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.Constant_Urls;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tejas on 3/6/17.
 */

public class Activity_SearchingMedicine extends AppCompatActivity {

    Context mContext;
    SearchAdapter adap;
    List<JSONObject> jlist = new ArrayList<>();
    ListView searching_list;
    Boolean MORERESULT = true;
    int page = 0;
    CustomEditText search_edt_bar;
    ResponseTask rt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Utility.ChangeLang(getApplicationContext(),Utility.getIngerSharedPreferences(getApplicationContext(),Constants.LANG));

        setContentView(R.layout.activity_searching);

        mContext = this;
        searching_list = (ListView) findViewById(R.id.searching_list);
        SetEditTextChange();

        Utility.hideKeyboard(mContext);
    }

    public void SetEditTextChange() {
        search_edt_bar = (CustomEditText) findViewById(R.id.search_edt_bar);

        search_edt_bar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                search_edt_bar.getText().toString().equals("");
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length()==0) {
                    try {
                        jlist.clear();
                        adap.notifyDataSetChanged();
                        searching_list.setVisibility(View.GONE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 1) {
                    SearchingMedicineTask(search_edt_bar.getText().toString());
                    searching_list.setVisibility(View.VISIBLE);
                }
                if (s.length()==0) {
                    try {
                        jlist.clear();
                        adap.notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        });

        /*search_edt_bar.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    SearchingMedicineTask(search_edt_bar.getText().toString());
                    return true;
                }
                return false;
            }
        });*/
    }

    public void SetAdapter() {
        adap = new SearchAdapter(this, mContext, jlist);
        searching_list.setAdapter(adap);

       /* searching_list.setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (MORERESULT) {
                    if (Utility.isConnectingToInternet(mContext)) {
                        page = page + 1;
                        SearchingMedicineTask(search_edt_bar.getText().toString());
                    }
                } else {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
                }
            }
        });*/

    }

    public void SearchingMedicineTask(String key) {
        try {
            JSONObject jobj = new JSONObject();
            jobj.put(Constants.ACTION, Constants.SEARCHMEDICINE);
            jobj.put(Constants.SEARCHKEY, key);
//            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(mContext, jobj);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
//                    Utility.HideDialog();
                    if (result == null) {
                        page = page - 1;
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.server_fail), false);
                    } else {
                        try {
                            JSONObject jo = new JSONObject(result);
                            if (jo.getString("success").equals("1")) {
                                findViewById(R.id.not_avail).setVisibility(View.GONE);
                                JSONArray j = jo.getJSONArray(Constants.OBJECT);
                                jlist.clear();
                                for (int i = 0; i < j.length(); i++) {
                                    jlist.add(j.getJSONObject(i));
                                }
                                SetAdapter();
                            } else if (jo.getString("success").equals("2")) {
                                jlist.clear();
                                findViewById(R.id.not_avail).setVisibility(View.VISIBLE);
                            } else if (jo.getString("success").equals("0")) {
                                MORERESULT = false;
                                Ttoast.ShowToast(mContext, jo.getString(Constants.SERVER_MSG), false);
                            }
                        } catch (JSONException e) {
                            page = page - 1;
                            e.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException j) {
            j.printStackTrace();
        }
    }

}
