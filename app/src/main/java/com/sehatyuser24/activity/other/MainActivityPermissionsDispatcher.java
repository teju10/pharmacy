package com.sehatyuser24.activity.other;



import com.sehatyuser24.drawerfragment.HomeFragment;

import permissions.dispatcher.PermissionUtils;

/**
 * Created by Tejas on 11/23/2016.
 */
public class MainActivityPermissionsDispatcher {
    private static final int REQUEST_ONPICKPHOTO = 0;

    private static final String[] PERMISSION_ONPICKPHOTO = new String[]{"android.permission.WRITE_EXTERNAL_STORAGE"};

    private static final int REQUEST_ONPICKDOC = 1;

    private static final String[] PERMISSION_ONPICKDOC = new String[]{"android.permission.WRITE_EXTERNAL_STORAGE"};

    private MainActivityPermissionsDispatcher() {

    }


    public static void onRequestPermissionsResult(HomeFragment target, int requestCode, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ONPICKPHOTO:
                if (PermissionUtils.getTargetSdkVersion(target.getActivity()) < 23 && !PermissionUtils.hasSelfPermissions(target.getActivity(), PERMISSION_ONPICKPHOTO)) {
                    return;
                }
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    target.onPickPhoto();
                }
                break;
            /*case REQUEST_ONPICKDOC:
                if (PermissionUtils.getTargetSdkVersion(target.getActivity()) < 23 && !PermissionUtils.hasSelfPermissions(target.getActivity(), PERMISSION_ONPICKDOC)) {
                    return;
                }
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    target.onPickDoc();
                }
                break;*/
            default:
                break;
        }
    }
}
