package com.sehatyuser24.activity.other;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.sehatyuser24.R;
import com.sehatyuser24.adapter.ReviewRateAdapter;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.customwidget.LoadMoreListView;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.CircularProgressBar;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Helper;
import com.sehatyuser24.utility.Util;
import com.sehatyuser24.utility.Utility;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

import static com.sehatyuser24.R.id.rate_probar;

/**
 * Created by and-04 on 18/7/17.
 */

public class Activity_Review_Screen extends AppCompatActivity {
    Context mContext;
    ResponseTask rt;
    CustomTextView rating_per;
    ProgressBar pb1, pb2, pb3, pb4, pb5;
    ArrayList<JSONObject> reviewarray = new ArrayList<>();
    ReviewRateAdapter adap;
    ImageView star1_rat, star2_rat, star3_rat, star4_rat, star5_rat;
    LoadMoreListView review_lst;
    int page = 0;
    boolean LOADMORE = true;
    String product_id="";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utility.ChangeLang(getApplicationContext(),Utility.getIngerSharedPreferences(getApplicationContext(),Constants.LANG));
        setContentView(R.layout.activity_reviewlist_screen);
        mContext = this;
        setSupportActionBar((Toolbar) findViewById(R.id.h_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((Toolbar) findViewById(R.id.h_toolbar)).setNavigationIcon(R.drawable.ic_navigate_before_white);
        ((CustomTextView) findViewById(R.id.app_name)).setText(mContext.getResources().getString(R.string.all_review));
        ((CircularProgressBar) findViewById(rate_probar)).setMax(5);
        rating_per = (CustomTextView)findViewById(R.id.rating_per);
        bind();

        listner();
        Utility.hideKeyboard(mContext);
    }

    public void bind() {
        pb1 = (ProgressBar) findViewById(R.id.pb1);
        pb2 = (ProgressBar) findViewById(R.id.pb2);
        pb3 = (ProgressBar) findViewById(R.id.pb3);
        pb4 = (ProgressBar) findViewById(R.id.pb4);
        pb5 = (ProgressBar) findViewById(R.id.pb5);

        star1_rat = (ImageView) findViewById(R.id.star1_rat);
        star2_rat = (ImageView) findViewById(R.id.star2_rat);
        star3_rat = (ImageView) findViewById(R.id.star3_rat);
        star4_rat = (ImageView) findViewById(R.id.star4_rat);
        star5_rat = (ImageView) findViewById(R.id.star5_rat);
        review_lst = (LoadMoreListView) findViewById(R.id.review_lst);
        product_id=getIntent().getExtras().getString("ProductId");
    }

    public void listner(){
        if (Utility.isConnectingToInternet(mContext)) {
            GetReviewList(""+page);

            review_lst.setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    if (LOADMORE) {
                        page = page + 1;
                        GetReviewList(""+page);
                    } else {
                        review_lst.onLoadMoreComplete();
                    }
                }
            });

        } else {
            Ttoast.show(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
        }
    }

    public void SetAdap() {
        adap = new ReviewRateAdapter(mContext, reviewarray);
        review_lst.setAdapter(adap);
        Helper.getListViewSize(review_lst);
    }

    public void GetReviewList(String _page) {
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constants.ACTION, Constants.PRODUCTREVIEWLIST);
            jo.put(Constants.PRODUCTID, product_id);
            jo.put(Constants.PAGE, _page);
            if (page == 0) {
                Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
            }
            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    if (page == 0) {
                        Utility.HideDialog();
                    }
                    review_lst.onLoadMoreComplete();
                    if (result == null) {
                        page = page - 1;
                        Ttoast.show(mContext, mContext.getResources().getString(R.string.server_fail), false);

                    } else {
                        try {
                            JSONObject j = new JSONObject(result);
                            if (j.getString("success").equals("1")) {
                                JSONObject ob = j.getJSONObject(Constants.OBJECT);

                                ((CustomTextView) findViewById(R.id.tv_totalreview)).setText(j.getString("average_rate")+ " " + "Ratings" +"  |  "+ j.getString("review")+ " " + "Review");

                                float total_review = Float.parseFloat(j.getString("average_rate"));

                                JSONArray jsonArray = ob.getJSONArray("rate_detail");
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    if(jsonArray.getJSONObject(i).getString("product_rate").equals("5")){

                                       float total= Float.parseFloat(jsonArray.getJSONObject(4).getString("total_user"));
                                        Utility.showToast(mContext,"index 4---"+ total);

                                        float grand_total=(total/total_review)*100;
                                        pb1.setProgress((int) grand_total);
                                        ((CustomTextView) findViewById(R.id.total_star_txt1)).setText(jsonArray.getJSONObject(4).getString("total_user"));

                                    }else if(jsonArray.getJSONObject(i).getString("product_rate").equals("4")){

                                        float total= Float.parseFloat(jsonArray.getJSONObject(3).getString("total_user"));
                                        //Utility.showToast(mContext,"index 4---"+ total);

                                        float grand_total=(total/total_review)*100;
                                        pb2.setProgress((int) grand_total);
                                        ((CustomTextView) findViewById(R.id.total_star_txt3)).setText(jsonArray.getJSONObject(3).getString("total_user"));

                                    }else if(jsonArray.getJSONObject(i).getString("product_rate").equals("3")){

                                        float total= Float.parseFloat(jsonArray.getJSONObject(2).getString("total_user"));
                                        //Utility.showToast(mContext,"index 4---"+ total);

                                        float grand_total=(total/total_review)*100;
                                        pb3.setProgress((int) grand_total);

                                        ((CustomTextView) findViewById(R.id.total_star_txt2)).setText(jsonArray.getJSONObject(2).getString("total_user"));

                                    }else if(jsonArray.getJSONObject(i).getString("product_rate").equals("2")){

                                        float total= Float.parseFloat(jsonArray.getJSONObject(1).getString("total_user"));
                                        //Utility.showToast(mContext,"index 4---"+ total);

                                        float grand_total=(total/total_review)*100;
                                        pb4.setProgress((int) grand_total);

                                        ((CustomTextView) findViewById(R.id.total_star_txt2)).setText(jsonArray.getJSONObject(1).getString("total_user"));

                                    }else if(jsonArray.getJSONObject(i).getString("product_rate").equals("1")){

                                        float total= Float.parseFloat(jsonArray.getJSONObject(0).getString("total_user"));
                                        //Utility.showToast(mContext,"index 4---"+ total);
                                        float grand_total=(total/total_review)*100;
                                        pb5.setProgress((int) grand_total);
                                        ((CustomTextView) findViewById(R.id.total_star_txt2)).setText(jsonArray.getJSONObject(0).getString("total_user"));

                                    }
                                }

                                JSONArray jarray = ob.getJSONArray("user_detail");
                                for (int i = 0; i < jarray.length(); i++) {
                                    reviewarray.add(jarray.getJSONObject(i));
                                }
                                float rate = Float.parseFloat(j.getString("average_rate"));
                                rating_per.setText(j.getString("average_rate"));
                                SetRattingStar(rate);
                                SetAdap();
                                GetRewardProgress((int) rate);
                            } else {
                                LOADMORE = false;
                                Ttoast.show(mContext, j.getString("msg"), false);
                            }

                        } catch (JSONException j) {
                            LOADMORE = false;
                            j.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();

        } catch (JSONException je) {
            je.printStackTrace();
        }
    }

    public void SetRattingStar(Float rate) {

        if (rate >= 0 && rate < 0.5) {
            star1_rat.setImageResource(R.drawable.empty_star_grey);
            star2_rat.setImageResource(R.drawable.empty_star_grey);
            star3_rat.setImageResource(R.drawable.empty_star_grey);
            star4_rat.setImageResource(R.drawable.empty_star_grey);
            star5_rat.setImageResource(R.drawable.empty_star_grey);

        } else if (rate >= 0.5 && rate < 1) {
            star1_rat.setImageResource(R.drawable.star_half);
            star2_rat.setImageResource(R.drawable.empty_star_grey);
            star3_rat.setImageResource(R.drawable.empty_star_grey);
            star4_rat.setImageResource(R.drawable.empty_star_grey);
            star5_rat.setImageResource(R.drawable.empty_star_grey);

        } else if (rate >= 1 && rate < 1.5) {
            star1_rat.setImageResource(R.drawable.star_green);
            star2_rat.setImageResource(R.drawable.empty_star_grey);
            star3_rat.setImageResource(R.drawable.empty_star_grey);
            star4_rat.setImageResource(R.drawable.empty_star_grey);
            star5_rat.setImageResource(R.drawable.empty_star_grey);
        } else if (rate >= 1.5 && rate < 2) {
            star1_rat.setImageResource(R.drawable.star_green);
            star2_rat.setImageResource(R.drawable.star_half);
            star3_rat.setImageResource(R.drawable.empty_star_grey);
            star4_rat.setImageResource(R.drawable.empty_star_grey);
            star5_rat.setImageResource(R.drawable.empty_star_grey);
        } else if (rate >= 2 && rate < 2.5) {
            star1_rat.setImageResource(R.drawable.star_green);
            star2_rat.setImageResource(R.drawable.star_green);
            star3_rat.setImageResource(R.drawable.empty_star_grey);
            star4_rat.setImageResource(R.drawable.empty_star_grey);
            star5_rat.setImageResource(R.drawable.empty_star_grey);
        } else if (rate >= 2.5 && rate < 3) {
            star1_rat.setImageResource(R.drawable.star_green);
            star2_rat.setImageResource(R.drawable.star_green);
            star3_rat.setImageResource(R.drawable.star_half);
            star4_rat.setImageResource(R.drawable.empty_star_grey);
            star5_rat.setImageResource(R.drawable.empty_star_grey);
        } else if (rate >= 3 && rate < 3.5) {
            star1_rat.setImageResource(R.drawable.star_green);
            star2_rat.setImageResource(R.drawable.star_green);
            star3_rat.setImageResource(R.drawable.star_green);
            star4_rat.setImageResource(R.drawable.empty_star_grey);
            star5_rat.setImageResource(R.drawable.empty_star_grey);
        } else if (rate >= 3.5 && rate < 4) {
            star1_rat.setImageResource(R.drawable.star_green);
            star2_rat.setImageResource(R.drawable.star_green);
            star3_rat.setImageResource(R.drawable.star_green);
            star4_rat.setImageResource(R.drawable.star_half);
            star5_rat.setImageResource(R.drawable.empty_star_grey);
        } else if (rate >= 4 && rate < 4.5) {
            star1_rat.setImageResource(R.drawable.star_green);
            star2_rat.setImageResource(R.drawable.star_green);
            star3_rat.setImageResource(R.drawable.star_green);
            star4_rat.setImageResource(R.drawable.star_green);
            star5_rat.setImageResource(R.drawable.empty_star_grey);
        } else if (rate >= 4.5 && rate < 5) {
            star1_rat.setImageResource(R.drawable.star_green);
            star2_rat.setImageResource(R.drawable.star_green);
            star3_rat.setImageResource(R.drawable.star_green);
            star4_rat.setImageResource(R.drawable.star_green);
            star5_rat.setImageResource(R.drawable.star_half);
        } else if (rate == 5) {
            star1_rat.setImageResource(R.drawable.star_green);
            star2_rat.setImageResource(R.drawable.star_green);
            star3_rat.setImageResource(R.drawable.star_green);
            star4_rat.setImageResource(R.drawable.star_green);
            star5_rat.setImageResource(R.drawable.star_green);
        }
    }

    public void GetRewardProgress(final int points) {
        final CircularProgressBar c2 = (CircularProgressBar) findViewById(rate_probar);
        c2.animateProgressTo(0, points, new CircularProgressBar.ProgressAnimationListener() {

            @Override
            public void onAnimationStart() {
                System.out.println("SHOHSE========>" + "  DEVDASSSSSSSS");
            }

            @Override
            public void onAnimationProgress(int progress) {
                c2.setTitle("");
            }

            @Override
            public void onAnimationFinish() {
                c2.setSubTitle("");
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
