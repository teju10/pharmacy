package com.sehatyuser24.activity.registration;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import com.sehatyuser24.R;
import com.sehatyuser24.customwidget.CustomButton;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Utility;

/**
 * Created by Tejas on 25/4/17.
 */

public class SelectLanguageActivity extends AppCompatActivity {

    CustomButton arabic,english;
    private Context appContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);
        appContext = this;

        findViewById(R.id.btn_sl).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utility.getSharedPreferences(appContext, Constants.SPLASHCK).equals("1")) {
                    startActivity(new Intent(appContext, MainActivity.class));
                    finish();
                    overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                } else {
                    startActivity(new Intent(appContext, LoginActivity.class));
                    finish();
                    overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                }
            }
        });
        Init();
    }

    private void Init() {
        arabic =(CustomButton) findViewById(R.id.lang_ar);
        english =(CustomButton) findViewById(R.id.lang_en);

        arabic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arabic.setTextColor(getResources().getColor(R.color.white));
                english.setTextColor(getResources().getColor(R.color.colorPrimary));
                arabic.setBackgroundResource(R.drawable.edit_btn_green);
                english.setBackgroundResource(R.drawable.button_greenboarder);
                Utility.setSharedPreference(appContext, Constants.LANG_PREF, "ar");
                Utility.setIntegerSharedPreference(appContext, Constants.LANG, 1);
            }
        });
        english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                english.setTextColor(getResources().getColor(R.color.white));
                arabic.setTextColor(getResources().getColor(R.color.colorPrimary));
                english.setBackgroundResource(R.drawable.edit_btn_green);
                arabic.setBackgroundResource(R.drawable.button_greenboarder);
                Utility.setSharedPreference(appContext, Constants.LANG_PREF, "en");
                Utility.setIntegerSharedPreference(appContext, Constants.LANG, 0);
            }
        });
    }
}
