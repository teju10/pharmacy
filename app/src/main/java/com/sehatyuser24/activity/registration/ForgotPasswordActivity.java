package com.sehatyuser24.activity.registration;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;


import com.sehatyuser24.R;
import com.sehatyuser24.customwidget.CustomButton;
import com.sehatyuser24.customwidget.CustomEditText;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.Constant_Urls;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Tejas on 31/3/17.
 */

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {
    Context mContext;
    CustomButton btn_submit;
    CustomEditText email;
    ResponseTask rt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utility.ChangeLang(getApplicationContext(),Utility.getIngerSharedPreferences(getApplicationContext(),Constants.LANG));
        setContentView(R.layout.activity_forget_password);
        mContext = this;
        bind();
    }

    public void bind() {

        btn_submit = (CustomButton) findViewById(R.id.btn_submit);
        email = (CustomEditText) findViewById(R.id.fp_email);
        listner();

    }

    public void listner() {
        btn_submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit:
                if(email.equals("")){
                    Ttoast.show(mContext,mContext.getResources().getString(R.string.fill_email),false);
                }else if(!Utility.isValidEmail((email).getText().toString())){
                    Ttoast.show(mContext,mContext.getResources().getString(R.string.corect_email),false);
                }else{
                    FPassword(email.getText().toString());
                }
                break;


        }

    }

    public void FPassword(String email) {

       /*http://infograins.com.208-91-199-7.md-plesk-web2.webhostbox.net/INFO01/pharmacy/api/api.php?action=
       ForgotPassword&email=tejasdani@gmail.com*/

        try {
            JSONObject jo = new JSONObject();
            jo.put(Constants.ACTION, Constants.FPASSWORD);
            jo.put(Constants.EMAIL, email);

            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));

            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {
                        JSONObject jo = new JSONObject(result);
                        if (jo.getString("success").equals("1")) {
                            Ttoast.show(mContext,mContext.getResources().getString(R.string.pw_send),false);
                            Utility.Alert(mContext,mContext.getResources().getString(R.string.emailsend));
                            startActivity(new Intent(ForgotPasswordActivity.this, LoginActivity.class));
                            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                            finish();
                        } else {
                            Ttoast.show(mContext, jo.getString(Constants.SERVER_MSG), false);
                        }

                    } catch (JSONException j) {
                        j.printStackTrace();
                    }
                }
            });
            rt.execute();
        } catch (JSONException g) {
            g.printStackTrace();
        }
    }
}
