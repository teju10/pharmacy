package com.sehatyuser24.activity.registration;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.iid.FirebaseInstanceId;
import com.sehatyuser24.R;
import com.sehatyuser24.customwidget.CustomButton;
import com.sehatyuser24.customwidget.CustomEditText;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.GPSTracker;
import com.sehatyuser24.utility.Utility;
import org.json.JSONException;
import org.json.JSONObject;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import static android.Manifest.permission.GET_ACCOUNTS;


/**
 * Created by Tejas on 31/3/17.
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.
        OnConnectionFailedListener, FacebookCallback<LoginResult> {
    private static final int RC_SIGN_IN = 9001;
    private static final int SIGN_IN_CODE = 0;
    Context mContext;
    CustomEditText login_email, login_pass;
    CustomButton btn_login;
    LinearLayout btn_google, btn_fb;
    CustomTextView forgot_pass, sign_up;
    ResponseTask rt, rt1;
    GPSTracker mGPS;
    double lat, lang;
    String TAG = LoginActivity.class.getSimpleName();
    Dialog dialog_verification;
    AccessToken f_token;
    GoogleApiClient google_api_client;
    CallbackManager callbackManager = CallbackManager.Factory.create();
    private int request_code;
    //    private ConnectionResult connection_result;
    private boolean is_signInBtn_clicked = false;
    private boolean is_intent_inprogress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Utility.ChangeLang(getApplicationContext(),Utility.getIngerSharedPreferences(getApplicationContext(),Constants.LANG));
        setContentView(R.layout.activity_login);
        mContext = this;

        if (Utility.getSharedPreferences(mContext, Constants.FCMID).equals("")) {
            String token = FirebaseInstanceId.getInstance().getToken();
            System.out.println("token is" + token);
            Utility.setSharedPreference(mContext, Constants.FCMID, token);
        }

        FacebookSdk.sdkInitialize(getApplicationContext());
        disconnectFromFacebook();

        System.out.println("access token is" + AccessToken.getCurrentAccessToken());
        if (AccessToken.getCurrentAccessToken() != null) {
            LoginManager.getInstance().logOut();
        }
        LoginManager.getInstance().registerCallback(callbackManager, this);

        mGPS = new GPSTracker(mContext);
        lat = mGPS.getLatitude();
        lang = mGPS.getLongitude();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();

        google_api_client = new GoogleApiClient.Builder(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, gso).build();
//        gPlusSignOut();
        bind();
        printKeyHash();

    }

    private void printKeyHash() {
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.sehatyuser24", PackageManager.GET_SIGNATURES); //replace com.demo with your package name
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("DeveloperKeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                MessageDigest mda = MessageDigest.getInstance("SHA-1");
                mda.update(signature.toByteArray());
                Log.d("releseKeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("KeyHash:", e.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.d("KeyHash:", e.toString());
        }
    }

    public void bind() {
        login_email = (CustomEditText) findViewById(R.id.login_email);
        login_pass = (CustomEditText) findViewById(R.id.login_pass);
        btn_login = (CustomButton) findViewById(R.id.btn_login);
        btn_fb = (LinearLayout) findViewById(R.id.btn_facebook);
        btn_google = (LinearLayout) findViewById(R.id.btn_google);
        forgot_pass = (CustomTextView) findViewById(R.id.forgot_pass);
        sign_up = (CustomTextView) findViewById(R.id.sign_up);
        listener();
    }

    public void listener() {
        btn_login.setOnClickListener(this);
        btn_fb.setOnClickListener(this);
        btn_google.setOnClickListener(this);
        forgot_pass.setOnClickListener(this);
        sign_up.setOnClickListener(this);
        findViewById(R.id.skip).setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
//        startActivity(new Intent(mContext, SelectLanguageActivity.class));
        finish();
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_login:
                if (login_email.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_email), false);
                } else if (!Utility.isValidEmail((login_email).getText().toString())) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.corect_email), false);
                } else if (login_pass.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_pw), false);
                } else if (Utility.isConnectingToInternet(mContext)) {
                    LoginTask(login_email.getText().toString(), login_pass.getText().toString());
                } else {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
                }
                break;

            case R.id.btn_facebook:
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile", "email"));
                break;

            case R.id.btn_google:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(this, GET_ACCOUNTS) == PackageManager.PERMISSION_GRANTED) {
                        signIn();
                    } else {
                        requestLocationPermission();
                    }
                } else {
                    if (Utility.isConnectingToInternet(mContext)) {
                        signIn();
                    } else {
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
                    }
                }
                break;

            case R.id.forgot_pass:
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                // finish();
                break;

            case R.id.sign_up:
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                break;

            case R.id.skip:
                Utility.setSharedPreference(mContext, Constants.GUEST_USER, "0");
                Utility.setIntegerSharedPreference(mContext, Constants.COUNT, 0);
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                signIn();
            } else {
                Ttoast.show(this, getResources().getString(R.string.denied_permission), false);

            }
        }
    }

    private void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, GET_ACCOUNTS)) {
        }
        ActivityCompat.requestPermissions(this, new String[]{GET_ACCOUNTS}, 1);
    }

    public void LoginTask(String email, String pw) {
        /*http://infograins.com.208-91-199-7.md-plesk-web2.webhostbox.net/INFO01/pharmacy/api/api.php?action=
        Login&email=chandraprakash.infograins@gmail.com&password=1234&deviceid=1234&device_type=565623*/
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constants.ACTION, Constants.LOGIN);
            jo.put(Constants.EMAIL, email);
            jo.put(Constants.PASSWORD, pw);
            jo.put(Constants.DEVICEID, Utility.getSharedPreferences(mContext, Constants.FCMID));
            jo.put(Constants.DEVICETYPE, "Android");

            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));

            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {
                        JSONObject jo = new JSONObject(result);
                        if (jo.getString("success").equals("1")) {
                            Utility.setSharedPreference(mContext, Constants.SPLASHCK, "1");
                            JSONObject j = jo.getJSONObject(Constants.OBJECT);
                            Utility.setSharedPreference(mContext, Constants.USERID, j.getString("userid"));
                            Utility.setSharedPreference(mContext, Constants.PROFILE_IMAGE, j.getString("profile_image"));
                            Utility.setSharedPreference(mContext, Constants.FNAME, j.getString("fname"));
                            Utility.setSharedPreference(mContext, Constants.GUEST_USER, "1");
                            Utility.setIntegerSharedPreference(mContext, Constants.COUNT, Integer.parseInt(j.getString("count")));
                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                            finish();
                        } else {
                            Ttoast.ShowToast(mContext, jo.getString(Constants.SERVER_MSG), false);
                            if (jo.getString("verify_status").equals("0")) {
                                Utility.setSharedPreference(mContext, Constants.USERID, jo.getString("userid"));
                                verificationAlert();
                            }
                        }
                    } catch (JSONException j) {
                        j.printStackTrace();
                    }
                }
            });
            rt.execute();
        } catch (JSONException g) {
            g.printStackTrace();
        }
    }

    public void verificationAlert() {

        dialog_verification = new Dialog(mContext);
        dialog_verification.setCancelable(false);
        dialog_verification.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_verification.setContentView(R.layout.dialog_varification);
        dialog_verification.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        (dialog_verification.findViewById(R.id.skip_btn)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (((CustomEditText) dialog_verification.findViewById(R.id.commet_review)).getText().toString().equals("")) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.verifi_code),false);
                } else {
                    if (Utility.isConnectingToInternet(mContext)) {
                        VerificationTask(((CustomEditText) dialog_verification.findViewById(R.id.commet_review)).getText().toString());
                    } else {
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
                    }
                }
            }
        });

        (dialog_verification.findViewById(R.id.resend_code)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utility.isConnectingToInternet(mContext)) {
                    ResendCode();
                } else {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
                }
            }
        });

        dialog_verification.findViewById(R.id.cancel_dialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_verification.dismiss();
            }
        });

        dialog_verification.show();
    }

    public void VerificationTask(String code) {
        try {
            JSONObject j = new JSONObject();
            j.put(Constants.ACTION, Constants.ACCOUNTVERIFY);
            j.put("token", code);
            j.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(mContext, j);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    try {
                        Utility.HideDialog();
                        JSONObject jo = new JSONObject(result);
                        if (result == null) {
                            Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
                        } else {
                            if (jo.getString("success").equals("1")) {
                                dialog_verification.dismiss();
                                Utility.setSharedPreference(mContext, Constants.SPLASHCK, "1");
                                JSONObject j = jo.getJSONObject(Constants.OBJECT);
                                Utility.setSharedPreference(mContext, Constants.USERID, j.getString("userid"));
                                Utility.setSharedPreference(mContext, Constants.PROFILE_IMAGE, j.getString("profile_image"));
                                Utility.setSharedPreference(mContext, Constants.FNAME, j.getString("fname"));
                                Utility.setSharedPreference(mContext, Constants.GUEST_USER, "1");
                                Utility.setIntegerSharedPreference(mContext, Constants.COUNT, Integer.parseInt(j.getString("count")));
                                Ttoast.show(mContext, mContext.getResources().getString(R.string.signupsuccess), false);
                                Intent in = new Intent(mContext, MainActivity.class);
                                startActivity(in);
                                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                                finish();
                            } else {
                                Ttoast.ShowToast(mContext, jo.getString(Constants.SERVER_MSG), false);
                            }
                        }

                    } catch (JSONException je) {

                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void ResendCode() {
        /*http://infograins.com.208-91-199-7.md-plesk-web2.webhostbox.net/INFO01/pharmacy/api/api.php?action=
        ResendVerificationCode&userid=1*/

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, Constants.ResendEmailVerification);
            jsonObject.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
            rt1 = new ResponseTask(mContext, jsonObject);
            rt1.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, mContext.getResources().getString(R.string.server_fail));
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString("success").equals("1")) {
                                Utility.ShowToastMessage(mContext, json.getString(Constants.SERVER_MSG));
                            } else {
                                Utility.ShowToastMessage(mContext, json.getString(Constants.SERVER_MSG));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            rt1.execute();
        } catch (JSONException j) {
            j.printStackTrace();
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        try {
            if (result.isSuccess()) {

                // Signed in successfully, show authenticated UI.
                GoogleSignInAccount acct = result.getSignInAccount();
                String Name[] = acct.getDisplayName().split(" ");
                String Fname = Name[0];
                String Lname = Name[1];
                System.out.println("NAME " + acct.getDisplayName());
                System.out.println("ID " + acct.getId());
                System.out.println("EMAIL " + acct.getEmail());
                String personPhotoUrl = acct.getPhotoUrl().toString();
                System.out.println("IMAGE SOCIAL " + personPhotoUrl);

                String GPlusStr = personPhotoUrl.replace("https://", "");
//                gPlusSignOut();
                CallSocialApiWithResponse(Fname,
                        Lname, acct.getEmail(), acct.getId(), Constants.GOOGLE_KEY, GPlusStr);
            } else {
                gPlusSignOut();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(google_api_client);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void gPlusSignOut() {
        Auth.GoogleSignInApi.signOut(google_api_client).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {

                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
            if (requestCode == SIGN_IN_CODE) {
                request_code = requestCode;
                if (resultCode != RESULT_OK) {
                    is_signInBtn_clicked = false;
                    Utility.HideDialog();
                }
                is_intent_inprogress = false;
            }
        }
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        f_token = loginResult.getAccessToken();
        Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                //Start new activity or use this info in your project.
                try {

                    if (object.has("email")) {

                        String Name[] = object.getString("name").split(" ");
                        String Fname = Name[0];
                        String Lname = Name[1];
                        CallSocialApiWithResponse(Fname, Lname, object.getString("email"), object.getString("id"), Constants.FACEBOOK_KEY, "graph.facebook.com/" + object.getString("id") + "/picture?type=large");
                    } else {
                        String Name[] = object.getString("name").split(" ");
                        String Fname = Name[0];
                        String Lname = Name[1];

                        CallSocialApiWithResponse(Fname, Lname, "noemail@gmail.com", object.getString("id"), Constants.FACEBOOK_KEY, "graph.facebook.com/" + object.getString("id") + "/picture?type=large");
                    }
                    LoginManager.getInstance().logOut();
                } catch (Exception e) {
                    Utility.HideDialog();
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, name, email, gender, birthday");
        request.setAccessToken(f_token);
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    public void onCancel() {
        LoginManager.getInstance().logOut();
        Toast.makeText(mContext, "Facebook login Canceled", Toast.LENGTH_SHORT).show();
        Utility.HideDialog();
    }

    @Override
    public void onError(FacebookException error) {
        LoginManager.getInstance().logOut();
        error.printStackTrace();
        Log.e("FacebookException", "FacebookException ====>" + error);
        Toast.makeText(mContext, "Facebook login Error", Toast.LENGTH_SHORT).show();
        Utility.HideDialog();

    }

    public void disconnectFromFacebook() {
        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }
        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                LoginManager.getInstance().logOut();
            }
        }).executeAsync();
    }

    public void CallSocialApiWithResponse(String FName, String LName, String email, String o_id, final String o_pro, String pro_img) {
        System.out.println("result social" + o_pro);
        // http://infograins.com.208-91-199-7.md-plesk-web2.webhostbox.net/INFO01/pharmacy/api/api.php?action=SocialLogin&
        // fname=kamal&lname=chouhan&email=kamal.infograins@gmail.com&outhid=102213147002458&outh_provider=facebook&
        // deviceid=787845454787878&device_type=Android&longitude=74.565323&latitude=23.236565&usertype=1

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, Constants.S_LOGIN);
            jsonObject.put(Constants.FNAME, FName);
            jsonObject.put(Constants.LNAME, LName);
            jsonObject.put(Constants.EMAIL, email);
            jsonObject.put(Constants.OUTH_ID, o_id);
            jsonObject.put(Constants.OUTH_PROVIDER, o_pro);
            jsonObject.put(Constants.TOKEN, Utility.getSharedPreferences(mContext, Constants.FCMID));
            jsonObject.put(Constants.DEVICETYPE, "Android");
            jsonObject.put(Constants.PROFILE_IMAGE, pro_img);
            rt = new ResponseTask(mContext, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), LoginActivity.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString("success").equals("1")) {
                                Ttoast.show(mContext, mContext.getResources().getString(R.string.signinsuccess), false);

                                Utility.setSharedPreference(mContext, Constants.SPLASHCK, "1");
                                JSONObject userdetail = jobj.getJSONObject(Constants.OBJECT);
                                Utility.setSharedPreference(mContext, Constants.USERID, userdetail.getString("userid"));
                                Utility.setSharedPreference(mContext, Constants.PROFILE_IMAGE, userdetail.getString("profile_image"));
                                Utility.setSharedPreference(mContext, Constants.FNAME, userdetail.getString("fname"));
                                Utility.setSharedPreference(mContext, Constants.LOGINTYPE, "social_login");
                                Utility.setSharedPreference(mContext, Constants.GUEST_USER, "1");
                                Utility.setIntegerSharedPreference(mContext, Constants.COUNT, Integer.parseInt(userdetail.getString("count")));
                                LogoutMethod(o_pro);
                                startActivity(new Intent(mContext, MainActivity.class));
                                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                                finish();
                            } else {
                                Utility.showCroutonWarn(jobj.getString(Constants.SERVER_MSG), LoginActivity.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), LoginActivity.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        Utility.HideDialog();
        Log.d(TAG, "onConnectionFailed:" + result);
    }

    void LogoutMethod(String outh_provider) {
        if (outh_provider.equals(Constants.FACEBOOK_KEY)) {
            LoginManager.getInstance().logOut();
        }
        if (outh_provider.equals(Constants.GOOGLE_KEY)) {
            gPlusSignOut();
        }
    }

}
