package com.sehatyuser24.activity.registration;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;


import com.sehatyuser24.R;
import com.sehatyuser24.activity.other.ActivitySetting;
import com.sehatyuser24.customwidget.CustomEditText;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.Constant_Urls;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by Tejas on 28/4/17.
 */

public class ActivityChangePW extends AppCompatActivity {

    Context mContext;
    ResponseTask rt;
    CustomEditText old_pass, new_pass, confrm_pass;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utility.ChangeLang(getApplicationContext(),Utility.getIngerSharedPreferences(getApplicationContext(),Constants.LANG));
        setContentView(R.layout.activity_changepw);
        mContext = this;

        Find();

    }

    public void Find() {
        old_pass = (CustomEditText) findViewById(R.id.old_pass);
        new_pass = (CustomEditText) findViewById(R.id.new_pass);
        confrm_pass = (CustomEditText) findViewById(R.id.confrm_pass);

        Listner();
    }

    public void Listner() {
        findViewById(R.id.btn_sve_pw).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (old_pass.getText().toString().equals("")) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_oldpw), false);
                } else if (new_pass.getText().toString().equals("")) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_newpw), false);
                } else if (confrm_pass.getText().toString().equals("")) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_conpw), false);
                } else if (new_pass.getText().toString().length() < 6) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.pw_length), false);
                } else if (!confrm_pass.getText().toString().equals(new_pass.getText().toString())) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.pw_match), false);
                } else if (Utility.isConnectingToInternet(mContext)) {
                    ChangePwTask(old_pass.getText().toString(), new_pass.getText().toString());
                } else {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
                }
            }
        });
    }

    public void ChangePwTask(String old_pw, String newpw) {

       /*http://infograins.com.208-91-199-7.md-plesk-web2.webhostbox.net/INFO01/pharmacy/api/api.php?action=
       ChangePassword&old_password=1234&new_password=123456&userid=23*/

        try {
            JSONObject jo = new JSONObject();
            jo.put(Constants.ACTION, Constants.RESETPW);
            jo.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
            jo.put("old_password", old_pw);
            jo.put("new_password", newpw);

            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));

            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {
                        JSONObject jo = new JSONObject(result);
                        if (jo.getString("success").equals("1")) {
                            Ttoast.show(mContext, mContext.getResources().getString(R.string.pw_save), false);
//                            startActivity(new Intent(ActivityChangePW.this, LoginActivity.class));
                            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                            finish();
                        } else {
                            Ttoast.show(mContext, jo.getString(Constants.SERVER_MSG), false);
                        }

                    } catch (JSONException j) {
                        j.printStackTrace();
                    }
                }
            });
            rt.execute();
        } catch (JSONException g) {
            g.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
//        startActivity(new Intent(mContext, ActivitySetting.class));
        finish();
        super.onBackPressed();
    }

}
