package com.sehatyuser24.activity.registration;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;


import com.google.firebase.iid.FirebaseInstanceId;
import com.sehatyuser24.R;
import com.sehatyuser24.activity.other.Activity_AddShippingAddress;
import com.sehatyuser24.activity.other.Activity_GetAddress;
import com.sehatyuser24.customwidget.CustomAutoCompleteTextView;
import com.sehatyuser24.customwidget.CustomButton;
import com.sehatyuser24.customwidget.CustomEditText;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.serverinteraction.GetResponseTask;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.Constant_Urls;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.GPSTracker;
import com.sehatyuser24.utility.PlaceJSONParser;
import com.sehatyuser24.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by nilesh on 31/3/17.
 */

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {
    Context mContext;
    CustomEditText first_name, last_name, email, mobile, pass, con_pass,
            city, dist, building_no, flatno, landmark;
    CustomButton ctv_signup;
    ResponseTask rt;
    String token = "", gen = "",stringpickupaddress = "",Mo_no = "";
    GPSTracker mGPS;
    String[] genderarray;

    double lat, lang;
    Dialog dialog_verification;
    Spinner gender;
    ArrayList<HashMap<String, String>> gender_spinner;
    CustomTextView street_n;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Utility.ChangeLang(getApplicationContext(),  Utility.getIngerSharedPreferences(getApplicationContext(), Constants.LANG));
        setContentView(R.layout.activity_signup);

        super.onCreate(savedInstanceState);
        mContext = this;
        token = FirebaseInstanceId.getInstance().getToken();
        System.out.println("token is" + token);

        mGPS = new GPSTracker(mContext);
        lat = mGPS.getLatitude();
        lang = mGPS.getLongitude();
        genderarray = new String[]{mContext.getResources().getString(R.string.select_gender),
                mContext.getResources().getString(R.string.male), mContext.getResources().getString(R.string.female)};
        bind();

    }

    public void bind() {

        ctv_signup = (CustomButton) findViewById(R.id.ctv_signup);
        first_name = (CustomEditText) findViewById(R.id.first_name);
        last_name = (CustomEditText) findViewById(R.id.last_name);
        gender = (Spinner) findViewById(R.id.gender);
        email = (CustomEditText) findViewById(R.id.email);
        mobile = (CustomEditText) findViewById(R.id.mobile);
        pass = (CustomEditText) findViewById(R.id.pass);
        con_pass = (CustomEditText) findViewById(R.id.con_pass);
        city = (CustomEditText) findViewById(R.id.city);
        dist = (CustomEditText) findViewById(R.id.dist);
        street_n = (CustomTextView) findViewById(R.id.street_n);
        building_no = (CustomEditText) findViewById(R.id.building_no);
        flatno = (CustomEditText) findViewById(R.id.flatno);
        landmark = (CustomEditText) findViewById(R.id.landmark);

        listener();
    }

    public void listener() {
        ctv_signup.setOnClickListener(this);
        street_n.setOnClickListener(this);
        getcurrency_spinner();

      /*  TelephonyManager tMgr = (TelephonyManager)mContext.getSystemService(Context.TELEPHONY_SERVICE);
        String mPhoneNumber = tMgr.getLine1Number();
        System.out.println("");*/
    }

    public void getcurrency_spinner() {
        gender_spinner = new ArrayList<>();
        for (int i = 0; i < genderarray.length; i++) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("Type", genderarray[i]);
            gender_spinner.add(hm);
        }
        int[] to = new int[]{R.id.s_item};
        String[] from = new String[]{"Type"};
        // Creating a SimpleAdapter for the Spinner
        SimpleAdapter adapter = new SimpleAdapter(mContext, gender_spinner, R.layout.text_single_spinner, from, to);
        gender.setAdapter(adapter);

        gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position < 0) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.select_gender), false);
                } else if (position == 1) {
                    gen = "0";
                } else {
                    gen = "1";
                }
                System.out.println("GENDERRRR  " + gen);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                gen = "";
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.ctv_signup:

                if (first_name.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_fname), false);
                } else if (last_name.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_lname), false);
                } else if (!Utility.isValidEmail((email).getText().toString())) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.corect_email), false);
                } else if (gen.equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_gender), false);
                } else if (email.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_email), false);
                } else if (pass.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_pw), false);
                } else if (con_pass.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_conpw), false);
                } else if (city.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_city), false);
                } else if (dist.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_dist), false);
                } else if (street_n.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_street), false);
                } else if (flatno.getText().toString().equals("")) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_flat), false);
                } else if (landmark.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_landmark), false);
                } else if (building_no.getText().toString().equals("")) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.fill_building), false);
                } else if (!(con_pass).getText().toString().equals((pass).getText().toString())) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.pw_match), false);
                } else if ((pass).getText().toString().length() < 6) {
                    Ttoast.show(mContext, mContext.getResources().getString(R.string.pw_length), false);
                } else SignupTask(first_name.getText().toString(),
                        last_name.getText().toString(),
                        gen, email.getText().toString(),
                        pass.getText().toString(), city.getText().toString(),
                        dist.getText().toString(),street_n.getText().toString(),
                        landmark.getText().toString(),
                        building_no.getText().toString(),
                        flatno.getText().toString(),
                        mobile.getText().toString());
                break;

            case R.id.street_n:
                Intent i = new Intent(mContext, Activity_GetAddress.class);
                startActivityForResult(i, 2);
                break;

        }

    }

    public void SignupTask(String fname, final String lname, String _gender, String e_mail,
                           String pw, String _city, String _dist, String street_add, String lmark, String b_no, String flat_no, String mo) {

        /*http://infograins.com.208-91-199-7.md-plesk-web2.webhostbox.net/INFO01/pharmacy/api/api.php?action=Register&
        fname=ppiyush&lname=rothore&gender=male&email=piyush.infograins@gmail.com&password=1234&
        city=indore&district=indore&street_address=bhawarkua&landmark=matagujri&mobile=07315425545&device_type=Android&
        deviceid=chnadra&longitude=72.2335424&latitude=72.235656&usertype=1*/

        try {
            JSONObject jo = new JSONObject();
            jo.put(Constants.ACTION, Constants.REGISTRATION);
            jo.put(Constants.FNAME, fname);
            jo.put(Constants.LNAME, lname);
            jo.put(Constants.GENDER, _gender);
            jo.put(Constants.EMAIL, e_mail);
            jo.put(Constants.PASSWORD, pw);
            jo.put(Constants.CITY, _city);
            jo.put(Constants.DIST, _dist);
            jo.put(Constants.LANDMARK, lmark);
            jo.put(Constants.BUILDINGNUMBER, b_no);
            jo.put(Constants.FLATNUMBER, flat_no);
            jo.put(Constants.MOBILE, mo);
            jo.put(Constants.DEVICETYPE, "Android");
            jo.put(Constants.DEVICEID, token);
            jo.put(Constants.STREET, street_add);
            jo.put(Constants.USERTYPE, "1");

            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));

            rt = new ResponseTask(mContext, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {
                        JSONObject jo = new JSONObject(result);
                        if (jo.getString("success").equals("1")) {
                            JSONObject j = jo.getJSONObject(Constants.OBJECT);
                            Utility.setSharedPreference(mContext, Constants.USERID, j.getString("userid"));

                            first_name.setText("");
                            last_name.setText("");
                            email.setText("");
                            mobile.setText("");
                            pass.setText("");
                            con_pass.setText("");
                            city.setText("");
                            dist.setText("");
                            dist.setText("");
                            building_no.setText("");
                            flatno.setText("");
                            landmark.setText("");

                            verificationAlert();

                        } else {
                            Ttoast.ShowToast(mContext, jo.getString(Constants.SERVER_MSG), false);
                        }

                    } catch (JSONException j) {
                        j.printStackTrace();
                    }
                }
            });
            rt.execute();
        } catch (JSONException g) {
            g.printStackTrace();
        }
    }

    public void verificationAlert() {

        dialog_verification = new Dialog(mContext);
        dialog_verification.setCancelable(false);
        dialog_verification.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_verification.setContentView(R.layout.dialog_varification);
        dialog_verification.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        (dialog_verification.findViewById(R.id.skip_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CustomEditText) dialog_verification.findViewById(R.id.commet_review)).getText().toString().equals("")) {
                    Utility.ShowToastMessage(mContext, mContext.getResources().getString(R.string.enter_veri_code));
                } else {
                    if (Utility.isConnectingToInternet(mContext)) {
                        VerificationTask(((CustomEditText) dialog_verification.findViewById(R.id.commet_review)).getText().toString());
                    } else {
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
                    }
                }

            }
        });

        (dialog_verification.findViewById(R.id.resend_code)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utility.isConnectingToInternet(mContext)) {
                    ResendCode();
                } else {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
                }
            }
        });

        (dialog_verification.findViewById(R.id.cancel_dialog)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_verification.dismiss();
            }
        });

        dialog_verification.show();
    }

    public void VerificationTask(String code) {
        try {
            JSONObject j = new JSONObject();
            j.put(Constants.ACTION, Constants.ACCOUNTVERIFY);
            j.put("token", code);
            j.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
            rt = new ResponseTask(mContext, j);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    try {
                        Utility.HideDialog();
                        JSONObject jo = new JSONObject(result);
                        if (result == null) {
                            Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
                        } else {
                            if (jo.getString("success").equals("1")) {
                                dialog_verification.dismiss();
                                Ttoast.show(mContext, mContext.getResources().getString(R.string.signupsuccess), false);

                                Utility.setSharedPreference(mContext, Constants.SPLASHCK, "1");

                                JSONObject j = jo.getJSONObject(Constants.OBJECT);
                                Utility.setSharedPreference(mContext, Constants.USERID, j.getString("userid"));
                                Utility.setSharedPreference(mContext, Constants.PROFILE_IMAGE, j.getString("profile_image"));
                                Utility.setSharedPreference(mContext, Constants.FNAME, j.getString("fname"));
                                Utility.setSharedPreference(mContext, Constants.GUEST_USER, "1");
                                Intent in = new Intent(mContext, MainActivity.class);
                                startActivity(in);
                                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                                finish();
                            } else if (jo.getString(Constants.SERVER_MSG).contains("0")) {
                                ((CustomEditText) dialog_verification.findViewById(R.id.commet_review)).setText("");
                                Ttoast.ShowToast(mContext, jo.getString(Constants.SERVER_MSG), false);
                            } else {
                                Ttoast.ShowToast(mContext, jo.getString(Constants.SERVER_MSG), false);
                            }
                        }

                    } catch (JSONException je) {

                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void ResendCode() {
        /*http://infograins.com.208-91-199-7.md-plesk-web2.webhostbox.net/INFO01/pharmacy/api/api.php?action=
        ResendVerificationCode&userid=1*/

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, Constants.ResendEmailVerification);
            jsonObject.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
            Utility.setSharedPreference(mContext, Constants.GUEST_USER, "1");
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
//            (dialog_verification.findViewById(R.id.resend_code)).setClickable(false);
            rt = new ResponseTask(mContext, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
//                    (dialog_verification.findViewById(R.id.resend_code)).setClickable(true);
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, mContext.getResources().getString(R.string.server_fail));
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString("success").equals("1")) {
                                Utility.ShowToastMessage(mContext, json.getString(Constants.SERVER_MSG));
                            } else {
                                Utility.ShowToastMessage(mContext, json.getString(Constants.SERVER_MSG));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException j) {
            j.printStackTrace();
        }
    }

    /*-----------------------AUTOCOMPLETE ADDRESS-----------------*/

   /* private void SetListenersforadd() {
        street_n.setThreshold(1);
        street_n.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                placesTask = new PlacesTask();
                placesTask.execute(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                pickupaddress = "";
            }
        });
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            stringpickupaddress = data.getStringExtra("PICKADDRESS");
            street_n.setText(stringpickupaddress);}
        super.onActivityResult(requestCode, resultCode, data);
    }
}
