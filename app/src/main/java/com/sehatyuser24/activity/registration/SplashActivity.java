package com.sehatyuser24.activity.registration;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.sehatyuser24.R;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.CustomShapeImageView;
import com.sehatyuser24.utility.Utility;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

/**
 * Created by Tejas on 29/5/17.
 */

public class SplashActivity extends Activity {
    public static final String TAG = SplashActivity.class.getSimpleName();
    Context mContext;
    CustomTextView msg;
    CustomShapeImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mContext = this;

        methodForchanegeGet();
        loadAnimation();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Init();
            }
        }, 3000);

    }
    private void Init(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                SwitchActivity();
            } else {
                // Show rationale and request permission.
                requestLocationPermission();
            }
        }else {
            SwitchActivity();
        }
    }
    public void loadAnimation() {
        Animation anim = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.alpha);
        anim.reset();
        RelativeLayout l = (RelativeLayout) findViewById(R.id.activity_splash);
        l.clearAnimation();
        l.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.translate);
        anim.reset();
        image = (CustomShapeImageView) findViewById(R.id.slpash_img);
        image.clearAnimation();
        image.startAnimation(anim);
    }

    private void methodForchanegeGet() {
        if (Utility.getSharedPreferences(mContext, Constants.FCMID).equals("")) {
            String token = FirebaseInstanceId.getInstance().getToken();
            System.out.println("token is" + token);
            Utility.setSharedPreference(mContext, Constants.FCMID, token);
        }
    }

    public void SwitchActivity(){
        if(Utility.getSharedPreferences(mContext,Constants.SPLASHCK).equals("1")){
            loadAnimation();
            if (Utility.getSharedPreferences(mContext,Constants.USERID).equals("")){
                startActivity(new Intent(mContext, SelectLanguageActivity.class));
                finish();
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            }else{
                startActivity(new Intent(mContext, MainActivity.class));
                finish();
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            }

        }else{
            Utility.setSharedPreference(mContext,Constants.GUEST_USER,"0");
            startActivity(new Intent(mContext, SelectLanguageActivity.class));
            finish();
            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        }
    }

    private void requestLocationPermission(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)){
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this,new String[]{ACCESS_FINE_LOCATION},1);
    }

    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //Checking the request code of our request
        if(requestCode == 1){
            //If permission is granted
            if(grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                SwitchActivity();
                //Displaying a toast
//                Toast.makeText(this,"Permission granted now you can read the storage",Toast.LENGTH_LONG).show();
            }else{
                //Displaying another toast if permission is not granted
                Toast.makeText(this,getResources().getString(R.string.denied_permission),Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }
}
