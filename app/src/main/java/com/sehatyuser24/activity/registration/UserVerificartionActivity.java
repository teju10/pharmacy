package com.sehatyuser24.activity.registration;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.sehatyuser24.R;
import com.sehatyuser24.customwidget.CustomButton;
import com.sehatyuser24.customwidget.CustomEditText;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Utility;


/**
 * Created by Tejas on 31/3/17.
 */

public class UserVerificartionActivity extends AppCompatActivity implements View.OnClickListener{
    Context mContext;
    CustomEditText email;
    CustomButton btn_continue;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utility.ChangeLang(getApplicationContext(),Utility.getIngerSharedPreferences(getApplicationContext(),Constants.LANG));
        setContentView(R.layout.activity_verification);
        mContext=this;
        bind();
    }
    public void bind(){

        btn_continue= (CustomButton) findViewById(R.id.btn_continue);
        email= (CustomEditText) findViewById(R.id.verify_email);
        listener();

    }

    public void listener(){

        btn_continue.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_continue:
                startActivity(new Intent(UserVerificartionActivity.this,VerifyCodeActivity.class));
                overridePendingTransition(android.R.anim.slide_out_right,android.R.anim.slide_in_left);
                break;
        }
    }
}
