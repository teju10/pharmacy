package com.sehatyuser24.activity.registration;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.sehatyuser24.R;
import com.sehatyuser24.customwidget.CustomButton;
import com.sehatyuser24.customwidget.CustomEditText;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Utility;


/**
 * Created by Tejas on 10/4/17.
 */

public class VerifyCodeActivity extends AppCompatActivity implements View.OnClickListener {
    Context mContext;
    CustomEditText verify_code;
    CustomButton btn_continue;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utility.ChangeLang(getApplicationContext(),Utility.getIngerSharedPreferences(getApplicationContext(),Constants.LANG));
        setContentView(R.layout.activity_verification_code);
        mContext = this;
        bind();
    }

    public void bind() {
        verify_code = (CustomEditText) findViewById(R.id.verify_code);
        btn_continue = (CustomButton) findViewById(R.id.btn_continue);
        listener();
    }

    public void listener() {
        btn_continue.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_continue:
                break;
        }
    }
}
