package com.sehatyuser24.activity.registration;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.RatingBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.sehatyuser24.ImageSelection.ImageSelection;
import com.sehatyuser24.R;
import com.sehatyuser24.activity.other.Activity_Everyday_Medical_Product;
import com.sehatyuser24.customwidget.CustomEditText;
import com.sehatyuser24.customwidget.CustomTextView;
import com.sehatyuser24.customwidget.Ttoast;
import com.sehatyuser24.drawerfragment.AboutusFragment;
import com.sehatyuser24.drawerfragment.EarnFragment;
import com.sehatyuser24.drawerfragment.FamillyFragment;
import com.sehatyuser24.drawerfragment.HomeFragment;
import com.sehatyuser24.drawerfragment.MedicationFragment;
import com.sehatyuser24.drawerfragment.MyAccountFragment;
import com.sehatyuser24.drawerfragment.MyOrderFragment;
import com.sehatyuser24.drawerfragment.MyPrescriptionOrderFrag;
import com.sehatyuser24.drawerfragment.UploadFragment;
import com.sehatyuser24.drawerfragment.ViewOfferFragment;
import com.sehatyuser24.serverinteraction.ResponseListener;
import com.sehatyuser24.serverinteraction.ResponseTask;
import com.sehatyuser24.utility.CircleImageView;
import com.sehatyuser24.utility.Constants;
import com.sehatyuser24.utility.Utility;
import com.soundcloud.android.crop.Crop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    DrawerLayout drawer;
    Context mContext;
    Dialog sdialog;
    ImageSelection imageSelection;
    CircleImageView nav_uimg;
    CustomTextView user_name;
    View nav_header;
    NavigationView navigationView;
    ResponseTask responseTask;
    boolean doubleBackToExitPressedOnce = false;
    Bundle b;
    String up_status = "", OrderId = "";
    Dialog feedback_dia;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Utility.ChangeLang(getApplicationContext(), Utility.getIngerSharedPreferences(getApplicationContext(), Constants.LANG));
        setContentView(R.layout.activity_main);
        mContext = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarprovidernav2);
        setSupportActionBar(toolbar);
        try {
            b = new Bundle();
            b = getIntent().getExtras();
            if (b.getString(Constants.NOTITYPE).equals("OverallOrderRating")) {
//                OrderId = b.getString(Constants.ORDERID);
                GiveRattingDialog(b.getString(Constants.ORDERID));
            } else {
                up_status = b.getString(Constants.UPLOADSTATUS);
            }
            System.out.println("BUNDLE========>STATUS " + up_status);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (savedInstanceState == null) {
            Fragment fragment = null;
            Class fragmentClass = null;
            if (up_status.equals("1")) {
                Utility.setIntegerSharedPreference(mContext, Constants.UPLOADPRESCACTION, 1);
                fragmentClass = UploadFragment.class;
            } else if (Utility.getSharedPreferences(mContext, Constants.BillingCheck).equals("2")) {
                fragmentClass = UploadFragment.class;
                Utility.setSharedPreference(mContext, Constants.BillingCheck, "");
            } else if (Utility.getSharedPreferences(mContext, Constants.ShippingCheck).equals("1")) {
                fragmentClass = UploadFragment.class;
                Utility.setSharedPreference(mContext, Constants.ShippingCheck, "");
            } else {
                fragmentClass = HomeFragment.class;
            }

            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.flContentprovider, fragment).commit();
        }
        drawer = (DrawerLayout) findViewById(R.id.drawer_provider_layout1);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_provider_view1);

        if (Utility.getSharedPreferences(mContext, Constants.GUEST_USER).equals("1")) {
            navigationView.getMenu().findItem(R.id.signin).setVisible(false);

        } else if (Utility.getSharedPreferences(mContext, Constants.GUEST_USER).equals("0")) {
            navigationView.getMenu().findItem(R.id.my_account).setVisible(false);
            navigationView.getMenu().findItem(R.id.my_order).setVisible(false);
            navigationView.getMenu().findItem(R.id.family).setVisible(false);
            navigationView.getMenu().findItem(R.id.logout).setVisible(false);
        }

        navigationView.setNavigationItemSelectedListener(this);

        nav_header = LayoutInflater.from(this).inflate(R.layout.nav_header, null);
        navigationView.addHeaderView(nav_header);

        nav_uimg = (CircleImageView) nav_header.findViewById(R.id.nav_uimg);
        user_name = (CustomTextView) nav_header.findViewById(R.id.user_name);

        SetDrawerData();
    }

    @Override
    public void onBackPressed() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_provider_layout1);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            finish();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Ttoast.show(mContext, mContext.getResources().getString(R.string.backprsed), false);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 3000);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();
        item.setChecked(true);
        //when we want to launch activity by intent from this method
        if (id == R.id.logout) {
            Logout();
        } else if (id == R.id.signin) {
            startActivity(new Intent(mContext, LoginActivity.class));
            finish();
        } else {
            Fragment fragment = null;
            switch (item.getItemId()) {
                case R.id.home:
                    fragment = new HomeFragment();
                    break;
                case R.id.my_account:
                    fragment = new MyAccountFragment();
                    break;
                case R.id.my_order:
                    fragment = new MyOrderFragment();
                    break;

                case R.id.my_presc:
                    fragment = new MyPrescriptionOrderFrag();
                    break;

                case R.id.family:
                    fragment = new FamillyFragment();
                    break;

                case R.id.upload:
                    if (Utility.getSharedPreferences(mContext, Constants.GUEST_USER).equals("0")) {
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.youneedlogin), false);
                    } else {
                        fragment = new UploadFragment();
                    }
                    break;
                case R.id.offer:
                    fragment = new ViewOfferFragment();
                    break;

             /*   case R.id.rating:
                    Ttoast.ShowToast(mContext, getString(R.string.c_soon), false);
                    fragment = new RatingFragment();
                    break;
*/
                case R.id.about:
                    fragment = new AboutusFragment();
                    break;

                case R.id.earn:
//                    Ttoast.ShowToast(mContext, getString(R.string.c_soon), false);
                    if (Utility.getSharedPreferences(mContext, Constants.GUEST_USER).equals("0")) {
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.youneedlogin), false);
                    } else {
                        fragment = new EarnFragment();
                    }
                    break;

                case R.id.madication:
                    fragment = new MedicationFragment();
                    break;

                case R.id.baby:
                    Utility.setIntegerSharedPreference(mContext, Constants.BTNCLK, 1);
                    startActivity(new Intent(mContext, Activity_Everyday_Medical_Product.class));
                    finish();
                    break;

                case R.id.setting:
                    Ttoast.ShowToast(mContext, getString(R.string.c_soon), false);
//                    startActivity(new Intent(mContext, ActivitySetting.class));
                    break;

                default:
                    break;
            }
            if (fragment != null) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.flContentprovider, fragment).commit();
            }
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_provider_layout1);
            drawer.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    public void Logout() {
        LogoutMethod();
    }

    public void setListener(ImageSelection listener) {
        imageSelection = listener;
    }

    public void SelectImage(MyAccountFragment frag) {
        setListener((ImageSelection) frag);
        sdialog = new Dialog(mContext, R.style.MyDialog);
        sdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        sdialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        sdialog.setContentView(R.layout.dialog_imgeselect);
        sdialog.setCancelable(true);
        sdialog.show();
        //((CustomTextView) sdialog.findViewById(R.id.dialog_pack_name)).setText(getResources().getString(R.string.selectpic));
        sdialog.findViewById(R.id.dialog_close).setOnClickListener(new MyDialogClick());
        sdialog.findViewById(R.id.camera).setOnClickListener(new MyDialogClick());
        sdialog.findViewById(R.id.gallery).setOnClickListener(new MyDialogClick());
        sdialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dialog.dismiss();
                    getFragmentManager().popBackStackImmediate();
                }
                return true;
            }
        });
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == Activity.RESULT_OK) {
            if (imageSelection != null) {
                imageSelection.selected(Crop.getOutput(result).getPath());
            }
        } else if (resultCode == Crop.RESULT_ERROR) {
            Utility.ShowToastMessage(mContext, Crop.getError(result).getMessage());
        }
    }

    private void beginCrop(Uri source) {
        File pro = new File(Utility.MakeDir(Constants.MYFOLDER, mContext), System.currentTimeMillis() + Constants.SEND);
        Uri destination1 = Uri.fromFile(pro);
        Crop.of(source, destination1).asSquare().withMaxSize(200, 200).start(MainActivity.this);
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            File getImage = mContext.getExternalCacheDir();
            if (getImage != null) {
                outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
            }
        } else {
            File getImage = mContext.getExternalCacheDir();
            if (getImage != null) {
                outputFileUri = FileProvider.getUriForFile(mContext, com.sehatyuser24.BuildConfig.APPLICATION_ID + ".provider",
                        new File(getImage.getPath(), "pickImageResult.jpeg"));
            }
        }
        return outputFileUri;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // new functions
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 1) {
                Uri imageUri = getPickImageResultUri(data);
                beginCrop(imageUri);
            } else if (requestCode == 2) {
                Uri selectedImage = data.getData();
                beginCrop(selectedImage);
            } else if (requestCode == Crop.REQUEST_CROP) {
                handleCrop(resultCode, data);
            }
        }
    }

    public void callSelectedFragment(String FragmentName, String Title) {
        Fragment fragment = null;
        if (FragmentName.equals(Constants.UPLOADFRAGMENT)) {
            fragment = new UploadFragment();
        } else if (FragmentName.equals(Constants.MYORDERFRAGMENT)) {
            fragment = new MyOrderFragment();
        } else if (FragmentName.equals(Constants.FAMILYFRAGMENT)) {
            fragment = new FamillyFragment();
        } else if (FragmentName.equals(Constants.MYPRESCORDERFRAGMENT)) {
            fragment = new MyPrescriptionOrderFrag();
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.flContentprovider, fragment);
            fragmentTransaction.commit();
            // set the toolbar title
            getSupportActionBar().setTitle(Title);
        }
    }

    public void SetDrawerData() {
        System.out.println("PROFILEIMAGE  " + Utility.getSharedPreferences(mContext, Constants.PROFILE_IMAGE));
        if (!Utility.getSharedPreferences(mContext, Constants.PROFILE_IMAGE).equals("")) {
            Glide.with(mContext)
                    .load(Utility.getSharedPreferences(mContext, Constants.PROFILE_IMAGE))
                    .crossFade().diskCacheStrategy(DiskCacheStrategy.ALL)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            Glide.with(mContext).load(R.drawable.user_icon).into(nav_uimg);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {

                            return false;
                        }
                    }).into(nav_uimg);
        } else if (!Utility.getSharedPreferences(mContext, Constants.PROFILE_IMAGE).equals("")) {
            Glide.with(mContext).load(Utility.getSharedPreferences(mContext, Constants.PROFILE_IMAGE)).
                    override(50, 50).diskCacheStrategy(DiskCacheStrategy.ALL).
                    into(nav_uimg);

        } else {
            Glide.with(mContext).load(R.drawable.user_icon).
                    override(50, 50).
                    diskCacheStrategy(DiskCacheStrategy.ALL).
                    into(nav_uimg);
        }
        (user_name).setText(Utility.getSharedPreferences(mContext, Constants.FNAME));

    }

    void LogoutMethod() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, Constants.LOGOUT);
            jsonObject.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));
            responseTask = new ResponseTask(mContext, jsonObject);

            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, mContext.getResources().getString(R.string.server_fail));
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString("success").equals("1")) {
                                Utility.setSharedPreference(mContext, Constants.USERID, "");
                                Utility.setSharedPreference(mContext, Constants.FNAME, "");
                                Utility.setSharedPreference(mContext, Constants.EMAIL, "");
                                Utility.setSharedPreference(mContext, Constants.PROFILE_IMAGE, "");
                                Utility.setSharedPreferenceBoolean(mContext, Constants.LOGIN, false);
                                Utility.clearsharedpreference(mContext);
                                Utility.setIntegerSharedPreference(mContext, Constants.COUNT, 0);
                                startActivity(new Intent(mContext, LoginActivity.class));
                                finish();
                                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                            } else {
                                Utility.ShowToastMessage(mContext, json.getString(Constants.SERVER_MSG));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            responseTask.execute();
        } catch (JSONException j) {
            j.printStackTrace();
        }
    }

    public class MyDialogClick implements View.OnClickListener {

        public MyDialogClick() {

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.dialog_close:
                    if (sdialog.isShowing() && sdialog != null) {
                        sdialog.dismiss();
                    }
                    break;
                case R.id.camera:
                    if (sdialog.isShowing() && sdialog != null) {
                        sdialog.dismiss();
                    }
//                    Ttoast.ShowToast(mContext,mContext.getResources().getString(R.string.c_soon);
                    Uri outputFileUri = getCaptureImageOutputUri();
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                    startActivityForResult(intent, 1);
                    break;
                case R.id.gallery:
                    if (sdialog.isShowing() && sdialog != null) {
                        sdialog.dismiss();
                    }
                    Intent intent1 = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent1, 2);
                    break;
            }
        }
    }

    public void GiveRattingDialog(final String orderid) {

        feedback_dia = new Dialog(mContext, R.style.MyDialog);
        feedback_dia.requestWindowFeature(Window.FEATURE_NO_TITLE);
        feedback_dia.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        feedback_dia.setContentView(R.layout.dialog_feedback_screen);
        feedback_dia.setCancelable(true);
        feedback_dia.show();

        CustomTextView title = (CustomTextView) feedback_dia.findViewById(R.id.a);
        title.setText(mContext.getResources().getString(R.string.overall_rate_title));

        feedback_dia.findViewById(R.id.edt_feedback).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.edt_feedback) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        try {
            LayerDrawable stars = (LayerDrawable) ((RatingBar) feedback_dia.findViewById(R.id.feed_rate_bar)).getProgressDrawable();
            stars.getDrawable(2).setColorFilter(mContext.getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(0).setColorFilter(mContext.getResources().getColor(R.color.grey), PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(1).setColorFilter(mContext.getResources().getColor(R.color.grey), PorterDuff.Mode.SRC_ATOP);
        } catch (Exception e) {
            e.printStackTrace();
        }

       /* try {
            ((RatingBar) feedback_dia.findViewById(R.id.rate_bar)).setRating(Float.parseFloat(jlist.get(pos1).getString("product_rate")));
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        feedback_dia.findViewById(R.id.submit_feedback).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float rat = ((RatingBar) feedback_dia.findViewById(R.id.feed_rate_bar)).getRating();
                if (rat > 0) {
                    if (feedback_dia != null && feedback_dia.isShowing()) {
                        feedback_dia.cancel();
                    }
                    if (feedback_dia.findViewById(R.id.edt_feedback).equals("")) {
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_cmnt), false);
                    } else if (Utility.isConnectingToInternet(mContext)) {
                        SubmitOverall(orderid,"" + rat,
                                ((CustomEditText)feedback_dia.findViewById(R.id.edt_feedback)).getText().toString());
                    } else {
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.check_internetcon), false);
                    }
                } else {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.rat_rev), false);
                }
            }
        });

        feedback_dia.findViewById(R.id.dialog_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (feedback_dia != null && feedback_dia.isShowing()) {
                    feedback_dia.cancel();
                }
            }
        });
    }
//    http://www.infograins.in/INFO01/Pharm01/api/api.php?action=OverallOrderFeedback&userid=155&orderid=OD1502702583&comment=hii%20thanks&rating=3

    public void SubmitOverall(String oid,String rate, String cmmnt) {

        /* http://www.infograins.in/INFO01/Pharm01/api/api.php?action=OverallOrderFeedback&userid=155&
        orderid=OD1502702583&comment=hii%20thanks&rating=3
*/
        try {
            JSONObject j = new JSONObject();
            j.put(Constants.ACTION, Constants.OVERALL_ORDERFEED);
            j.put(Constants.USERID, Utility.getSharedPreferences(mContext, Constants.USERID));
            j.put("orderid", oid);
            j.put("comment", cmmnt);
            j.put("rating", rate);

            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.loading_msg));

            responseTask = new ResponseTask(mContext, j);
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {
                        if (result == null) {
                            Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.server_fail), false);
                        } else {
                            JSONObject object = new JSONObject(result);
                            if (object.getString("success").equals("1")) {
                                Ttoast.show(mContext, mContext.getResources().getString(R.string.feedback_submit), false);
                                if (feedback_dia != null && feedback_dia.isShowing()) {
                                    feedback_dia.cancel();
                                }
                            } else {
                                Ttoast.ShowToast(mContext, object.getString("msg"), false);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
            responseTask.execute();
        } catch (JSONException j) {
            Utility.HideDialog();
            j.printStackTrace();
        }

    }
}